FROM python:3.6

#RUN apk update && apk add openssh
#RUN apk add --no-cache git

RUN mkdir -p /app
#RUN mkdir /app/.ssh/
#ADD id_rsa /app/.ssh/id_rsa

# Create known_hosts
#RUN touch /app/.ssh/known_hosts

# Add bitbuckets key
#RUN ssh-keyscan -t rsa bitbucket.org > /app/.ssh/known_hosts

RUN git clone https://endemio:XVNarxgasSBVdHVtBMrL@bitbucket.org/endemio/assem-instagram.git  /app/scripts

WORKDIR /app/scripts

RUN pip install -r requirements.txt

RUN mkdir -p /app/storage

RUN mkdir -p /app/storage/logs
RUN mkdir -p /app/storage/redis
RUN mkdir -p /app/storage/followers
RUN mkdir -p /app/storage/followings
RUN mkdir -p /app/storage/posts

CMD [ "python", "./public.py" ]