import config
import json
import time
from datetime import datetime, timezone
import os
import requests
import random
import sys
import ast
import traceback

from module.instagram.parsing import InstagramParsing
from module.mysql.main import MysqlORM
from module.rabbitmq.amqp import AMQP
from module.redis.main import RedisClass
from module.aws.s3 import S3
from module.minio.minio import MinioClass
from module.custom.location import CustomLocationFunction
from module.custom.functions import CustomFunction
from module.custom.amqp_requests import AMQPRequests


def log_info(message, username, tag=None):
    log.info({
        'message': message,
        'username': username,
        'link': custom_f.get_link(),
        'token': custom_f.get_token(),
        'virtual_host': virtual_host,
        'message_tag': tag
    })


def log_warning(message, username, tag=None):
    log.warning({
        'message': message,
        'username': username,
        'link': custom_f.get_link(),
        'token': custom_f.get_token(),
        'virtual_host': virtual_host,
        'message_tag': tag
    })


def amqp_send_message(ch, method, answer, routing_key, priority=1, error=False, delivery_mode=2):
    try:
        json_object = json.loads(answer)
    except:
        answer = json.dumps(answer, default=lambda o: o.__dict__, indent=4)
        pass

    routing = routing_key if not error else routing_error_config

    try:
        ch.basic_publish(
            exchange=Amqp.get_exchange(),
            routing_key=routing,
            body=answer,
            properties=Amqp.basic_properties(delivery_mode, priority))
    except Exception as err:
        log.exception("Error while sending message to result queue " + str(err))
        quit()

    log_info("Send message to {} queue, message len =  {}, text = {}".format(routing, len(answer), json.loads(answer)),
             None)


def amqp_callback(ch, method, properties, message):
    message = (message.decode('ascii'))
    timenow = str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    timestart = time.time()

    log_info("Get Body: {}".format(message), None)

    try:
        message_dict = json.loads(message)
    except Exception as err:
        log.exception("Get exception at Body message JSON parsing {}")

        # Acking message from AMQP
        ch.basic_ack(delivery_tag=method.delivery_tag)

        # Error message
        answer = '{' + '"task":"task-parsing-error","datetime":"{}","status":false,"error":"{}","received":"{}"'.format(
            str(timenow), ' Get exception at JSON parsing ' + str(err), message) + '}'

        amqp_send_message(ch, method, answer, '', 1, True)
        return

    # Project flow
    project_flow = list(message_dict['project_flow']) if 'project_flow' in message_dict else {}
    if not project_flow:
        log.error('{' + '"message":"Project flow not exist {}","message_tag":""'.format(message_dict) + '}')
        ch.basic_ack(delivery_tag=method.delivery_tag)
        # Default answer
        answer = '{' + '"task":"project-flow-error","status":false,"datetime":"{}","error":"No tasks found"'.format(
            timenow) + '}'
        amqp_send_message(ch, method, answer, '', 1, True)
        return
    else:
        project_flow_body = project_flow.pop(0)

    # Task
    task = str(project_flow_body['task']) if 'task' in project_flow_body else ''

    # Tokens
    token = str(message_dict['token']) if 'token' in message_dict else ''
    custom_f.set_token(token)

    # Link
    link = str(project_flow_body['link']) if 'link' in project_flow_body else ''
    custom_f.set_link(link)
    amqp_request_f.set_link(link)

    # Other settings
    limit = int(project_flow_body['limit']) if 'limit' in project_flow_body else 500
    option_project_id = int(project_flow_body['project_id']) if 'project_id' in project_flow_body else 0
    option_user_id = int(project_flow_body['user_id']) if 'user_id' in project_flow_body else 0

    # Main options
    option_use_proxy = bool(project_flow_body['use_proxy']) if 'use_proxy' in project_flow_body else True
    option_priority = int(project_flow_body['priority']) if 'priority' in project_flow_body else 0
    option_mysql_instance = int(project_flow_body['mysql_instance']) if 'mysql_instance' in project_flow_body else 4

    # Check option
    option_force_check = bool(project_flow_body['force_check']) if 'force_check' in project_flow_body else False
    option_get_posts_data = bool(
        project_flow_body['get_posts_data']) if 'get_posts_data' in project_flow_body else False
    option_get_account_data = bool(
        project_flow_body['get_accounts_data']) if 'get_accounts_data' in project_flow_body else True
    option_get_engagements_data = bool(
        project_flow_body['get_engagements_data']) if 'get_engagements_data' in project_flow_body else False

    # Store options
    option_file_append = str(project_flow_body['file_token']) if 'file_token' in project_flow_body else ''
    option_store_local_json = bool(project_flow_body['store_json']) if 'store_json' in project_flow_body else False
    option_store_redis = bool(project_flow_body['store_redis']) if 'store_redis' in project_flow_body else False
    option_store_s3 = bool(project_flow_body['store_s3']) if 'store_s3' in project_flow_body else True

    # AMQP options
    routing_return = str(
        project_flow_body['return_queue']) if 'return_queue' in project_flow_body else routing_return_config

    # Export options
    option_export_random = int(project_flow_body['export_random']) if 'export_random' in project_flow_body else 0
    option_export_full = bool(project_flow_body['export_full']) if 'export_full' in project_flow_body else False
    option_export_mysql_keys = dict(
        project_flow_body['export_mysql_keys']) if 'export_mysql_keys' in project_flow_body else dict()
    option_export_mysql_table = str(
        project_flow_body['export_mysql_table']) if 'export_mysql_table' in project_flow_body else 'account'

    # Set direct Mysql connection
    mysql = None
    if option_mysql_instance:

        # Get mysql data from config by key
        item = custom_f.get_mysql_by_key(option_mysql_instance)

        try:
            mysql = MysqlORM(log, debug)
            mysql.config_set_mysql(True, item['host'], item['port'], item['login'], item['pass'], item['db'])
        except Exception as err:
            log.error(
                '{' + '"message":"Mysql connection error {},creds {}","message_tag":"mysql-log"'.format(err,
                                                                                                        item) + '}')
            raise
    # Coefficient to decrease account check for ip with account in "account-info" tasks
    coef = 1

    # -----------------------------------------------------------------------------------------------------------------
    #  Get SHORT instagram account data and store it in json (if 'store-json' = true)
    #  and send data to queue 'result' from amqp-message. If set redis - store in redis
    #

    # if task == 'full-audit-choose-accounts':

    if task == 'get-location-data-from-posts':

        if not option_project_id:
            ch.basic_ack(delivery_tag=method.delivery_tag)
            log.error('Project_id is empty')
            mysql.close()
            return

        username = []

        if not username:
            username = str(project_flow_body['username']) if 'username' in project_flow_body else None
            if not username:
                log.warning(
                    '{' + '"message":"Username not found in {}","username":"","message_tag":""'
                    .format(message_dict) + '}')
                ch.basic_ack(delivery_tag=method.delivery_tag)
                time.sleep(10)
                return

        log_info("Start {}, username {}  user_id = {}, project_id is {}".format(task,
                                                                                username,
                                                                                option_user_id,
                                                                                option_project_id),
                 username)
        try:
            account = mysql.get_row_form_table_by_field_array('account', [username], 'username')[0]
        except Exception as err:

            log_info("Account not exists in DB, try to get account data", username)

            ch.basic_ack(delivery_tag=method.delivery_tag)

            # send request to get account data to store it in mysql with getting posts by limit
            step1 = amqp_request_f.request_get_account_data('public', option_priority, option_mysql_instance)
            step2 = amqp_request_f.request_get_all_posts('location', option_priority + 1, limit, option_mysql_instance,False,False,False,None,username,option_user_id)

            project_flow = [step1] + [step2] + [project_flow_body] + project_flow

            answer = amqp_request_f.answer_default(project_flow, option_project_id, [username])
            amqp_send_message(ch, method, answer, 'public', option_priority)

            # Store account information about country/city to Redis
            # step1 = amqp_request_f.request_append_account_data_to_redis('', option_priority, 'account', option_user_id,
            #                                                            token, username)
            # data = {'city': 1, 'country': 1}

            # amqp_send_message(ch, method, amqp_request_f.answer_default([step1], username, [], data), 'redis',
            #                  option_priority)

            return

        filename = str(username) + str(option_file_append) + '.json'
        filename_temp = str(username) + '-temp.json'

        posts = []
        if option_store_s3 and not option_force_check:
            if s3.check_file_exist(s3_posting_bucket, filename):
                log_info("File {} exists in S3 {}".format(filename, s3_posting_bucket), username)

                # Get object data to check time modified
                json_posts = s3.get_object_content(s3_posting_bucket, filename)

                posts = json.loads(json_posts)

        log_info('Account posts is {}, is private {}, user_id {}'.format(account['posts'], account['private'],
                                                                         account['user_id']), username)

        if not account['posts'] or account['private']:
            log_info('Account hasnt posts - stop checking and send to result queue', username)

            mysql.update_table_all_values_by_key('account_location', {'username': username,
                                                                      'location_post': json.dumps(
                                                                          ['empty']),
                                                                      'result_post': json.dumps(
                                                                          'empty'), 'summary': json.dumps([])},
                                                 {'username': 'username', 'result_post': 'result_post',
                                                  'location_post': 'location_post', 'summary': 'summary'})

            ch.basic_ack(delivery_tag=method.delivery_tag)

            if option_store_redis:
                # Store account information about country/city to Redis
                step1 = amqp_request_f.request_append_account_data_to_redis('', option_priority, 'account',
                                                                            option_user_id, token, username)
                data = {'city': 1, 'country': 1}

                amqp_send_message(ch, method, amqp_request_f.answer_default([step1], username, [], data), 'redis',
                                  option_priority)

            if routing_return:
                amqp_send_message(ch, method, amqp_request_f.answer_default(project_flow, username),
                                  routing_return, option_priority)
            return

        # If there isn;t posts in s3 - get it
        if not len(posts) and account['posts'] > 2:
            log_info('Posts not len(posts) and account[posts] not found in s3', username)
            ch.basic_ack(delivery_tag=method.delivery_tag)
            step2 = amqp_request_f.request_get_all_posts('location', option_priority + 1, limit, option_mysql_instance,False,False,False,None,username,option_user_id)

            project_flow = [step2] + [project_flow_body] + project_flow

            answer = amqp_request_f.answer_default(project_flow, option_project_id, [username])

            amqp_send_message(ch, method, answer, 'public', option_priority)

            log_info("There isn't posts json in s3 - try to get it ", username)

            return
        elif not len(posts) and account['posts'] < 3:
            log_info('Posts not len(posts) and account[posts] < 3 not found in s3 and to less in Ig account', username)
            ch.basic_ack(delivery_tag=method.delivery_tag)

            mysql.update_table_all_values_by_key('account_location', {'username': username,
                                                                      'location_post': json.dumps([]),
                                                                      'result_post': json.dumps([]),
                                                                      'summary': json.dumps([])},
                                                 {'username': 'username', 'result_post': 'result_post',
                                                  'location_post': 'location_post', 'summary': 'summary'})

            if option_store_redis:
                # Store account information about country/city to Redis
                step1 = amqp_request_f.request_append_account_data_to_redis('', option_priority, 'account',
                                                                            option_user_id, token, username)
                data = {'city': 1, 'country': 1}

                amqp_send_message(ch, method, amqp_request_f.answer_default([step1], username, [], data), 'redis',
                                  option_priority)

            answer = amqp_request_f.answer_default(project_flow, option_project_id, [username])
            amqp_send_message(ch, method, answer, routing_return, option_priority)
            log_info("There isn\'t posts json in s3 and account have less 4 posts - goint to next project_flow",
                     username)
            return

        log_info('Number of posts is {}, accounts posts from IG stat is {}'.format(len(posts), account['posts']),
                 username)

        if posts[0] == 'n/a' and account['posts'] < 3:
            log_info('There isnt posts to check, store empty data to mysq and return to project flow ', username)

            ch.basic_ack(delivery_tag=method.delivery_tag)

            mysql.update_table_all_values_by_key('account_location', {'username': username,
                                                                      'location_post': json.dumps([]),
                                                                      'result_post': json.dumps([]),
                                                                      'summary': json.dumps([])},
                                                 {'username': 'username', 'result_post': 'result_post',
                                                  'location_post': 'location_post', 'summary': 'summary'})

            if option_store_redis:
                # Store account information about country/city to Redis
                step1 = amqp_request_f.request_append_account_data_to_redis('', option_priority, 'account',
                                                                            option_user_id, token, username)
                data = {'city': 1, 'country': 1}

                amqp_send_message(ch, method, amqp_request_f.answer_default([step1], username, [], data), 'redis',
                                  option_priority)

            amqp_send_message(ch, method, amqp_request_f.answer_default(project_flow, username), routing_return,
                              option_priority)
            log_info("There isn\'t posts - goint to next project_flow", username)
            return
        elif posts[0] == 'n/a' and account['posts'] >= 3:
            # Repeat posts
            ch.basic_ack(delivery_tag=method.delivery_tag)

            # send request to get account data to store it in mysql with getting posts by limit
            step2 = amqp_request_f.request_get_all_posts('location', option_priority + 1, limit, option_mysql_instance,False,False,False,None,username,option_user_id)

            project_flow = [step2] + [project_flow_body] + project_flow
            answer = amqp_request_f.answer_default(project_flow, option_project_id, [username])
            amqp_send_message(ch, method, answer, 'public', option_priority)
            log_info("There isn\'t posts data in post at {}, continue project flow", username)
            return

        try:
            temp = [{'location': d['location'], 'created': d['created'], 'code': d['code']} for d in posts if
                    'location' in d]
            locations = []
            for index, item in enumerate(temp):
                try:
                    if item['location']:
                        locations.append(item)
                except Exception as err:
                    print(item)
                    raise

            locations = [{'id': d['location']['id'], 'created': d['created'], 'code': d['code']} for d in locations if
                         'id' in d['location']]

        except Exception as err:
            log.exception(err, 'Number of posts is {}, username {}'.format(len(posts), username))
            raise

        if len(locations) > 0:

            log_info('Number of not null location is {}, locations is {}'.format(len(locations), len(locations)),
                     username)
            log_info('Store posts data to mysql', username)

            # Get exist location in DB
            try:
                account_location = mysql.get_row_form_table_by_field_array('account_location', [username], 'username')[
                    0]
                account_location['location_post'] = json.dumps([]) if not account_location['location_post'] else \
                    account_location['location_post']
            except Exception:
                account_location = {'location_post': json.dumps([])}

            log_info('Account location {}'.format(len(account_location)), username)

            # summ new and old locations (update)
            locations = locations + json.loads(account_location['location_post'])

            # unique codes
            try:
                codes = set([d['code'] for d in locations if d is not None and 'code' in d])
            except Exception as err:
                log.exception('Location is {}', locations)
                raise

            locations_unique = []
            for item in locations:
                if item:
                    if 'code' in item:
                        if item['code'] in codes:
                            locations_unique.append(item)
                            codes.remove(item['code'])

            locations_ids = [int(d['id']) for d in locations_unique if d['id'] is not None]
            log_info('Ids is {}'.format(len(locations_ids)), username)

            # todo: get username and locations_ids and check in Redis
            # if there is/are locations is not in Redis - send request to "public" queue to get
            # locations data by location id. Exist location add to mysql.
            # task from
            client = redis_location.get_redis_clent()

            in_redis = get_elements_from_redis(client, locations_ids)
            not_in_redis = [x for x in locations_ids if x not in in_redis.keys()]
            log_info('Elements in redis {}, not in redis {}'.format(len(in_redis), len(not_in_redis)), username)

            if len(not_in_redis):

                log_info('Elements not in redis {}'.format(not_in_redis), username)

                mysql.update_table_all_values_by_key('account_location', {'username': username,
                                                                          'location_post': json.dumps(
                                                                              locations_unique)},
                                                     {'username': 'username', 'location_post': 'location_post'})

                # Send to public get locations and return back here
                only_location_check_project_flow = [amqp_request_f.request_get_location_info('', option_priority)]

                # Last message to queu to return back to "location" queue
                project_flow = [amqp_request_f.request_get_location_info('location', option_priority)] + [
                    project_flow_body] + project_flow

                ch.basic_ack(delivery_tag=method.delivery_tag)

                # split location by part to quick check
                parts = custom_f.chunkIt(list(set(not_in_redis)), int(len(list(set(not_in_redis))) / 10) + 1)

                # Send task to get locations from locations_ids. We send n-1 message to get location ids and 1 to get locations with return back queue
                # It will allow us do not repeat locations ids to check multiple times
                for i, item in enumerate(parts):
                    if i == len(parts) - 1:
                        answer = amqp_request_f.answer_default(project_flow, '', item)
                        amqp_send_message(ch, method, answer, 'info', option_priority)
                    else:
                        answer = amqp_request_f.answer_default(only_location_check_project_flow, '', item)
                        amqp_send_message(ch, method, answer, 'info', option_priority)

            else:
                log_info('All data in Redis exist, let count summary and add it to DB', username)
                log_info('Locations {}'.format(locations_unique), username)

                result_post = []

                for item in locations_unique:
                    try:
                        location_data = custom_f.generate_data_from_redis_location_string(in_redis[int(item['id'])],
                                                                                          int(item['id']))
                    except Exception as err:
                        log_info('Accounts in redis is {}'.format(in_redis), username)
                        log.exception('Error by {}'.format(item))
                        raise
                    result_post.append(location_data)

                locations_analysis = location_f.analyze_account_locations_to_result_post(result_post)

                log_info('Location analysis result is {}'.format(locations_analysis), username)

                mysql.update_table_all_values_by_key('account_location',
                                                     {'username': username,
                                                      'location_post': json.dumps(locations_unique),
                                                      'result_post': json.dumps(result_post),
                                                      'summary': json.dumps(locations_analysis['all_country_array']),
                                                      'summary_city': json.dumps(locations_analysis['all_city_array'])
                                                      },
                                                     {'username': 'username', 'result_post': 'result_post',
                                                      'location_post': 'location_post', 'summary': 'summary',
                                                      'summary_city': 'summary_city'})

                if routing_return:
                    log_info("Getting location from posts complete, send to queue {}".format(routing_return), username)

                ch.basic_ack(delivery_tag=method.delivery_tag)

                if routing_return:
                    # raise Exception('retrun')
                    amqp_send_message(ch, method, amqp_request_f.answer_default(project_flow, username),
                                      routing_return, option_priority)

                if option_store_redis:
                    # Store account information about country/city to Redis
                    step1 = amqp_request_f.request_append_account_data_to_redis('', option_priority, 'account',
                                                                                option_user_id, token, username)
                    data = {
                        'city': 1 if not len(locations_analysis['main_city']) else {
                            'n': locations_analysis['main_city'],
                            'p': locations_analysis['main_city_percent']},
                        'country': 1 if not len(locations_analysis['main_country']) else {
                            'n': locations_analysis['main_country'],
                            'p': locations_analysis['main_country_percent']}
                    }
                    amqp_send_message(ch, method, amqp_request_f.answer_default([step1], username, [], data), 'redis',
                                      option_priority)

                # time.sleep(60)
        else:
            log_info("There isnt location in posts. Account data posts is {}, posts from s3 {}".format(
                account['posts'], len(posts)), username)

            ch.basic_ack(delivery_tag=method.delivery_tag)

            # send to mysql db empty location_post
            mysql.update_table_all_values_by_key('account_location',
                                                 {'username': username, 'location_post': json.dumps([]),
                                                  'summary': json.dumps([]), 'summary_city': json.dumps([])},
                                                 {'username': 'username', 'location_post': 'location_post',
                                                  'summary': 'summary', 'summary_city': 'summary_city'})

            if routing_return:
                # raise Exception('retrun')
                amqp_send_message(ch, method, amqp_request_f.answer_default(project_flow, username),
                                  routing_return, option_priority)

            if option_store_redis:
                # Store account information about country/city to Redis
                step1 = amqp_request_f.request_append_account_data_to_redis('', option_priority, 'account',
                                                                            option_user_id, token, username)
                data = {'city': 1, 'country': 1}

                amqp_send_message(ch, method, amqp_request_f.answer_default([step1], username, [], data), 'redis',
                                  option_priority)

        if routing_return:
            #raise Exception('return')
            amqp_send_message(ch, method, amqp_request_f.answer_default(project_flow, username),
                              routing_return, option_priority)

        log_info('Time: {}'.format(time.time() - timestart), username)

        return

    elif task == 'get-location-bio-for-account':

        ch.basic_ack(delivery_tag=method.delivery_tag)

        return

    else:
        ch.basic_ack(delivery_tag=method.delivery_tag)
        # Default message "task not found"
        answer = amqp_request_f.request_return_task_not_found(instance_type, instance, token, message)
        amqp_send_message(ch, method, answer, '', 1, True)

    if mysql:
        mysql.close()

    raise Exception('debug')

    # time.sleep(1)


#
# Get element in redis with stat
#
def get_elements_from_redis(client, ids):
    in_redis = {}

    p = client.pipeline()
    for id in ids:
        p.get(int(id))
    result = p.execute()

    if len(result) > 0:
        for index, item in enumerate(result):
            if item:
                in_redis[ids[index]] = item

    return in_redis


def looping_results(data, key, index, export_key, func_name=''):
    result = []
    error404 = []
    recheck = []
    proxy_errors_counter = 0

    # Looping results
    if isinstance(data, list):
        for item in data:
            try:
                if 'error' in item[key]:
                    if item[key]['error'] == 404:
                        #  Element is not created or deleted
                        log.warning('404 error ' + str(item[key][index]))
                        error404.append(item[key][index])
                    elif item[key]['error'] == 408:
                        log.error(
                            '{' + '"message":"Timeout exception - need to change proxy to another","message_tag":""' + '}')
                        proxy_errors_counter += 1
                    elif item[key]['error'] == 429:
                        recheck.append(item[key][index])
                        log.error('429 error ' + str(item[key][index]))
                        proxy_errors_counter += 1
                    elif item[key]['error']:
                        log.error('Error during execution %s', item[key])
                        proxy_errors_counter = max(
                            item[key]['proxy_error_counter'] if 'proxy_error_counter' in item[
                                key] else 0,
                            proxy_errors_counter)
                        proxy_errors_counter += 1
                        recheck.append(item[key][index])
                    else:
                        # Compeleted getting results
                        item[key]['error'] = 0
                        item[key]['status'] = 2

                        export = {}
                        for i in export_key:
                            export[i] = item[i]
                        # Append to result array
                        result.append(export)
                else:
                    result.append(item)
            except Exception as err:
                log.error(
                    '{' + '"message":"Error in looping results error = {}, item = {},func_name{}, trace {}","message_tag":"public-looping-result"'.format(
                        err, item, func_name, traceback.format_exc()) + '}')
                quit()

    return {'complete': result, 'error404': error404, 'recheck': recheck, 'proxy_errors': proxy_errors_counter}


"""
def check_ids_for_exist_in_redis(ids):
    result = []
    not_id = 0
    for item in ids:
        if isinstance(item, dict):
            value = 0
            try:
                if 'id' in item:
                    value = redis.get_value_by_int(int(item['id']), buckets_keys)
            except Exception as err:
                log.error(
                    'Error check_ids_for_exist_in_redis id={}, bucket size={}, error={}'.format(item['id'],
                                                                                                buckets_keys, err))

            if value and 'u' in item:
                result.append(item['u'])
        else:
            not_id += 1

    if not_id:
        log.info('List elements without ids = %s', not_id)

    return result
"""


def amqp_message_body_add_new_data_to_redis(usernames, project_id, username='', audit_stage_3=False):
    get_redis_data = {
        'task': 'get-private-list-accounts-info',
        "use_proxy": True,
        "mysql_instance": False,
        "store_json": False,
        "force_check": False,
        "store_redis": True,
        "export_full": False,
        "priority": 10,
        "date": datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        "return_queue": 'audit.final' if audit_stage_3 else ''
        # "return_queue": 'error'
    }

    return_to_audit_stage_3 = {
        'task': 'full-audit-check-redis-complete',
        "project_id": project_id,
        'token': username,
        "priority": 10,
        "date": datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        "return_queue": ""
    }

    if audit_stage_3:
        steps = [get_redis_data, return_to_audit_stage_3]
    else:
        steps = [get_redis_data]

    message = {
        "project_flow": steps,
        "token": '',
        "completed": usernames
    }

    return json.dumps(message)


def get_ua_dict():
    content = custom_f.get_file_content(config.USERAGENTS_PATH)
    data = json.loads(content[0])
    return data


if __name__ == '__main__':

    test_url = 'https://www.instagram.com'

    instance = 1
    instance_type = 'location'
    amqp_instance = 1
    virtual_host = 'prod'
    exchange = 'instagram-exchange'
    debug = False

    if 'instance' in os.environ:
        instance = int(os.environ['instance'])

    if 'amqp' in os.environ:
        amqp_instance = int(os.environ['amqp'])

    if 'virtual_host' in os.environ:
        virtual_host = str(os.environ['virtual_host'])

    if 'exchange' in os.environ:
        exchange = str(os.environ['exchange'])

    if 'debug' in os.environ:
        debug = bool(os.environ['debug'])

    if 'docker' in os.environ:
        fluent_host = config.LOG_FLUENT_HOST_DOCKER
    else:
        fluent_host = config.LOG_FLUENT_HOST

    if len(sys.argv) > 1:
        instance = int(sys.argv[1])

    if len(sys.argv) > 3:
        virtual_host = str(sys.argv[3])

    if len(sys.argv) > 2:
        amqp_instance = int(sys.argv[2])

    if len(sys.argv) > 4:
        exchange = str(sys.argv[4])

    if len(sys.argv) > 5:
        debug = bool(sys.argv[5])

    custom_f = CustomFunction(instance, debug)

    # Set logger
    log = custom_f.set_logging(instance, None, 'fluent', instance_type,
                               'application.assem.{}.{}'.format(instance_type, virtual_host),
                               {'host': fluent_host, 'port': config.LOG_FLUENT_PORT},
                               'INFO' if not debug else 'DEBUG', ['s3transfer', 'pika'], ['botocore', 'urllib3'],
                               virtual_host)

    location_f = CustomLocationFunction(log)
    amqp_request_f = AMQPRequests(log)

    ua = get_ua_dict()

    log.info("Start location.py")

    # routing_request_config = config.AMQP_QUEUE_AUDIT_2
    routing_request_config = 'location'
    routing_return_audit_requirements = config.AMQP_QUEUE_AUDIT_REQ
    routing_return_config = config.AMQP_QUEUE_APPLICATION
    routing_error_config = config.AMQP_QUEUE_ERROR

    # Get AQMP details
    amqp_creds = custom_f.get_amqp_by_key(amqp_instance)

    # Set RabbitMQ
    Amqp = AMQP(
        exchange,
        routing_request_config,
        virtual_host,
        amqp_creds['host'],
        amqp_creds['port'],
        amqp_creds['login'],
        amqp_creds['pass'],
        amqp_callback,
        log,
        debug
    )

    s3_posting_bucket = 'posting-bucket'

    try:

        # Star Insatgram class
        Instagram = InstagramParsing(20, log, instance, debug)

        # setttings
        Instagram.config_set_ua_local(True, ua)

        # Start Redis locations
        redis_location = RedisClass(log, config.REDIS_LOCATION_HOST, config.REDIS_LOCATION_PORT,
                                    config.REDIS_LOCATION_PASSWORD)

        # Start s3
        s3 = S3(log, config.AWS_S3_ACCESS_KEY_ID, config.AWS_S3_ACCESS_KEY_SECRET)
        s3.connect_client()

        # Store posts on local
        path_posts = config.INSTAGRAM_POSTS_LOCAL_STORE_PATH
        path_followers = '/home/endemic/storage/followers'

    except Exception as err:
        log.error(err)
        quit()

    try:
        Amqp.run()
    except KeyboardInterrupt:
        Amqp.stop()
        quit()
    except Exception as err:
        log.error("Global raise",err)
        time.sleep(60)
        quit()
