# assem-instagram

### Setup

Add plugins
curl -fsSL https://raw.githubusercontent.com/CWSpear/local-persist/master/scripts/install.sh | sudo bash

Create storage
docker volume create --driver local-persist -o mountpoint=/home/endemic/mnt/ --name=Storage

Create image
docker build -t public:latest .

### Start 

/usr/bin/docker run --name=private_%i --env vhost=test --env instance=%i --env amqp=1 --env  exchange=instagram-exchange -v storage:/app/storage private:latest  
