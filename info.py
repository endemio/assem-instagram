import config
import logging
import json
import time
import datetime
import os
import requests
import http.cookiejar
import random
import sys
import ast


from module.instagram.parsing import InstagramParsing
from module.mysql.main import MysqlORM
from module.rabbitmq.amqp import AMQP
from module.redis.main import RedisClass
from module.aws.s3 import S3
from module.custom.functions import CustomFunction
from module.custom.amqp_requests import AMQPRequests

def log_info(message, username, tag=None):
    log.info({
        'message': message,
        'username': username,
        'link': custom_f.get_link(),
        'token': custom_f.get_token(),
        'virtual_host': virtual_host,
        'message_tag': tag
    })


def log_warning(message, username, tag=None):
    log.warning({
        'message': message,
        'username': username,
        'link': custom_f.get_link(),
        'token': custom_f.get_token(),
        'virtual_host': virtual_host,
        'message_tag': tag
    })


def amqp_send_message(ch, method, answer, routing_key, priority=1, error=False, delivery_mode=2):
    try:
        json_object = json.loads(answer)
    except:
        answer = json.dumps(answer, default=lambda o: o.__dict__, indent=4)
        pass

    routing = routing_key if not error else routing_error_config

    try:
        ch.basic_publish(
            exchange=Amqp.get_exchange(),
            routing_key=routing,
            body=answer,
            properties=Amqp.basic_properties(delivery_mode, priority))
    except Exception as err:
        log.exception("Error while sending message to result queue " + str(err))
        quit()

    log_info("Send message to {} queue, message len =  {}, text = {}".format(routing, len(answer), json.loads(answer)),
             None)


def amqp_callback(ch, method, properties, message):
    message = (message.decode('ascii'))
    timenow = str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

    try:
        json_body = json.loads(message)
    except Exception as e:
        log.error(' Get exception at Body message JSON parsing ' + str(e))
        # Acking message from AMQP
        ch.basic_ack(delivery_tag=method.delivery_tag)
        answer = '{' + '"task":"task-parsing-error","datetime":"{}","status":false,"error":"{}","received":"{}"'.format(
            str(timenow), ' Get exception at JSON parsing ' + str(e), message) + '}'
        amqp_send_message(ch, method, answer, '',1, True)
        return

    # Project flow
    project_flow = list(json_body['project_flow']) if 'project_flow' in json_body else {}
    if not project_flow:
        log.error('Project flow not exist')
        ch.basic_ack(delivery_tag=method.delivery_tag)
        # Default answer
        answer = '"task":"project-flow-error","status":false,"datetime":"{}","error":"No tasks found"'.format(
            timenow)
        amqp_send_message(ch, method, answer, '', 1, True)
        return
    else:
        body = project_flow.pop(0)

    # Task
    task = str(body['task']) if 'task' in body else ''

    # Task
    task = str(body['task']) if 'task' in body else ''

    # Tokens
    token = str(json_body['token']) if 'token' in json_body else ''
    custom_f.set_token(token)

    link = str(body['link']) if 'link' in body else ''
    custom_f.set_link(link)

    log_info('Get body {}, task {}'.format(json_body, task), None, None)

    # Main options
    option_use_proxy = bool(body['use_proxy']) if 'use_proxy' in body else True
    option_priority = int(body['priority']) if 'priority' in body else 0
    option_mysql_instance = int(body['mysql_instance']) if 'mysql_instance' in body else 0

    # Check option
    option_force_check = bool(body['force_check']) if 'force_check' in body else False
    option_get_posts_data = bool(body['get_posts_data']) if 'get_posts_data' in body else False
    option_get_account_data = bool(body['get_accounts_data']) if 'get_accounts_data' in body else True
    option_get_engagements_data = bool(body['get_engagements_data']) if 'get_engagements_data' in body else False

    # Store options
    option_store_local_json = bool(body['store_json']) if 'store_json' in body else False
    option_store_redis = bool(body['store_redis']) if 'store_redis' in body else False
    option_store_s3 = bool(body['store_s3']) if 'store_s3' in body else False

    # AMQP options
    routing_return = str(body['return_queue']) if 'return_queue' in body else routing_return_config

    # Export options
    option_export_random = int(body['export_random']) if 'export_random' in body else 0
    option_export_full = bool(body['export_full']) if 'export_full' in body else False
    option_export_mysql_keys = dict(body['export_mysql_keys']) if 'export_mysql_keys' in body else dict()
    option_export_mysql_table = str(body['export_mysql_table']) if 'export_mysql_table' in body else 'account'

    # Set information to parser for storing data to local server
    if option_store_local_json:
        today = datetime.datetime.today().strftime('%Y-%m-%d')
        Instagram.config_set_local_storing(True, config.INSTAGRAM_LOCAL_STORE_PATH + '/' + str(today))
    else:
        Instagram.config_set_local_storing(False)

    # Set proxy to parser
    proxy_errors_counter = 0
    if option_use_proxy:
        proxy_path = get_proxy()
        Instagram.config_set_proxy(True, {'http': proxy_path, 'https': proxy_path})
    else:
        proxy_path = ''
        Instagram.config_set_proxy(False)

    # Set getting data from account (Info, Posts, Count ER)
    Instagram.config_set_export_dict(option_get_account_data, option_get_posts_data, option_get_engagements_data)

    # Set direct Mysql connection
    mysql = None
    if option_mysql_instance:
        # Get mysql data from config by key
        item = get_mysql_by_key(option_mysql_instance)
        mysql = MysqlORM(log,True)
        mysql.config_set_mysql(True, item['host'], item['port'], item['login'], item['pass'], item['db'])

    # -----------------------------------------------------------------------------------------------------------------
    #  Get instagram account info and store it in json (if 'store-json' = true)
    #  and send data to queue 'result' from amqp-message. If set redis - store in redis
    #

    if task == 'get-account-info':

        log.debug('# Start get-account-info case')

        # Get input list
        if not isinstance(json_body['completed'], list):
            input = list(filter(None, ast.literal_eval(json_body['completed'])))
        else:
            input = json_body['completed']

        completed = []
        error404 = []

        # todo: additional check in redis
        ids = input

        log.debug(ids)

        # If ids is empty - ending task
        if len(ids) == 0:
            if debug:  log.error('Send message to notice about empty ids list')

            # Acking message from AMQP
            ch.basic_ack(delivery_tag=method.delivery_tag)

            # Send message to queue
            answer = '{' + '"task":"empty-ids-list-error","datetime":"{}","token":"{}",'.format(timenow, token) + \
                     '"message":"There is\'nt ids to check in task {}","message-get":"{}"'.format(task,message) + '}'

            # Send error message to error queue
            amqp_send_message(ch, method, answer, '',1, True)

        # If there are ids in list
        else:

            option_store_s3 = True

            if option_store_s3:
                s3 = S3('', config.AWS_S3_ACCESS_KEY_ID, config.AWS_S3_ACCESS_KEY_SECRET)
                s3.connect_client()
            else:
                s3 = None

            n = config.INSTAGRAM_CHECK_INFO_IN_REQUEST

            while len(ids):
                ids_check = ids[:n]
                log.debug('List of ids to check' + str(ids_check))

                data = []

                ids_remove = []
                if option_store_s3:
                    # Check if exists accounts info with same data
                    for id in ids_check:
                        filename = str(str(id) + '-full.json')
                        log.debug('Filename is %s',filename)
                        if s3.check_file_exist('account-info-bucket', filename) and not option_force_check:
                            log.debug('Info file exists')
                            s3_object = s3.get_object_content('account-info-bucket', filename)
                            json_string = s3_object
                            try:
                                json_dict = json.loads(json_string)
                                data.append(Instagram.fetch_export_account_info_from_json(json_string,
                                                                                          json_dict['user'][
                                                                                              'username']))
                                ids_remove.append(id)
                            except Exception as exn:
                                log.exception(json_string)

                log.debug('List of ids to check from queue' + str(ids_check))
                log.debug('List of ids to remove' + str(ids_remove))
                ids_check = [x for x in ids_check if x not in ids_remove]
                log.debug('List of ids to check' + str(ids_check))
                #log.debug('Data = %s',data)

                # Check if only we have accounts info in ids_check
                if len(ids_check) > 0:
                    try:
                        data += Instagram.get_instagram_info_for_accounts_by_username(ids_check)
                    except Exception as e:
                        log.exception('Instagram.get_instagram_info_for_accounts_by_username error' + str(e))

                        if len(routing_return) > 0:
                            # Send results to result queue
                            for item in completed:
                                amqp_send_message(ch, method, answer_account_info_success(item, error404, project_flow,
                                                                                          option_export_random,
                                                                                          item['account']['username']),
                                                  routing_return,option_priority)

                        # Send error message to error queue
                        answer = '{' + '"task":"get-data-from-web-error","datetime":"{}","status":false,"error":"{}",' + \
                                 '"username":"{}"'.format(timenow,
                                                          ' Get exception get_instagram_data_for_accounts_by_username ' +
                                                          str(e), str(ids_check)) + '}'
                        amqp_send_message(ch, method, answer, '', 1, True)
                        raise Exception('Restart by bad parsing')  # Raise to return current message back to AMQP

                # Looping results
                export_key = ['edge']
                result = looping_results(data, 'edge', 'username', export_key)
                completed += result['complete']
                error404 += result['error404']

                # Store mysql error 404
                if len(result['error404']) > 0 and option_mysql_instance:
                    for item in result['error404']:
                        # Add to mysql
                        mysql.insert_into_table_increase_field(option_export_mysql_table, {'username': item, 'error': 1}, 'error')

                # Check for proxy errors
                if proxy_errors_counter > config.PROXY_ERROR_LIMIT:
                    send = {'proxy': proxy_path.split('//')[1], 'error': 'ProxyErrorNumberExceed'}
                    r = requests.post('https://proxy.endemic.ru/api/broke-proxy',
                                      headers=get_post_header_proxy(),
                                      json=send)

                    if len(routing_return) > 0:
                        option_export = True if int(option_export_random) + int(option_export_full) > 0 else False
                        for item in completed:
                            amqp_send_message(ch, method, answer_account_info_success(item, error404, project_flow,
                                                                                      option_export,
                                                                                      item['edge']['username']),
                                              routing_return,option_priority)

                    raise Exception('Restart by bad proxy')  # Restart app by exceeded errors number

                option_export_mysql_keys_info = {
                    "username" : "username",
                    "status" : "status",
                    "media" : "posts",
                    "public_email" : "public_email",
                    "category" : "category"
                }

                # Store mysql completed data
                if len(result['complete']) > 0 and option_mysql_instance and option_export_mysql_keys:
                    #print(option_export_mysql_keys)
                    log.debug('Store new account data to mysql, type = %s', option_mysql_instance)
                    for item in result['complete']:
                        #print(item['edge']['data'])
                        # Add to mysql
                        mysql.update_table_all_values_by_key(option_export_mysql_table, item['edge']['data'], option_export_mysql_keys)
                        mysql.update_table_all_values_by_key('account', item['edge']['data'], option_export_mysql_keys_info)

                # Export to s3 and local store
                log.debug('Storing in s3 is ' + str(option_store_s3))
                if len(result['complete']) > 0 and option_store_s3:
                    log.debug(result)
                    log.debug(ids_check)
                    for item in result['complete']:
                        log.debug(int(item['edge']['data']['user_id']))
                        if str(item['edge']['data']['user_id']) in ids_check:

                            #print(item['edge']['json'])
                            filename = str(item['edge']['data']['user_id'])+'-full.json'
                            log.debug('Storing at S3 file to ' + filename)
                            path = store_results_to_file(path_posts, filename, item['edge']['json'])
                            status = s3.upload_file(path, 'account-info-bucket', filename)
                            if status and not option_store_local_json:
                                os.remove(path)
                            if debug:
                                log.debus('Storing at S3 file complete')
                        else:
                            log.debug('We do not storing duplicates!!!')

                # Sleep between checks
                sleep_time = config.INSTAGRAM_CHECK_INFO_PARSING_DELAY * len(
                    ids_check) / n
                log.debug('Count sleep time. Sleep time = ' + str(sleep_time))
                time.sleep(sleep_time)

                # Remove first part of ids from list
                ids += result['recheck']
                ids = ids[n:]
                ids = list(set(ids))

            # Return proxy to list with status "1" if it still working
            data = {'proxy': proxy_path.split('//')[1]}
            r = requests.post('https://proxy.endemic.ru/api/return-proxy',
                              headers=get_post_header_proxy(),
                              json=data)

            # Usernames ended - wait for new list
            log.debug('Usernames ended, waiting for new message in queue')
            ch.basic_ack(delivery_tag=method.delivery_tag)

            # Send results to result queue
            if len(routing_return) > 0:
                option_export = True if int(option_export_random) + int(option_export_full) > 0 else False
                for item in completed:
                    item['edge']['json'] = {}
                    amqp_send_message(ch, method,
                                      answer_account_info_success(item, error404, project_flow, option_export,
                                                                  item['edge']['username']),
                                      routing_return,option_priority)

    elif task == 'get-location-info':

        # Get input list
        locations = []
        if not isinstance(json_body['completed'], list):
            locations = list(filter(None, ast.literal_eval(json_body['completed'])))
        else:
            locations = json_body['completed']

        completed = []
        error404 = []

        # If usernames is empty - ending task
        if len(locations) == 0:
            log_warning("Send message to notice about empty locations list", None)
        else:
            n = int(config.INSTAGRAM_CHECK_INFO_IN_REQUEST)

            log_info('Start getting info {} ids, n = {}'.format(locations, n),None)

            while len(locations):

                locations_check = locations[:n]

                hmsets = {}

                log.debug('Start get location from location list, number location in iteration is {}'.format(
                    len(locations_check)))

                try:
                    data = Instagram.get_location_data_for_token(locations_check)
                except Exception as e:
                    log.exception('Instagram.get_location_data_for_token error')

                    # Send current exists data
                    if len(routing_return) > 0:
                        option_export = True if int(option_export_random) + int(option_export_full) > 0 else False

                        # Send exist data in completed if we get error during get location
                        amqp_send_message(ch, method,
                                          amqp_request_f.answer_location_list_success(completed, error404, project_flow,
                                                                                      option_export, token),
                                          routing_return, option_priority)

                    # Send error message to error queue
                    answer = '{' + '"task":"get-data-from-web-error","datetime":"{}","status":false,"error":"{}",' + \
                             '"username":"{}"'.format(timenow,
                                                      ' Get exception get_instagram_data_for_accounts_by_username ' +
                                                      str(e), str(locations_check)) + '}'
                    amqp_send_message(ch, method, answer, '', 1, True)
                    raise Exception('Restart by bad parsing')  # Raise to return current message back to AMQP

                # Looping results
                result = looping_results(data, 'location', 'token', ['location'])
                completed += result['complete']
                error404 += result['error404']

                # Store REDIS completed data
                if len(result['complete']) > 0 and option_store_redis:
                    for item in result['complete']:
                        string_location = custom_f.generate_string_for_redis_location(item['location']['data'])
                        hmsets[item['location']['data']['id']] = string_location
                        if len(locations_check) == 1:
                            if locations_check[0] != item['location']['data']['id']:
                                hmsets[locations_check[0]] = string_location

                # Store REDIS completed data
                if len(result['error404']) > 0 and option_store_redis:
                    log.debug(result['error404'])
                    for item in result['error404']:
                        hmsets[item] = 'ZZ:0:0'

                log.debug(hmsets)

                if len(hmsets) > 0:
                    log.debug("Add data to redis location {}".format(json.dumps(hmsets)))

                    redis_location.insert_dict_key_value(hmsets)

                # Stop working in we reached proxy_error limit
                if proxy_errors_counter > config.PROXY_ERROR_LIMIT:
                    log_warning("Send request to update proxy status to 0", None)
                    data = {'proxy': proxy_path.split('//')[1], 'error': 'ProxyErrorNumberExceed'}
                    r = requests.post('https://proxy.endemic.ru/api/broke-proxy',
                                      headers=custom_f.get_post_header_proxy(),
                                      json=data)
                    # If result routing key not null - send current data
                    if len(routing_return) > 0:
                        amqp_send_message(ch, method,
                                          amqp_request_f.answer_location_list_success(completed, error404, project_flow,
                                                                                      option_export_random, token),
                                          routing_return, option_priority)
                    raise Exception('Restart by bad proxy')

                # Sleep between checks
                sleep_time = config.INSTAGRAM_CHECK_USERNAMES_PARSING_DELAY * len(
                    locations_check) / n
                log.debug('{' + '"message":"Count sleep time. Sleep time = ' + str(sleep_time))
                time.sleep(sleep_time)

                # Remove first part of locations from list
                locations += result['recheck']
                locations = locations[n:]
                locations = list(set(locations))

            log_info("Locations {} ended, waiting for new message in queue".format(json_body['completed']), None)

            # Send getting info to result queue
        ch.basic_ack(delivery_tag=method.delivery_tag)
        option_export = True if int(option_export_random) + int(option_export_full) > 0 else False
        if routing_return:
            amqp_send_message(ch, method,
                              amqp_request_f.answer_location_list_success(completed, error404, project_flow,
                                                                          option_export,
                                                                          token),
                              routing_return, option_priority)

    else:
        ch.basic_ack(delivery_tag=method.delivery_tag)
        # Default message "task not found"
        answer = '{' + '"task":"info-error","status":false,"token":"{}","datetime":"{}","error":"No task found","message":"{}"'.format(
            token, timenow,message) + '}'
        amqp_send_message(ch, method, answer, '',1, True)

    if mysql:
        mysql.close()


def answer_account_info_success(data, error404, project_flow, full_export, token='',
                                timenow=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")):
    return '{' + \
           '"project_flow":{},'.format(json.dumps(project_flow)) + \
           '"datetime":"{}",'.format(timenow) + \
           '"token":"{}",'.format(token) + \
           '"status":true,' + \
           '"completed":"{}",'.format(data['edge']['username']) + \
           '"error404":{},'.format(json.dumps(error404)) + \
           '"data":{}'.format(json.dumps(data['edge']) if full_export else {}) + \
           '}'


def looping_results(data, key, index, export_key):
    result = []
    error404 = []
    recheck = []
    proxy_errors_counter = 0

    # Looping results
    if isinstance(data, list):
        for item in data:
            try:
                if 'error' in item[key]:
                    if item[key]['error'] == 404:
                        #  Element is not created or deleted
                        log.error('404 error ' + str(item[key][index]))
                        error404.append(item[key][index])
                    elif item[key]['error'] == 408:
                        log.error('Timeout exception - need to change proxy to another')
                        proxy_errors_counter += 1
                    elif item[key]['error'] == 429:
                        recheck.append(item[key][index])
                        log.error('429 error ' + str(item[key][index]))
                        proxy_errors_counter += 1
                    elif item[key]['error']:
                        log.error('Error during execution %s', item[key])
                        proxy_errors_counter = max(
                            item[key]['proxy_error_counter'] if 'proxy_error_counter' in item[
                                key] else 0,
                            proxy_errors_counter)
                        proxy_errors_counter += 1
                        recheck.append(item[key][index])
                    else:
                        # Compeleted getting results
                        item[key]['error'] = 0
                        export = {}
                        for i in export_key:
                            export[i] = item[i]
                        # Append to result array
                        result.append(export)
                else:
                    result.append(item)
            except Exception as e:
                log.error('Error in looping results :' + str(e))

    return {'complete': result, 'error404': error404, 'recheck': recheck, 'proxy_errors': proxy_errors_counter}


def get_usernames_list_by_options(input, option_force_check, option_store_redis):
    usernames = []

    # Check if data exist in redis
    if not option_force_check:  # Check for "Force" checking
        log.debug('Not forced check')
        if option_store_redis:  # Check if we use Redis
            log.debug('Checking in redis')
            rows = check_ids_for_exist_in_redis(input)  # Get data from redis
            log.debug('Rows in reds = %s', len(rows))
            for item in input:
                if isinstance(item, dict):  # Check if dict or list
                    if item['u'] not in rows:
                        usernames.append(item['u'])
                else:
                    usernames.append(item)
        else:
            for item in input:
                if isinstance(item, dict):  # Check if dict or list
                    usernames.append(item['u'])
                else:
                    usernames.append(item)
    else:
        for item in input:
            if isinstance(item, dict):  # Check if dict or list
                usernames.append(item['u'])
            else:
                usernames.append(item)
    return usernames


def get_proxy(key = 0):
    proxy_status = False
    while not proxy_status:
        # Set proxy
        proxy_path = get_proxy_by_key(instance).rstrip() if not key else get_proxy_by_key(key).rstrip()
        proxy_data = check_if_proxy_is_alive(proxy_path)
        log.debug('Proxy => ' + str(proxy_data))
        if not proxy_data['error']:
            # proxy_status = True
            # proxy_path = proxy_data['proxy']
            return proxy_data['proxy']
        else:
            # Send data to say that this proxy is bad
            data = {'proxy': proxy_data['proxy'], 'error': proxy_data['error']}
            requests.post('https://proxy.endemic.ru/api/broke-proxy',
                          headers=get_post_header_proxy(),
                          json=data)


def check_if_proxy_is_alive(proxy):
    #print(str("Trying HTTP proxy %21s \t\t" % (proxy)))

    cj = http.cookiejar.CookieJar()

    try:
        t1 = time.time()
        r = requests.get(test_url, proxies={'http': proxy, 'https': proxy}, cookies=cj, timeout=(5, 20))
        t2 = time.time()
    except requests.exceptions.ReadTimeout:
        print('Oops. Read timeout occured')
        return {'proxy': proxy, 'error': 'ReadTimeout'}
    except requests.exceptions.ConnectTimeout:
        print('Oops. Connection timeout occured!')
        return {'proxy': proxy, 'error': 'ConnectTimeout'}
    except requests.exceptions.RequestException as e:
        print('Oops. Retry error timeout occured! %s', e)
        return {'proxy': proxy, 'error': 'RequestException'}
    except Exception as e:
        print("%s (%s)" % ('FAIL', str(e)))
        return {'proxy': proxy, 'error': 'AllException'}

    print(" Response time: %d, length=%s" % (int((t2 - t1) * 1000), str(len(r.content))))
    return {'proxy': proxy, 'error': False}


def store_results_to_file(folder, filename, data):
    try:
        with open(os.path.join(folder, filename), 'w', encoding='utf-8') as f:
            f.write(str(data) + "\n")
        return os.path.join(folder, filename)
    except Exception as e:
        log.error(e)
        raise


'''

def add_element_to_redis_export_dict(hmsets, user_id, string):
    user_id = int(user_id)
    bucket_index = int(user_id / int(buckets_keys))
    index = user_id - bucket_index * buckets_keys
    if bucket_index not in hmsets:
        hmsets[bucket_index] = {}
    hmsets[bucket_index][index] = string
    return hmsets
'''


def count_redis_index(user_id):
    user_id = int(user_id)
    bucket_index = int(user_id / int(buckets_keys))
    index = user_id - bucket_index * buckets_keys
    return [bucket_index, index]


def generate_string_for_redis(data):
    diff = datetime.datetime.today() - datetime.datetime.strptime("01/03/2019", "%d/%m/%Y")
    return str(int(1)) + ':' + \
           str(diff.days) + ':' + \
           str(data['posts_count']) + ':' + \
           str(data['follow']) + ':' + \
           str(data['followers']) + ':' + \
           str(int(data['business'])) + ':' + \
           str(int(data['private']))


def check_ids_for_exist_in_redis(ids):
    result = []
    not_id = 0
    for item in ids:
        if isinstance(item, dict):
            value = 0
            try:
                if 'id' in item:
                    value = redis.get_value_by_int(int(item['id']), buckets_keys)
            except Exception as e:
                log.error(
                    'Error check_ids_for_exist_in_redis id=' + str(item['id']) + ' bucket size=' + str(buckets_keys))
                log.error(e)

            if value and 'u' in item:
                result.append(item['u'])
        else:
            not_id += 1

    if not_id:
        log.error('List elements without ids = %s', not_id)

    return result


def get_file_content(filepath):
    with open(filepath, 'r') as the_file:
        data = the_file.read()
        result = data.split('\n')
        return result


def get_mysql_by_key(key):
    list = get_file_content(config.SETTINGS_MYSQL_PATH)
    mysql = list[key - 1].split(':')
    return {'host': mysql[0], 'port': int(mysql[1]), 'login': mysql[2], 'pass': mysql[3], 'db': mysql[4]}


def get_amqp_by_key(key):
    list = get_file_content(config.SETTINGS_AMQP_PATH)
    data = list[key - 1].split(':')
    return {'host': data[0], 'port': int(data[1]), 'login': data[2], 'pass': data[3]}

def get_instagram_credentials_by_key(key):
    r = requests.post(config.CREDENTIAL_INSTAGRAM_ACCOUNTS_URL, headers=get_post_header_proxy(), json={'id': key})
    log.debug(r.content.decode('utf-8'))
    try:
        data = json.loads(r.content.decode('utf-8'))
    except Exception as e:
        log.error('JSON parsing error for string %s %s',r.content,e)
        raise
    return data


def get_proxy_by_key(key):
    if key < 80:
        r = requests.get(config.CREDENTIAL_INSTAGRAM_PROXY_URL)
        data = r.content.decode('utf-8')
        lines = data.split('\n')
        return lines[key - 1]
    else:
        # Get from proxy.endemic.ru
        r = requests.post('https://proxy.endemic.ru/api/new-proxy', headers=get_post_header_proxy(), json={'instance': key})
        try:
            content = json.loads(r.content)
            return 'http://' + content['proxy']['proxy']
        except Exception as e:
            log.error(e)
            log.error(r.content)
            quit()


def get_post_header_proxy():
    return {
        'Content-Type': 'application/json; charset=utf-8',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
        'accept-language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,cs;q=0.6,da;q=0.5,de;q=0.4,es;q=0.3,it;q=0.2,pt;q=0.1,zh-CN;q=0.1,zh;q=0.1',
        'cache-control': 'max-age=0',
        'referer': 'https://www.google.com/',
        'X-AUTH-TOKEN': config.PROXY_ENDEMIC_AUTH_TOKEN_KEY
    }


def get_ua_dict():
    content = get_file_content(config.USERAGENTS_PATH)
    data = json.loads(content[0])
    return data


def set_logging(key):
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%m-%d %H:%M:%S',
                        filename=config.LOG_PATH + '/info-' + str('{:03d}'.format(key)) + '.log',
                        filemode='a')  # Append, not rewrite
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')

    logging.getLogger("urllib3").setLevel(logging.WARNING)
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("requests.packages.urllib3.connectionpool").setLevel(logging.WARNING)

    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)
    return logging


if __name__ == '__main__':

    debug = False

    test_url = 'https://www.instagram.com'

    app = 'info'
    instance = 61
    amqp_instance = 1
    virtual_host = ''
    exchange = ''

    if 'instance' in os.environ:
        instance = int(os.environ['instance'])

    if 'amqp' in os.environ:
        amqp_instance = int(os.environ['amqp'])

    if 'virtual_host' in os.environ:
        virtual_host = str(os.environ['virtual_host'])

    if 'exchange' in os.environ:
        exchange = str(os.environ['exchange'])

    if 'debug' in os.environ:
        debug = bool(os.environ['debug'])

    if 'docker' in os.environ:
        fluent_host = config.LOG_FLUENT_HOST_DOCKER
    else:
        fluent_host = config.LOG_FLUENT_HOST

    if len(sys.argv) > 1:
        instance = int(sys.argv[1])

    if len(sys.argv) > 2:
        amqp_instance = int(sys.argv[2])

    if len(sys.argv) > 3:
        virtual_host = str(sys.argv[3])

    if len(sys.argv) > 4:
        exchange = str(sys.argv[4])

    if len(sys.argv) > 5:
        debug = bool(sys.argv[5])

    custom_f = CustomFunction(instance, debug)

    # Set logger
    log = custom_f.set_logging(instance, None, 'fluent', app,
                               'application.assem.{}.{}'.format(app, virtual_host),
                               {'host': fluent_host, 'port': config.LOG_FLUENT_PORT},
                               'INFO' if not debug else 'DEBUG', ['s3transfer', 'pika'], ['botocore', 'urllib3','requests'],
                               virtual_host)

    amqp_request_f = AMQPRequests(log)

    ua = get_ua_dict()

    routing_request_config = config.AMQP_QUEUE_INFO
    routing_return_config = config.AMQP_QUEUE_APPLICATION
    routing_error_config = config.AMQP_QUEUE_ERROR

    print(amqp_instance)
    # Get AQMP details
    amqp_creds = get_amqp_by_key(amqp_instance)

    # Set RabbitMQ
    Amqp = AMQP(
        exchange,
        routing_request_config,
        virtual_host,
        amqp_creds['host'],
        amqp_creds['port'],
        amqp_creds['login'],
        amqp_creds['pass'],
        amqp_callback,
        log,
        debug
    )

    try:

        # Star Insatgram class
        Instagram = InstagramParsing(20, log, instance, True)

        # setttings
        Instagram.config_set_ua_local(True, ua)

        # Set proxy to parser
        log.debug('Try to get proxy')
        proxy_errors_counter = 0
        proxy_path = custom_f.get_proxy()
        if proxy_path:
            Instagram.config_set_proxy(True, {'http': proxy_path, 'https': proxy_path})
        else:
            log.error('Proxy not working {}'.format(instance))
            time.sleep(12*3600)

        # Get instagram credentials
        response = get_instagram_credentials_by_key(instance)
        #log.info('Response is %s', response)

        if response['error']:
            log.error('Received error message from credential request => %s', response['message'])
            time.sleep(3600)
            raise Exception(response['message'])

        Instagram.config_set_credential(True, response['credential'])

        # Start Redis
        redis = RedisClass(log, config.REDIS_ACCOUNTS_HOST, config.REDIS_ACCOUNTS_PORT)
        buckets_keys = config.REDIS_ACCOUNTS_BUCKETS_KEYS

        redis_location = RedisClass(log, config.REDIS_LOCATION_HOST, config.REDIS_LOCATION_PORT,
                                    config.REDIS_LOCATION_PASSWORD)

        # Store posts on local
        path_posts = config.INSTAGRAM_POSTS_LOCAL_STORE_PATH

    except Exception as e:
        log.error(e)
        quit()

    try:
        Amqp.run()
    except KeyboardInterrupt:
        Amqp.stop()
        quit()
    except Exception as err:
        log.exception("Global raise",err)
        time.sleep(60)
        quit()
