import config
import json
import time
from datetime import datetime, timezone
import os
import requests
import random
import sys
from operator import itemgetter
import ast

from module.instagram.parsing import InstagramParsing
from module.rabbitmq.amqp import AMQP
from module.redis.main import RedisClass
from module.aws.s3 import S3
from module.minio.minio import MinioClass
from module.custom.functions import CustomFunction
from module.custom.amqp_requests import AMQPRequests


def log_info(message, username, tag=None):
    log.info({
        'message': message,
        'username': username,
        'link': custom_f.get_link(),
        'task': custom_f.get_task(),
        'token': custom_f.get_token(),
        'virtual_host': virtual_host,
        'message_tag': tag
    })


def log_warning(message, username, tag=None):
    log.warning({
        'message': message,
        'username': username,
        'link': custom_f.get_link(),
        'task': custom_f.get_task(),
        'token': custom_f.get_token(),
        'virtual_host': virtual_host,
        'message_tag': tag
    })


def amqp_send_message(ch, method, answer, routing_key, priority=1, error=False, delivery_mode=2):
    routing = routing_key if not error else routing_error_config

    try:
        json_object = json.loads(answer)
    except:
        answer = json.dumps(answer, default=lambda o: o.__dict__, indent=4)
        pass

    try:
        ch.basic_publish(
            exchange=Amqp.get_exchange(),
            routing_key=routing,
            body=answer,
            properties=Amqp.basic_properties(delivery_mode, priority))

        log_info(
            "Send message to {} queue, message len =  {}, text = {}".format(routing, len(answer), json.loads(answer)),
            None)
    except Exception as e:
        log.exception("Error while sending message to result queue " + str(e))


def amqp_callback(ch, method, properties, message):
    message = (message.decode('utf-8'))
    timenow = str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    timestart = datetime.now(timezone.utc)

    '''
    message = '{"project_flow":[{"task":"get-all-comments-by-post","use_proxy":true,"store_json":false,'+ \
              '"store_s3":true,"limit":10000,"update_limit":false,"export_random":100,'+\
              '"force_check":false,"store_redis":false,"export_full_data":false,"date":"2019-04-11T15:10:25+00:00",'+\
              '"return_queue":"comment"},'+\
              '{"task": "get-comment-analysis", "mysql_instance": 3, "random_count": 100, ' + \
              '"export_mysql_keys": {"username": "username", "comment_analysis": "analysis"}, ' + \
              '"force_check": false, "export_full_data": false, "date": "2019-04-11T15:10:25+00:00", ' + \
              '"return_queue": ""}],"token":1000,"completed":["BwYkw9FnDAt"]}'
    '''

    try:
        json_body = json.loads(message)
    except Exception as e:
        log.exception("Get exception at Body message JSON parsing")

        # Acking message from AMQP
        ch.basic_ack(delivery_tag=method.delivery_tag)

        # Error message
        answer = '{' + '"task":"task-parsing-error","datetime":"{}","status":false,"error":"{}","received":"{}"'.format(
            str(timenow), ' Get exception at JSON parsing ' + str(e), message) + '}'

        amqp_send_message(ch, method, answer, '', 1, True)
        return

    # Project flow
    project_flow = list(json_body['project_flow']) if 'project_flow' in json_body else {}
    if not project_flow:
        log.error('{' + '"message":"Project flow not exist","message_tag":""' + '}","message_tag":""' + '}')
        ch.basic_ack(delivery_tag=method.delivery_tag)
        # Default answer
        answer = '{' + '"task":"project-flow-error","status":false,"datetime":"{}","error":"No tasks found"'.format(
            timenow) + '}'
        amqp_send_message(ch, method, answer, '', 1, True)
        return
    else:
        body = project_flow.pop(0)

    # Task
    task = str(body['task']) if 'task' in body else ''
    custom_f.set_task(task)

    # Token
    token = str(json_body['token']) if 'token' in json_body else ''
    custom_f.set_token(token)

    # Link
    link = str(body['link']) if 'link' in body else ''
    custom_f.set_link(link)
    amqp_request_f.set_link(link)

    log_info('Get body {}, task {}'.format(json_body, task), None, None)

    limit = int(body['limit']) if 'limit' in body else 50000

    # Main options
    option_use_proxy = bool(body['use_proxy']) if 'use_proxy' in body else True
    option_priority = int(body['priority']) if 'priority' in body else 0
    option_mysql_instance = int(body['mysql_instance']) if 'mysql_instance' in body else 0

    # Check option
    option_force_check = bool(body['force_check']) if 'force_check' in body else False
    option_get_posts_data = bool(body['get_posts_data']) if 'get_posts_data' in body else False
    option_get_account_data = bool(body['get_accounts_data']) if 'get_accounts_data' in body else True
    option_get_engagements_data = bool(body['get_engagements_data']) if 'get_engagements_data' in body else False

    # Store options
    option_store_local_json = bool(body['store_json']) if 'store_json' in body else False
    option_store_redis = bool(body['store_redis']) if 'store_redis' in body else False
    option_store_s3 = bool(body['store_s3']) if 'store_s3' in body else False

    # AMQP options
    routing_return = str(body['return_queue']) if 'return_queue' in body else ''
    routing_redis_data = str(body['return_redis_data']) if 'return_redis_data' in body else ''

    # Export options
    option_export_random = int(body['export_random']) if 'export_random' in body else 0
    option_export_full = bool(body['export_full']) if 'export_full' in body else False
    option_export_posts = bool(body['export_posts']) if 'export_posts' in body else False
    option_hmsets_export = str(body['export_redis_data']) if 'export_redis_data' in body else ''
    option_export_mysql_keys = dict(body['export_mysql_keys']) if 'export_mysql_keys' in body else dict()
    option_export_mysql_table = str(body['export_mysql_table']) if 'export_mysql_table' in body else 'account'

    # Statistic options
    option_limit_er = int(body['limit_er']) if 'limit_er' in body else 12
    option_limit_ltcr = int(body['limit_ltcr']) if 'limit_ltcr' in body else 12
    option_comments_stat = int(body['comments_stat']) if 'comments_stat' in body else 0
    option_export_mysql_keys = dict(body['export_mysql_keys']) if 'export_mysql_keys' in body else dict()
    option_export_mysql_table = str(body['export_mysql_table']) if 'export_mysql_table' in body else 'account'

    # Location options
    option_location_posts = bool(body['check_location_posts']) if 'check_location_posts' in body else False
    option_location_bio = bool(body['check_location_bio']) if 'check_location_bio' in body else False
    option_location_info = bool(body['check_location_info']) if 'check_location_info' in body else False

    # Set information to parser for storing data to local server
    # todo: remove it or change it -> str(today)
    if option_store_local_json:
        today = datetime.today().strftime('%Y-%m-%d')
        Instagram.config_set_local_storing(True, config.INSTAGRAM_LOCAL_STORE_PATH + '/' + str(today))
    else:
        Instagram.config_set_local_storing(False)

    # Set proxy to parser
    # proxy_errors_counter = 0
    # if option_use_proxy:
    #    proxy_path = custom_f.get_proxy()
    #    Instagram.config_set_proxy(True, {'http': proxy_path, 'https': proxy_path})
    # else:
    #    proxy_path = ''
    #    Instagram.config_set_proxy(False)

    # Set getting data from account (Info, Posts, Count ER)
    Instagram.config_set_export_dict(option_get_account_data, option_get_posts_data, option_get_engagements_data)

    # todo: Move mysq to separate queue
    # Set direct Mysql connection
    # mysql = None
    # if option_mysql_instance:

    # Get mysql data from config by key
    #    item = custom_f.get_mysql_by_key(option_mysql_instance)

    #    try:
    #        mysql = MysqlORM(log, debug)
    #        mysql.config_set_mysql(True, item['host'], item['port'], item['login'], item['pass'], item['db'])
    #    except Exception as e:
    #        log.error(
    #            '{' + '"message":"Mysql connection error {},creds {}","message_tag":"mysql-log"'.format(e, item) + '}')
    #        raise
    # Coefficient to decrease account check for ip with account in "account-info" tasks
    coef = 1
    coef_location = .5

    # -----------------------------------------------------------------------------------------------------------------
    #  Get SHORT instagram account data and store it in json (if 'store-json' = true)
    #  and send data to queue 'result' from amqp-message. If set redis - store in redis
    #

    if task == 'get-account-data':

        # Get input list
        if not isinstance(json_body['completed'], list):
            input = list(filter(None, ast.literal_eval(json_body['completed'])))
        else:
            input = json_body['completed']

        completed = []
        error404 = []
        user_404 = []
        hmsets = {}

        log_info("Account to check number is {}, list {}".format(len(input), input), None)

        # Check if data exist in redis
        usernames = []
        if len(input) > 1:
            usernames = get_usernames_list_by_options(input, option_force_check, option_store_redis)
        elif isinstance(input, list):
            for item in input:
                if isinstance(item, dict):
                    usernames.append(item['u'])
                else:
                    usernames.append(item)

        # print(usernames)
        # quit()

        # If usernames is empty - ending task
        if len(usernames) == 0:
            log_warning("Send message to notice about empty username list", None)

            # Acking message from AMQP
            # ch.basic_ack(delivery_tag=method.delivery_tag)

            # Send message to queue
            answer = '{' + '"task":"empty-usernames-list-error","datetime":"{}","token":"{}",'.format(timenow, token) + \
                     '"message":"There is\'nt usernames to check","message-get":"{}"'.format(message) + '}'

            # Send error message to error queue
            amqp_send_message(ch, method, answer, '', 1, True)

            return

        # If there are usernames in list
        else:

            n = config.INSTAGRAM_CHECK_USERNAMES_IN_REQUEST * coef
            while len(usernames):
                usernames_check = usernames[:n]
                log.debug('{' + '"message":"List of usernames to check {}","message_tag":"input-data"'.format(
                    usernames_check) + '}')

                try:
                    data = Instagram.get_instagram_data_for_accounts_by_username(usernames_check)
                except Exception as e:
                    log.exception("Instagram.get_instagram_data_for_accounts_by_username error")

                    if len(routing_return) > 0:
                        # Send results to result queue
                        for item in completed:
                            amqp_send_message(ch, method,
                                              amqp_request_f.answer_account_data_success(item, project_flow,
                                                                                         option_export_random,
                                                                                         item['account']['username']),
                                              routing_return, option_priority)

                    # Send error message to error queue
                    answer = '{' + '"task":"get-data-from-web-error","datetime":"{}","status":false,"error":"{}",' + \
                             '"username":"{}"'.format(timenow,
                                                      ' Get exception get_instagram_data_for_accounts_by_username ' +
                                                      str(e), str(usernames_check)) + '}'
                    amqp_send_message(ch, method, answer, '', 1, True)

                    raise Exception('Restart by bad parsing')

                # Looping results
                export_key = ['account']
                if option_get_posts_data:
                    export_key.append('posts')
                result = looping_results(data, 'account', 'username', export_key)
                completed += result['complete']
                error404 += result['error404']

                # print(completed)

                # print(result)
                # quit()

                # Store mysql error 404
                # if len(result['error404']) > 0 and option_mysql_instance:
                #    answer = amqp_request_f.answer_store_data_mysql_username_404(option_export_mysql_table, result['error404'], 1)
                #    amqp_send_message(ch, method, answer, config.AMQP_QUEUE_MYSQL)

                # for item in result['error404']:
                #    answer = custom_f.store_data_mysql_username_404(option_export_mysql_table,item,1)
                #    amqp_send_message(ch,method,answer,config.AMQP_QUEUE_MYSQL)
                # Add to mysql
                # mysql.insert_into_table_increase_field(option_export_mysql_table,
                #                                       {'username': item, 'error': 1}, 'error')
                # todo: send data to mytsql queue 404 - turn off

                # Store mysql completed data
                if len(result['complete']) > 0 and option_mysql_instance:
                    log.debug('{' + '"message":"Store new account data to mysql, type = {}",'.format(
                        option_mysql_instance) + '"message_tag":""' + '}')

                    data = [item['account'] for item in result['complete']]

                    # answer = amqp_request_f.answer_store_data_mysql_username_exist(option_export_mysql_table, data,
                    #                                                         insert_keys)
                    answer = amqp_request_f.answer_store_data_mysql_username_exist(option_export_mysql_table, data,
                                                                                   insert_keys, option_mysql_instance)

                    amqp_send_message(ch, method, answer, routing_mysql_config, option_priority + 10)

                #                    for item in result['complete']:
                #                        answer = custom_f.store_data_mysql_username_exist(option_export_mysql_table,item['account'],insert_keys)
                #                        amqp_send_message(ch,method,answer,config.AMQP_QUEUE_MYSQL)

                # Add to mysql
                # mysql.update_table_all_values_by_key(option_export_mysql_table, item['account'], insert_keys)

                # Store REDIS completed data
                if len(result['complete']) > 0:
                    for item in result['complete']:
                        try:
                            string = custom_f.generate_string_for_redis(item['account'])
                        except Exception as err:
                            log.error(item)
                            log.exception(err)
                            raise
                        index = custom_f.count_redis_index(item['account']['user_id'], bucket_size)
                        if index[0] not in hmsets:
                            hmsets[index[0]] = {}
                        hmsets[index[0]][index[1]] = string

                # Store REDIS error404 data
                if len(result['error404']) > 0:
                    log.debug('404 - {}'.format(result['error404']))
                    log.debug('Input - {}'.format(input))
                    for item in result['error404']:
                        if isinstance(input, list) and isinstance(input[0], dict):
                            log.debug('Item is dict and input is list')
                            user_id = [d['id'] for d in input if 'u' in d and d['u'] == item and 'id' in d]
                            if len(user_id) > 0:
                                user_404.append({item: user_id[0]})
                                index = custom_f.count_redis_index(user_id[0], bucket_size)
                                if index[0] not in hmsets:
                                    hmsets[index[0]] = {}
                                hmsets[index[0]][index[1]] = str(-1) + ':'
                # time.sleep(5)

                if len(hmsets) > 0 and option_store_redis:
                    # log.debug(
                    #    '{' + '"message":"Add data to redis {}",'.format(hmsets) + '"message_tag":""' + '}')
                    # redis.insert_data_from_dict(hmsets)

                    # Send data to redis queue
                    log.debug(
                        '{' + '"message":"Send data to redis queue, number of elements is {}",'.format(len(hmsets)) +
                        '"message_tag":""' + '}')

                    answer = amqp_request_f.answer_store_data_redis(hmsets)
                    amqp_send_message(ch, method, answer, routing_redis_config, option_priority + 10)

                # Check for proxy errors
                if proxy_errors_counter > config.PROXY_ERROR_LIMIT:
                    data = {'proxy': proxy_path.split('//')[1], 'error': 'ProxyErrorNumberExceed'}
                    r = requests.post('https://proxy.endemic.ru/api/broke-proxy',
                                      headers=custom_f.get_post_header_proxy(),
                                      json=data)

                    if len(routing_return) > 0:
                        option_export = True if int(option_export_random) + int(option_export_full) > 0 else False
                        for item in completed:
                            amqp_send_message(ch, method,
                                              amqp_request_f.answer_account_data_success(item, project_flow,
                                                                                         option_export,
                                                                                         item['account']['username']),
                                              routing_return, option_priority)

                        if len(user_404) > 0:
                            amqp_send_message(ch, method,
                                              amqp_request_f.answer_user404(user_404, project_flow),
                                              routing_return, option_priority)

                    # mysql.close()
                    raise Exception('Restart by bad proxy')  # Restart app by exceeded errors number

                print(user_404)

                # Sleep between checks
                sleep_time = config.INSTAGRAM_CHECK_USERNAMES_PARSING_DELAY * len(
                    usernames_check) / n
                log.debug('{' + '"message":"Count sleep time. Sleep time = {}"'.format(sleep_time) + '}')
                time.sleep(sleep_time)

                if len(result['recheck']):
                    usernames += result['recheck']
                    log_info('Accounts to recheck {}, new usernames list is {}'.format(result['recheck'],usernames),None,None)

                # Remove first part of usernames from list
                usernames = usernames[n:]
                usernames = list(set(usernames))

                log_info('Usernames left {}'.format(usernames), None, None)

            # Return proxy to list with status "1" if it still working
            # data = {'proxy': proxy_path.split('//')[1]}
            # r = requests.post('https://proxy.endemic.ru/api/return-proxy',
            #                  headers=custom_f.get_post_header_proxy(),
            #                  json=data)

            # Usernames ended - wait for new list
            # log.debug('{'+'"message":"Usernames ended, waiting for new message in queue')

            log_info("Usernames ended, waiting for new message in queue",None,None)
            ch.basic_ack(delivery_tag=method.delivery_tag)

            """
            # Send redis data to result queue
            if len(hmsets) > 0 and routing_redis_data:
                amqp_send_message(ch, method,
                                  amqp_request_f.answer_hmsets_data_success(hmsets, input, project_flow),
                                  routing_redis_data, option_priority)

                if len(user_404) > 0:
                    amqp_send_message(ch, method,
                                      amqp_request_f.answer_user404(user_404, project_flow),
                                      routing_redis_data, option_priority)

            """

            log.debug('Routing return is {}'.format(routing_return))

            if routing_return == 'audit.choice':
                amqp_send_message(ch, method,
                                  amqp_request_f.answer_default(project_flow, '', [], []),
                                  routing_return, option_priority)

            # Send results to result queue
            if len(routing_return) > 0:
                option_export = True if int(option_export_random) + int(option_export_full) > 0 else False

                if len(user_404) > 0:
                    amqp_send_message(ch, method,
                                      amqp_request_f.answer_user404(user_404, project_flow),
                                      routing_return, option_priority)

                for item in completed:
                    print(item)
                    amqp_send_message(ch, method,
                                      amqp_request_f.answer_account_data_success(item, project_flow, option_export,
                                                                                 item['account']['username']),
                                      routing_return, option_priority)

    # -----------------------------------------------------------------------------------------------------------------
    #  Get SHORT instagram account data from i.instagram.com
    #

    elif task == 'get-account-info':

        log.debug('# Start get-account-info case')

        # Get input list
        if not isinstance(json_body['completed'], list):
            input = list(filter(None, ast.literal_eval(json_body['completed'])))
        else:
            input = json_body['completed']

        completed = []
        error404 = []

        # todo: additional check in redis
        ids = input

        log.debug(ids)

        # If ids is empty - ending task
        if len(ids) == 0:
            if debug:  log.error('Send message to notice about empty ids list')

            # Acking message from AMQP
            ch.basic_ack(delivery_tag=method.delivery_tag)

            # Send message to queue
            answer = '{' + '"task":"empty-ids-list-error","datetime":"{}","token":"{}",'.format(timenow, token) + \
                     '"message":"There is\'nt ids to check in task {}","message-get":"{}"'.format(task,message) + '}'

            # Send error message to error queue
            amqp_send_message(ch, method, answer, '',1, True)

        # If there are ids in list
        else:

            option_store_s3 = True

            if option_store_s3:
                s3 = S3('', config.AWS_S3_ACCESS_KEY_ID, config.AWS_S3_ACCESS_KEY_SECRET)
                s3.connect_client()
            else:
                s3 = None

            n = config.INSTAGRAM_CHECK_INFO_IN_REQUEST

            while len(ids):
                ids_check = ids[:n]
                log.debug('List of ids to check' + str(ids_check))

                data = []

                ids_remove = []
                if option_store_s3:
                    # Check if exists accounts info with same data
                    for id in ids_check:
                        filename = str(str(id) + '-full.json')
                        log.debug('Filename is %s',filename)
                        """
                        if s3.check_file_exist('account-info-bucket', filename) and not option_force_check:
                            log.debug('Info file exists')
                            s3_object = s3.get_object_content('account-info-bucket', filename)
                            json_string = s3_object
                            try:
                                json_dict = json.loads(json_string)
                                data.append(Instagram.fetch_export_account_info_from_json(json_string,
                                                                                          json_dict['user'][
                                                                                              'username']))
                                ids_remove.append(id)
                            except Exception as exn:
                                log.exception(json_string)
                        """
                log.debug('List of ids to check from queue' + str(ids_check))
                log.debug('List of ids to remove' + str(ids_remove))
                ids_check = [x for x in ids_check if x not in ids_remove]
                log.debug('List of ids to check' + str(ids_check))
                #log.debug('Data = %s',data)

                # Check if only we have accounts info in ids_check
                if len(ids_check) > 0:
                    try:
                        data += Instagram.get_instagram_info_for_accounts_by_ids(ids_check)
                    except Exception as e:
                        log.exception('Instagram.get_instagram_info_for_accounts_by_username error' + str(e))

                        if len(routing_return) > 0:
                            # Send results to result queue
                            for item in completed:
                                amqp_send_message(ch, method, amqp_request_f.answer_default(project_flow, token,
                                                                                            [item['edge']['username']],
                                                                                            []),
                                                  routing_return, option_priority)

                        # Send error message to error queue
                        answer = '{' + '"task":"get-data-from-web-error","datetime":"{}","status":false,"error":"{}",' + \
                                 '"username":"{}"'.format(timenow,
                                                          ' Get exception get_instagram_data_for_accounts_by_username ' +
                                                          str(e), str(ids_check)) + '}'
                        amqp_send_message(ch, method, answer, '', 1, True)
                        raise Exception('Restart by bad parsing')  # Raise to return current message back to AMQP

                # Looping results
                export_key = ['edge']
                result = looping_results(data, 'edge', 'username', export_key)
                completed += result['complete']
                error404 += result['error404']

                # Store mysql error 404
                #if len(result['error404']) > 0 and option_mysql_instance:
                    #for item in result['error404']:
                        # Add to mysql
                        #mysql.insert_into_table_increase_field(option_export_mysql_table, {'username': item, 'error': 1}, 'error')

                hmsets = {}
                option_store_redis = True
                # Store REDIS completed data
                if len(result['complete']) > 0:
                    for item in result['complete']:
                        try:
                            string = custom_f.generate_string_for_redis(item['edge']['data'])
                        except Exception as err:
                            log.error(item)
                            log.exception(err)
                            raise
                        index = custom_f.count_redis_index(item['edge']['data']['user_id'], bucket_size)
                        if index[0] not in hmsets:
                            hmsets[index[0]] = {}
                        hmsets[index[0]][index[1]] = string

                if len(hmsets) > 0 and option_store_redis:
                    # log.debug(
                    #    '{' + '"message":"Add data to redis {}",'.format(hmsets) + '"message_tag":""' + '}')
                    # redis.insert_data_from_dict(hmsets)

                    # Send data to redis queue
                    log.debug(
                        '{' + '"message":"Send data to redis queue, number of elements is {}",'.format(len(hmsets)) +
                        '"message_tag":""' + '}')

                    answer = amqp_request_f.answer_store_data_redis(hmsets)
                    amqp_send_message(ch, method, answer, routing_redis_config, option_priority + 10)

                # Check for proxy errors
                if proxy_errors_counter > config.PROXY_ERROR_LIMIT:
                    #send = {'proxy': proxy_path.split('//')[1], 'error': 'ProxyErrorNumberExceed'}
                    #r = requests.post('https://proxy.endemic.ru/api/broke-proxy',
                    #                  headers=get_post_header_proxy(),
                    #                  json=send)

                    if len(routing_return) > 0:
                        option_export = True if int(option_export_random) + int(option_export_full) > 0 else False
                        for item in completed:
                            amqp_send_message(ch, method, amqp_request_f.answer_default(project_flow,token,[item['edge']['username']],[]),
                                              routing_return,option_priority)

                    raise Exception('Restart by bad proxy')  # Restart app by exceeded errors number

                option_export_mysql_keys_info = {
                    "username" : "username",
                    "status" : "status",
                    "media" : "posts",
                    "public_email" : "public_email",
                    "category" : "category"
                }

                # Store mysql completed data
                if len(result['complete']) > 0 and option_mysql_instance and option_export_mysql_keys:
                    #print(option_export_mysql_keys)
                    log.debug('Store new account data to mysql, type = %s', option_mysql_instance)
                    #for item in result['complete']:
                        #print(item['edge']['data'])
                        # Add to mysql
                        #mysql.update_table_all_values_by_key(option_export_mysql_table, item['edge']['data'], option_export_mysql_keys)
                        #mysql.update_table_all_values_by_key('account', item['edge']['data'], option_export_mysql_keys_info)

                # Export to s3 and local store
                log.debug('Storing in s3 is ' + str(option_store_s3))
                if len(result['complete']) > 0 and option_store_s3:
                    #log.debug(result)
                    #log.debug(ids_check)
                    for item in result['complete']:
                        log.debug(int(item['edge']['data']['user_id']))
                        if str(item['edge']['data']['user_id']) in ids_check:

                            #print(item['edge']['json'])
                            filename = str(item['edge']['data']['user_id'])+'-full.json'
                            log.debug('Storing at S3 file to ' + filename)
                            path = custom_f.store_message_to_json_file(path_posts, filename, item['edge']['json'])
                            status = s3.upload_file(path, 'account-info-bucket', filename)
                            if status and not option_store_local_json:
                                os.remove(path)
                            if debug:
                                log.debus('Storing at S3 file complete')
                        else:
                            log.debug('We do not storing duplicates!!!')

                # Sleep between checks
                sleep_time = config.INSTAGRAM_CHECK_INFO_PARSING_DELAY * len(
                    ids_check) / n/3
                log.debug('Count sleep time. Sleep time = ' + str(sleep_time))
                time.sleep(sleep_time)

                # Remove first part of ids from list
                ids += result['recheck']
                ids = ids[n:]
                ids = list(set(ids))


            # Usernames ended - wait for new list
            log.debug('Usernames ended, waiting for new message in queue')
            ch.basic_ack(delivery_tag=method.delivery_tag)

            # Send results to result queue
            if routing_return:
                for item in completed:
                    item['edge']['json'] = {}
                    amqp_send_message(ch, method,
                                      amqp_request_f.answer_default(project_flow,token,[item['edge']['username']],item['edge']),
                                      routing_return,option_priority)

    # -----------------------------------------------------------------------------------------------------------------
    #  Get followers/following accounts data.
    #
    elif task == 'get-private-list-accounts-info':

        # Get input list
        if not isinstance(json_body['completed'], list):
            input = list(filter(None, ast.literal_eval(json_body['completed'])))
        else:
            input = json_body['completed']

        completed = []
        error404 = []

        # Check if data exist in redis
        usernames = get_usernames_list_by_options(input, option_force_check, option_store_redis)

        # If usernames is empty - ending task
        if len(usernames) == 0:
            if debug:  log_warning("Send message to notice about empty username list", None)

            # Send message to queue
            ch.basic_ack(delivery_tag=method.delivery_tag)
            answer = '{' + '"task":"empty-usernames-list-error","datetime":"{}","token":"{}",'.format(timenow, token) + \
                     '"message":"There is\'nt usernames to check","message-get":"{}"'.format(message) + '}'

            amqp_send_message(ch, method, answer, '', 1, True)
        else:

            n = config.INSTAGRAM_CHECK_USERNAMES_IN_REQUEST * coef

            while len(usernames):

                hmsets = {}

                log.debug(
                    '{' + '"message":"#----------------------------------  Iteration --------------------------------","message_tag":""' + '}')

                usernames_check = usernames[:n]

                log.debug('{' + '"message":"Usernames summary to check = ' + str(len(usernames)))
                log.debug('{' + '"message":"Number of usernames at iteration = ' + str(len(usernames_check)))

                try:
                    data = Instagram.get_instagram_data_for_accounts_by_username(usernames_check)
                except Exception as e:
                    log.exception('Instagram.get_instagram_data_for_accounts_by_username error {}')

                    # Send current exists data
                    if len(routing_return) > 0:
                        option_export = True if int(option_export_random) + int(option_export_full) > 0 else False
                        amqp_send_message(ch, method,
                                          amqp_request_f.answer_private_list_account_success(completed, error404,
                                                                                             project_flow,
                                                                                             option_export, token),
                                          routing_return, option_priority)

                    # Send error message to error queue
                    answer = '{' + '"task":"get-data-from-web-error","datetime":"{}","status":false,"error":"{}",' + \
                             '"username":"{}"'.format(timenow,
                                                      ' Get exception get_instagram_data_for_accounts_by_username ' +
                                                      str(e), str(usernames_check)) + '}'
                    amqp_send_message(ch, method, answer, '', 1, True)

                    raise Exception('Restart by bad parsing')  # Raise to return current message back to AMQP

                # Looping results

                result = looping_results(data, 'account', 'username', ['account'], 'get-private-list-accounts-info')
                completed += result['complete']
                error404 += result['error404']

                # Store REDIS completed data
                if len(result['complete']) > 0 and option_store_redis:
                    for item in result['complete']:
                        string = custom_f.generate_string_for_redis(item['account'])
                        index = custom_f.count_redis_index(item['account']['user_id'], bucket_size)
                        if index[0] not in hmsets:
                            hmsets[index[0]] = {}
                        hmsets[index[0]][index[1]] = string

                # Store REDIS error404 data
                if len(result['error404']) > 0 and option_store_redis:
                    for item in result['error404']:
                        if isinstance(input, dict):
                            # todo: find user_id in usernames list
                            user_id = [d['id'] for d in input if d['u'] == item]
                            index = custom_f.count_redis_index(user_id[0], bucket_size)
                            if index[0] not in hmsets:
                                hmsets[index[0]] = {}
                            hmsets[index[0]][index[1]] = str(-1) + ':'

                if len(hmsets) > 0:
                    log.debug('{' + '"message":"Add data to redis ' + str(hmsets))
                    redis.insert_data_from_dict(hmsets)

                # Stop working in we reached proxy_error limit
                if proxy_errors_counter > config.PROXY_ERROR_LIMIT:
                    log_warning("Send request to update proxy status to 0", None)
                    data = {'proxy': proxy_path.split('//')[1], 'error': 'ProxyErrorNumberExceed'}
                    r = requests.post('https://proxy.endemic.ru/api/broke-proxy',
                                      headers=custom_f.get_post_header_proxy(),
                                      json=data)
                    # If result routing key not null - send current data
                    if len(routing_return) > 0:
                        amqp_send_message(ch, method,
                                          amqp_request_f.answer_private_list_account_success(completed, error404,
                                                                                             project_flow,
                                                                                             option_export_random,
                                                                                             token),
                                          routing_return, option_priority)
                    raise Exception('Restart by bad proxy')

                # Sleep between checks
                sleep_time = config.INSTAGRAM_CHECK_USERNAMES_PARSING_DELAY * len(
                    usernames_check) / n
                log.debug('{' + '"message":"Count sleep time. Sleep time = ' + str(sleep_time))
                time.sleep(sleep_time)

                # Remove first part of usernames from list
                usernames += result['recheck']
                usernames = usernames[n:]
                usernames = list(set(usernames))

            log.debug(
                '{' + '"message":"Usernames ended, waiting for new message in queue","message_tag":""' + '}')

            ch.basic_ack(delivery_tag=method.delivery_tag)

            # Send getting info to result queue
            option_export = True if int(option_export_random) + int(option_export_full) > 0 else False
            if len(routing_return) > 0:
                log.debug('Send to queue {} '.format(routing_return))
                amqp_send_message(ch, method,
                                  amqp_request_f.answer_private_list_account_success(completed, error404, project_flow,
                                                                                     option_export,
                                                                                     token),
                                  routing_return, option_priority)

            # Return proxy to list with status "1" if it still working
            # data = {'proxy': proxy_path.split('//')[1]}
            # r = requests.post('https://proxy.endemic.ru/api/return-proxy',
            #                  headers=custom_f.get_post_header_proxy(),
            #                  json=data)

    # -----------------------------------------------------------------------------------------------------------------
    #  Get location info.
    #

    elif task == 'get-location-info-deprecated':

        # Get input list
        locations = []
        if not isinstance(json_body['completed'], list):
            locations = list(filter(None, ast.literal_eval(json_body['completed'])))
        else:
            locations = json_body['completed']

        completed = []
        error404 = []

        # ch.basic_ack(delivery_tag=method.delivery_tag)

        # routing_return = 'location'

        # amqp_send_message(ch, method,
        #                  amqp_request_f.answer_location_list_success([], [], project_flow, False, token),
        #                  routing_return, option_priority)
        # return

        # If usernames is empty - ending task
        if len(locations) == 0:
            log_warning("Send message to notice about empty locations list", None)

            # Send message to queue
            ch.basic_ack(delivery_tag=method.delivery_tag)
            answer = '{' + '"task":"empty-locations-list-error","datetime":"{}","token":"{}",'.format(timenow, token) + \
                     '"error":"There is\'nt locations to check","message":"{}"'.format(message) + '}'
            amqp_send_message(ch, method, answer, '', 1, True)

        else:
            n = int(config.INSTAGRAM_CHECK_USERNAMES_IN_REQUEST * coef_location)

            log_info('Start getting info {} ids, n = {}'.format(locations, n), None)

            while len(locations):

                locations_check = locations[:n]

                hmsets = {}

                log.debug('Start get location from location list, number location in iteration is {}'.format(
                    len(locations_check)))

                try:
                    data = Instagram.get_location_data_for_token(locations_check)
                except Exception as e:
                    log.exception('Instagram.get_location_data_for_token error')

                    # Send current exists data
                    if len(routing_return) > 0:
                        option_export = True if int(option_export_random) + int(option_export_full) > 0 else False

                        # Send exist data in completed if we get error during get location
                        amqp_send_message(ch, method,
                                          amqp_request_f.answer_location_list_success(completed, error404, project_flow,
                                                                                      option_export, token),
                                          routing_return, option_priority)

                    # Send error message to error queue
                    answer = '{' + '"task":"get-data-from-web-error","datetime":"{}","status":false,"error":"{}",' + \
                             '"username":"{}"'.format(timenow,
                                                      ' Get exception get_instagram_data_for_accounts_by_username ' +
                                                      str(e), str(locations_check)) + '}'
                    amqp_send_message(ch, method, answer, '', 1, True)
                    raise Exception('Restart by bad parsing')  # Raise to return current message back to AMQP

                # Looping results
                result = looping_results(data, 'location', 'token', ['location'])
                completed += result['complete']
                error404 += result['error404']

                # Store REDIS completed data
                if len(result['complete']) > 0 and option_store_redis:
                    for item in result['complete']:
                        string_location = custom_f.generate_string_for_redis_location(item['location']['data'])
                        hmsets[item['location']['data']['id']] = string_location
                        if len(locations_check) == 1:
                            if locations_check[0] != item['location']['data']['id']:
                                hmsets[locations_check[0]] = string_location

                # Store REDIS completed data
                if len(result['error404']) > 0 and option_store_redis:
                    log.debug(result['error404'])
                    for item in result['error404']:
                        hmsets[item] = 'ZZ:0:0'

                log.debug(hmsets)

                if len(hmsets) > 0:
                    log.debug("Add data to redis location {}".format(json.dumps(hmsets)))

                    redis_location.insert_dict_key_value(hmsets)

                # Stop working in we reached proxy_error limit
                if proxy_errors_counter > config.PROXY_ERROR_LIMIT:
                    log_warning("Send request to update proxy status to 0", None)
                    data = {'proxy': proxy_path.split('//')[1], 'error': 'ProxyErrorNumberExceed'}
                    r = requests.post('https://proxy.endemic.ru/api/broke-proxy',
                                      headers=custom_f.get_post_header_proxy(),
                                      json=data)
                    # If result routing key not null - send current data
                    if len(routing_return) > 0:
                        amqp_send_message(ch, method,
                                          amqp_request_f.answer_location_list_success(completed, error404, project_flow,
                                                                                      option_export_random, token),
                                          routing_return, option_priority)
                    raise Exception('Restart by bad proxy')

                # Sleep between checks
                sleep_time = config.INSTAGRAM_CHECK_USERNAMES_PARSING_DELAY * len(
                    locations_check) / n
                log.debug('{' + '"message":"Count sleep time. Sleep time = ' + str(sleep_time))
                time.sleep(sleep_time)

                # Remove first part of locations from list
                locations += result['recheck']
                locations = locations[n:]
                locations = list(set(locations))

            log_info("Locations {} ended, waiting for new message in queue".format(json_body['completed']), None)

            # Send getting info to result queue
            ch.basic_ack(delivery_tag=method.delivery_tag)
            option_export = True if int(option_export_random) + int(option_export_full) > 0 else False
            if len(routing_return) > 0:
                amqp_send_message(ch, method,
                                  amqp_request_f.answer_location_list_success(completed, error404, project_flow,
                                                                              option_export,
                                                                              token),
                                  routing_return, option_priority)

            # Return proxy to list with status "1" if it still working
            # data = {'proxy': proxy_path.split('//')[1]}
            # r = requests.post('https://proxy.endemic.ru/api/return-proxy',
            #                  headers=custom_f.get_post_header_proxy(),
            #                  json=data)

            # -----------------------------------------------------------------------------------------------------------------
            #  Get all posts.
            #
            # Message example
            #{"project_flow": [
            #    {"task": "get-all-posts-for-username", "limit": 500, "store_s3": false, "comments_stat": 24}],
            # "token": "", "completed": ["get_travel_light"]}

    # -----------------------------------------------------------------------------------------------------------------
    #  Get posts for username
    #

    elif task == 'get-all-posts-for-username':

        # Get input list
        if not isinstance(json_body['completed'], list):
            try:
                input = list(filter(None, ast.literal_eval(json_body['completed'])))
            except:
                input = [json_body['completed']]
        else:
            input = json_body['completed']

        # Check if data exist in redis
        # usernames = get_usernames_list_by_options(input, option_force_check, option_store_redis)

        usernames = input

        if len(usernames) > 5 and routing_return:

            ch.basic_ack(delivery_tag=method.delivery_tag)

            project_flow = [body] + project_flow

            for username in usernames:
                try:
                    amqp_send_message(ch, method,
                                      amqp_request_f.answer_string_get_all_posts([], project_flow, [username],
                                                                                 username),
                                      config.AMQP_QUEUE_PUBLIC, option_priority - 1)
                except Exception as err:
                    log.exception(err)

            return

        # If usernames is empty - ending task
        if len(usernames) == 0:
            log_warning("Send message to notice about empty username list", None)

            # Acking message from AMQP
            ch.basic_ack(delivery_tag=method.delivery_tag)

            # Send message to queue
            answer = '{' + '"task":"{}","error":"empty-usernames-list-error","datetime":"{}","token":"{}",'.format(task,
                                                                                                                   timenow,
                                                                                                                   token) + \
                     '"message":"There is\'nt usernames to check","message-get":"{}"'.format(message) + '}'

            # Send error message to error queue
            amqp_send_message(ch, method, answer, '', 1, True)
        else:

            if option_store_s3:
                s3 = S3(log, config.AWS_S3_ACCESS_KEY_ID, config.AWS_S3_ACCESS_KEY_SECRET)
                s3.connect_client()
            else:
                s3 = None

            for username in usernames:

                log_info("Start get-all-posts-for-username {}".format(username), username)

                next = True

                log.debug('{' + '"message":"List of usernames to check {}, store in s3 {}",'.format(username,
                                                                                                    option_store_s3) + '"message_tag":"input-data"' + '}')

                # Check if posts file have already saved in S3
                filename = str(username) + '.json'

                posts = []

                log.debug('Option store s3 is %s', option_store_s3)

                # Get account data from instagram to compare last posts sended
                Instagram.config_set_export_dict(True, True, False)

                account = Instagram.get_instagram_data_for_accounts_by_username([username])

                log.debug(account)

                try:

                    status = True
                    if account[0]['account']['error'] == 404 or not len(account[0]['posts']):
                        status=False

                        data = {
                            'username': username,
                            'posts': 0,
                            'error':5
                        }

                        export_keys = {'username': 'username', 'posts': 'posts','error':'error'}

                    if 'private' in account[0]['account']:
                        if account[0]['account']['private']:
                            status = False

                            data = {
                                'username':username,
                                'private': account[0]['account']['private'],
                                'posts': len(account[0]['posts'])
                            }

                            export_keys = {'username': 'username', 'private': 'private','posts':'posts'}

                    if len(account[0]['posts']) < 1:
                        status = False

                        data = {
                            'username': username,
                            'posts': len(account[0]['posts'])
                        }

                        export_keys = {'username': 'username', 'posts': 'posts'}


                except Exception as err:
                    log.exception(err)
                    log.error(account)
                    raise

                if not status and routing_return:

                    log_info('Send to next task because this account hasnt posts to scrape',username)
                    ch.basic_ack(delivery_tag=method.delivery_tag)

                    # send to mysql new value for private and posts
                    answer = amqp_request_f.answer_store_data_mysql_comments_stat('account', data, export_keys,
                                                                                  option_mysql_instance)

                    amqp_send_message(ch, method, answer, routing_mysql_config, option_priority)



                    amqp_send_message(ch, method,
                                      amqp_request_f.answer_default(project_flow,token,[username],[]),
                                      routing_return,
                                      option_priority)

                    time.sleep(3)

                    return

                log_info('rountin {}'.format(routing_return),username)

                last_post_date_from_s3 = None

                if option_store_s3 and s3.check_file_exist(s3_posting_bucket, filename):

                    try:
                        # result = get_posts_from_minio_storage(username, account[0]['posts'])
                        result = get_posts_from_s3(username, account[0]['posts'])
                    except Exception as err:
                        log.exception(err)
                        result = []
                        continue

                    log.debug('Posts from s3 is {}'.format(len(result['posts'])))

                    # if 'dates' in result:
                    #    if 'last_post_date_from_s3' in result['dates']:
                    #        last_post_date_from_s3 = result['dates']['last_post_date_from_s3']
                    #        if result['dates']['last_post_date_from_s3'] == result['dates']['last_post_date_instagram']:
                    #            next = False

                    if 'posts' in result:
                        posts = result['posts']

                    log.debug('Next = {}, Posts = {}'.format(next, len(posts)))

                if option_force_check:
                    next = True
                    last_post_date_from_s3 = None

                log_info('rountin {}'.format(routing_return),username)

                # Continue getting posts from Instagram
                if next:
                    try:
                        data = Instagram.get_all_posts_by_username(username, limit, last_post_date_from_s3, '')
                    except Exception as err:
                        log.exception("Instagram.get_instagram_data_for_accounts_by_username error {}")

                        # Send error message to error queue
                        answer = '{' + '"task":"get-data-from-web-error","datetime":"{}","status":false,"error":"{}",' + \
                                 '"username":"{}"'.format(timenow,
                                                          ' Get exception get_instagram_data_for_accounts_by_username ' +
                                                          str(err), str(username)) + '}'
                        amqp_send_message(ch, method, answer, '', 1, True)

                        raise Exception('Restart by bad parsing')  # Raise to return current message back to AMQP

                    # Looping results
                    export_key = []
                    if option_get_posts_data:
                        export_key.append('posts')

                    try:
                        result = looping_results(data['posts'], 'code', '', ['posts'])
                    except Exception as err:
                        log.exception(err)
                        result = {'complete': []}
                        pass

                    log.debug('{' + '"message":"List of added posts id {}","message":""'.format(
                        [d['code'] for d in result['complete']]) + '}')

                    if len(result['complete']) > 0:
                        posts = result['complete'] + posts

                log_info("Task complete for user {}, posts exported {}".format(username, len(posts)), username, None)

                # Store to s3 anyway
                if option_store_s3 and next:

                    log.debug('Summary posts is  {} , store to s3 is {}'.format(len(posts), option_store_s3))

                    # Export to s3 and local store
                    if option_store_s3:
                        log.debug('{' + '"message":"Storing at s3 file to {}",'.format(
                            filename) + '"message_tag":""' + '}')

                        # todo: add unique posts only
                        # posts = [i for n, i in enumerate(posts) if i not in posts[n + 1:]]

                        path = custom_f.store_results_to_file(path_posts, filename, json.dumps(posts))

                        status = s3.upload_file(path, s3_posting_bucket, filename)
                        log.debug('{' + '"message":"Storing at S3 file to {} is {}",'.format(
                            filename, status) + '"message_tag":""' + '}')

                        status = s3.upload_file(path, s3_posting_bucket, filename)
                        if status and not option_store_local_json:
                            os.remove(path)
                        log.debug('{' + '"message":"Storing at s3 file complete","message_tag":""' + '}')
                    else:
                        log.debug(
                            '{' + '"message":"Storing in s3 is  {}, stop storring ",'.format(
                                option_store_s3) + '"message_tag":""' + '}')

                if option_comments_stat and len(posts) > 3:
                    comments_by_posts = [d['comments'] for d in posts[:option_comments_stat] if 'comments' in d]

                    log.debug(' {}, {}, -> Pure comments {}'.format(len(comments_by_posts), option_comments_stat,
                                                                    comments_by_posts))

                    comments_stat = {
                        'avr': "{0:.2f}".format(sum(comments_by_posts) / len(comments_by_posts)),
                        'zero': "{0:.3f}".format(comments_by_posts.count(0) / len(comments_by_posts)),
                        'under5': "{0:.3f}".format(
                            sum(1 for i in comments_by_posts if 0 < i < 5) / len(comments_by_posts)),
                        'stdev': "{0:.4f}".format(custom_f.statistic_standard_deviation(comments_by_posts))}

                    data = {'username': username, 'comments_stat': comments_stat}

                    log.debug('Comments stat is {}'.format(comments_stat))

                    # send data to "mysql" queue to add mysql
                    answer = amqp_request_f.answer_store_data_mysql_comments_stat('account', data,
                                                                                  {'username': 'username',
                                                                                   'comments_stat': 'comments_stat'},
                                                                                  option_mysql_instance)

                    amqp_send_message(ch, method, answer, routing_mysql_config, option_priority)

                    log_info("Get comments stat {}".format(comments_stat), username)

                log_info('rountin {}'.format(routing_return),username)

                if routing_return:
                    log_info('Routing return is {}, export posts is {}, number of posts is {}'.format(routing_return,
                                                                                                      option_export_posts,
                                                                                                      len(posts)),None)
                    try:
                        export = posts if option_export_full else []
                        amqp_send_message(ch, method,
                                          amqp_request_f.answer_string_all_posts_scraping_complete(export, 'posts',
                                                                                                   project_flow,
                                                                                                   username),
                                          routing_return,
                                          option_priority)
                    except Exception as err:
                        log.exception('Error during send message to result queue')
                        raise

        log.debug('Time elapsed at end {}'.format((datetime.now(timezone.utc) - timestart).seconds))

        ch.basic_ack(delivery_tag=method.delivery_tag)

        time.sleep(5)

    # -----------------------------------------------------------------------------------------------------------------
    #  Get all comments for posts list.
    #
    elif task == 'get-posts-with-comments':

        # Get input list
        if not isinstance(json_body['data'], list):
            input = list(filter(None, ast.literal_eval(json_body['data'])))
        else:
            input = json_body['data']

        posts_with_comments = [d['code'] for d in input if d['comments']]

        print(len(posts_with_comments))

        ch.basic_ack(delivery_tag=method.delivery_tag)

        answer = amqp_request_f.answer_default(project_flow, token, posts_with_comments, [])

        amqp_send_message(ch, method, answer, routing_return, option_priority)

        return

    # -----------------------------------------------------------------------------------------------------------------
    #  Get all comments for posts list.
    #

    elif task == 'get-posts-data':

        # Get input list
        if not isinstance(json_body['completed'], list):
            input = list(filter(None, ast.literal_eval(json_body['completed'])))
        else:
            input = json_body['completed']

        usernames = input

        option_search_word_in_accessibility_caption = body['search_word'] if 'search_word' in body else None

        # usernames = ['mrs.fit.ina']

        # If usernames is empty - ending task
        if len(usernames) == 0:
            log_warning("Send message to notice about empty username list", None)

            # Acking message from AMQP
            ch.basic_ack(delivery_tag=method.delivery_tag)

            return

        else:

            if option_store_s3:
                s3 = S3(log, config.AWS_S3_ACCESS_KEY_ID, config.AWS_S3_ACCESS_KEY_SECRET)
                s3.connect_client()
            else:
                s3 = None

            for username in usernames:

                log_info("Start get-posts-data {}".format(username), username)

                # Check if posts file have already saved in S3
                filename = str(username) + '.json'

                log.debug('Option store s3 is %s', option_store_s3)

                if option_store_s3 and s3.check_file_exist(s3_posting_bucket, filename):

                    object_data = s3.get_object_content(s3_posting_bucket, filename)

                    posts = json.loads(object_data)

                    if posts[0] == "n/a":
                        posts = []

                else:

                    posts = []

                try:
                    posts_sorted_by_created = sorted(posts, key=lambda i: i['created'], reverse=True)
                except Exception as err:
                    log.exception(err)
                    log.error(posts)
                    quit()

                codes_all = [d['code'] for d in posts_sorted_by_created]

                seen = set()
                seen_add = seen.add
                codes_all = [x for x in codes_all if not (x in seen or seen_add(x))]

                codes = codes_all[:60]

                export = []

                n = config.INSTAGRAM_CHECK_USERNAMES_IN_REQUEST

                limit = 10

                while len(codes):
                    codes_check = codes[:n]
                    log_info("List of posts to check {}".format(codes_check), username)

                    try:
                        data = Instagram.get_instagram_data_for_posts(codes_check)
                    except Exception as e:
                        log.exception("Instagram.get_instagram_data_for_posts error")
                        raise Exception('Restart by bad parsing')

                    print(len(data))

                    try:
                        result = looping_results(data, 'post', 'code', ['post'])
                    except Exception as err:
                        log.exception(err)
                        result = {'complete': []}
                        pass

                    if option_search_word_in_accessibility_caption:

                        for post in result['complete']:
                            # print(post['post']['data'])
                            for media in post['post']['data']['media']:
                                if media['accessibility_caption']:
                                    #print(media['accessibility_caption'])
                                    if any(search in media['accessibility_caption'] for search in
                                           option_search_word_in_accessibility_caption):
                                        #print('Contain')
                                        export.append({'image': media['image'], 'text': media['accessibility_caption'],
                                                       'created': post['post']['data']['created']})
                                        if len(export) > limit:
                                            codes = []

                    codes = codes[n:]

                    # Sleep between checks
                    sleep_time = config.INSTAGRAM_CHECK_USERNAMES_PARSING_DELAY * len(codes_check) / n
                    log.debug("Count sleep time. Sleep time = {}".format(sleep_time))
                    time.sleep(sleep_time)

                if routing_return:
                    amqp_send_message(ch, method,
                                      amqp_request_f.answer_default(project_flow, token, [username], export),
                                      routing_return, option_priority)

        ch.basic_ack(delivery_tag=method.delivery_tag)

        return







    elif task == 'get-all-comments-by-post':

        username = token

        limit = 500 if limit > 500 else limit

        # Get input list
        if not isinstance(json_body['completed'], list):
            input = list(filter(None, ast.literal_eval(json_body['completed'])))
        else:
            input = json_body['completed']

        log.debug('Input is {}'.format(len(input)))

        input = list(set(input))

        # Flag to get summary comments from all posts
        update_limit = body['update_limit'] if 'update_limit' in body else False

        posts = input

        log_info("List of posts is {}".format(",".join(posts)), username)

        # If usernames is empty - ending task
        if len(posts) == 0:
            log.debug("Send message to notice about empty posts list")

            # Acking message from AMQP
            ch.basic_ack(delivery_tag=method.delivery_tag)

            # Send message to queue
            answer = '{' + '"task":"{}","error":"empty-usernames-list-error","datetime":"{}","token":"{}",'.format(task,
                                                                                                                   timenow,
                                                                                                                   token) + \
                     '"message":"There is\'nt usernames to check","message-get":"{}"'.format(message) + '}'

            # Send error message to error queue
            amqp_send_message(ch, method, answer, '', 1, True)
        else:

            if option_store_s3:
                s3 = S3('', config.AWS_S3_ACCESS_KEY_ID, config.AWS_S3_ACCESS_KEY_SECRET)
                s3.connect_client()
            else:
                s3 = None

            filename = str(username) + '.json'

            all_comments = {}

            if s3.check_file_exist(s3_comments_account_bucket, filename):

                log.debug('{' + '"message":"File {} exists in S3","message_tag":""'.format(filename) + '}')

                # Get object data to check time modified
                object_data = s3.get_key_option(s3_comments_account_bucket, filename)

                # If object modified too much time ago - repeat
                if (timestart - object_data['LastModified']).seconds > 72 * 3600 and option_force_check:
                    log.debug('{' + '"message":"Continue work - get full followers list","message_tag":""' + '}')
                else:
                    comments_json = s3.get_object_content(s3_comments_account_bucket, filename)
                    all_comments = json.loads(comments_json)
                    posts = []

            for post in posts:

                # Check if comments from post is exist in local storage
                if custom_f.check_file_exist(path_comments + '/' + str(post) + '.json'):
                    log.debug('File {} exists, get content from this file'.format(str(post) + '.json'))
                    try:
                        content = custom_f.get_data_from_json_file(path_comments, str(post) + '.json')
                        all_comments[post] = content
                    except:
                        os.remove(path_comments + '/' + str(post) + '.json')
                        pass
                else:
                    log.debug('File {} NOT exists, get content from this file'.format(str(post) + '.json'))
                    all_comments[post] = []

                if not len(all_comments[post]):

                    # Set data = {}
                    data = {}

                    try:
                        if limit > 0:
                            log.debug('{' + '"message":"Start get comments from URL","message_tag":""' + '}')
                            data = Instagram.get_all_comments_by_post(post, limit, '')
                            time.sleep(2)
                    except Exception as e:
                        log.error(
                            '{' + '"message":"Instagram.get_all_comments_by_post error {}","message_tag":""'.format(
                                e) + '}')

                        # Send error message to error queue
                        answer = '{' + '"task":"get-data-from-web-error","datetime":"{}","status":false,"error":"{}",' + \
                                 '"username":"{}"'.format(timenow,
                                                          ' Get exception get_all_comments_by_post ' +
                                                          str(e), str(post)) + '}'
                        amqp_send_message(ch, method, answer, '', 1, True)
                        raise Exception('Restart by bad parsing')  # Raise to return current message back to AMQP

                    if len(data):
                        # Looping results
                        try:
                            log.debug('Start to loop results from {}'.format(data))
                            result = looping_results(data['comments'], 'id', 'code', ['comments'],
                                                     'get-private-list-accounts-info')
                            all_comments[post] = result['complete']

                            # Store to local storage
                            custom_f.store_message_to_json_file(path_comments, str(post) + '.json', all_comments[post])

                            # log.debug('all_comments is  {}'.format(all_comments))
                        except Exception as e:
                            log.exception(e)
                            result = {'complete': []}
                            pass

                if update_limit and len(all_comments[post]) > 0:
                    limit -= len(all_comments[post])

                log.debug('Comments limit is {}'.format(limit))

            log_info('Get comments for posts {}, time elapsed {}'.format(list(all_comments.keys()), (
                    datetime.now(timezone.utc) - timestart).seconds), username, None)

            # Send data to result queue with export options
            comments = []
            if option_export_random:
                log.debug('{' + '"message":"Ready to export random comments","message_tag":""' + '}')

                temp = []
                for index, item in all_comments:
                    temp += item

                option_export_random = option_export_random if len(temp) > option_export_random else len(
                    temp)
                list_of_random = random.sample(temp, option_export_random)  # Get random

                # Get only texts
                comments = [{'text': d['text'], 'id': d['id']} for d in list_of_random if 'text' in d]

            # Check if file exist in S3
            if option_store_s3:
                filename = str(token) + '.json'
                log.debug('{' + '"message":"Send scraping comments to file in s3 {}",'.format(
                    filename) + '"message_tag":""' + '}')

                # If file exists - if we need to random export daata - get it, decoding json and create data like
                # at next step
                path = custom_f.store_results_to_file(path_posts, filename, json.dumps(all_comments))
                status = s3.upload_file(path, 'comments-account-bucket', filename)
                # status = minioS3.upload_file(path, 'comments-bucket', filename)
                if status and not option_store_local_json:
                    os.remove(path)
                log_info('Storing comments file to s3 complete', username)

            # Send if only we have not null list of text
            if len(comments) > 0 and routing_return and option_export_random:
                log_info("Number of comments for {} is {}".format(token, len(comments)), username)
                amqp_send_message(ch, method,
                                  amqp_request_f.answer_string_all_comments_scraping_complete(comments, 'comments',
                                                                                              project_flow, posts,
                                                                                              token),
                                  routing_return, option_priority)

            ch.basic_ack(delivery_tag=method.delivery_tag)

            if routing_return:
                amqp_send_message(ch, method,
                                  amqp_request_f.answer_string_all_comments_scraping_complete([], 'comments',
                                                                                              project_flow, posts,
                                                                                              token),
                                  routing_return, option_priority)

            for post in posts:
                if custom_f.check_file_exist(path_comments + '/' + str(post) + '.json'):
                    os.remove(path_comments + '/' + str(post) + '.json')

    # -----------------------------------------------------------------------------------------------------------------
    #  Get account stat.
    #

    elif task == 'get-account-location-list':

        # Get input list
        if not isinstance(json_body['completed'], list):
            try:
                usernames = list(filter(None, ast.literal_eval(json_body['completed'])))
            except:
                usernames = [json_body['completed']]
        else:
            usernames = json_body['completed']

        log.debug('Username is {},count ={}'.format(usernames, len(usernames)))

        # If usernames is empty - ending task
        if len(usernames) == 0:
            log.debug(
                '{' + '"message":"Send message to notice about empty usernames list","message_tag":""' + '}')

            # Send message to queue
            ch.basic_ack(delivery_tag=method.delivery_tag)
            answer = '{' + '"task":"{}-error","datetime":"{}","token":"{}",'.format(task, timenow, token) + \
                     '"error":"There is\'nt locations to check","message":"{}"'.format(message) + '}'
            amqp_send_message(ch, method, answer, '', 1, True)
            return

        for username in usernames:

            option_store_s3 = True

            log.debug('Option store s3 is %s', option_store_s3)

            # Get account data from instagram to compare last posts sended
            Instagram.config_set_export_dict(True, True, False)
            account = Instagram.get_instagram_data_for_accounts_by_username([username])

            if option_location_posts:

                try:
                    if len(account[0]['posts']) < 1:
                        continue

                    if account[0]['account']['error'] == 404 or not len(account[0]['posts']):
                        continue

                    if account[0]['account']['private']:
                        continue
                except Exception as err:
                    log.exception(account)
                    raise

                try:
                    # result = get_posts_from_minio_storage(username, account[0]['posts'])
                    result = get_posts_from_s3(username, account[0]['posts'])
                except Exception as err:
                    log.exception(err)
                    continue

                if result['error'] == 400:
                    json_body['project_flow'].insert(0, amqp_request_f.request_get_all_posts('public', option_priority,
                                                                                             1000,
                                                                                             option_mysql_instance))

                    amqp_send_message(ch, method, json.dumps(json_body), 'public', option_priority)
                    continue
                else:
                    posts = result['posts']

                locations = [{'id': d['location']['id'], 'created': d['created']} for d in posts if d['location']]

                if routing_return:
                    username_project_flow = [amqp_request_f.request_check_location_by_location_ids(
                        option_mysql_instance, 50,
                        '')] + project_flow
                    amqp_send_message(ch, method,
                                      amqp_request_f.answer_string_statistic_complete(locations, username_project_flow,
                                                                                      [username],
                                                                                      token),
                                      routing_return, option_priority)

            if routing_return and option_location_info and account[0]['account']['business']:
                username_project_flow = [amqp_request_f.request_check_account_location_by_info('',
                                                                                               option_mysql_instance,
                                                                                               50,
                                                                                               '')] + project_flow
                amqp_send_message(ch, method,
                                  amqp_request_f.answer_string_check_location_from_info([], username_project_flow,
                                                                                        [account[0]['account'][
                                                                                             'user_id']], token),
                                  config.AMQP_QUEUE_INFO, option_priority)

            if routing_return and option_location_bio:
                username_project_flow = [amqp_request_f.request_check_location_by_bio(option_mysql_instance, 50,
                                                                                      '')] + project_flow
                amqp_send_message(ch, method,
                                  amqp_request_f.answer_string_check_location_from_bio([], username_project_flow,
                                                                                       account[0]['account'][
                                                                                           'bio'], token),
                                  config.AMQP_QUEUE_LOCATION, option_priority)

        log_info("Task {} for usernames {} is complete".format(task, usernames), None)
        ch.basic_ack(delivery_tag=method.delivery_tag)

    # -----------------------------------------------------------------------------------------------------------------
    #  Get locations by usaername.
    #
    elif task == 'get-account-engagement-rate':

        # Get input list
        if not isinstance(json_body['completed'], list):
            try:
                usernames = list(filter(None, ast.literal_eval(json_body['completed'])))
            except:
                usernames = [json_body['completed']]
        else:
            usernames = json_body['completed']

        option_store_s3 = True

        # If usernames is empty - ending task
        if len(usernames) == 0:
            log.debug("Send message to notice about empty usernames list")

            # Send message to queue
            ch.basic_ack(delivery_tag=method.delivery_tag)
            answer = '{' + '"task":"{}-error","datetime":"{}","token":"{}",'.format(task, timenow, token) + \
                     '"error":"There is\'nt locations to check","message":"{}"'.format(message) + '}'
            amqp_send_message(ch, method, answer, '', 1, True)
            return

        for username in usernames:

            log_info(
                'Username is {},count ={}, Option store s3 is {} '.format(usernames, len(usernames), option_store_s3),
                username)

            # Check if posts file have already saved in S3
            filename = str(username + '.json')

            last_post_in_s3_date = None
            posts = []

            # Get account data from instagram to compare last posts sended
            Instagram.config_set_export_dict(True, True, False)
            account = Instagram.get_instagram_data_for_accounts_by_username([username])

            status = True
            try:

                if account[0]['account']['error'] == 404:
                    log_warning('Account 404',username)
                    status = False

                if account[0]['account']['private']:
                    log_warning('Account is private',username)
                    status = False

                if len(account[0]['posts']) < 1:
                    log_warning('Account have no posts',username)
                    status = False

            except Exception as err:
                log.exception(err)
                raise

            if not status and routing_return:

                log_info('Send to next task because this account hasnt posts to scrape',username)
                ch.basic_ack(delivery_tag=method.delivery_tag)
                amqp_send_message(ch, method,
                                  amqp_request_f.answer_default(project_flow,token,[username],[]),
                                  routing_return,
                                  option_priority)

                time.sleep(3)

                return

            try:
                result = get_posts_from_s3(username, account[0]['posts'])
            except Exception as err:
                log.exception(err)
                continue

            if result['error'] == 400:
                json_body['project_flow'].insert(0,
                                                 amqp_request_f.request_get_all_posts('public', option_priority, 1000,
                                                                                      option_mysql_instance))

                amqp_send_message(ch, method, json.dumps(json_body), 'public', option_priority)
                continue
            else:
                posts = result['posts']

            stat_er = Instagram.get_engagement_rate(account[0]['account'], posts[:option_limit_er])

            stat_ltcr = Instagram.get_engagement_rate(account[0]['account'], posts[:option_limit_ltcr])

            data = {
                'follow': float(stat_er['follow']),
                'followers': float(stat_er['followers']),
                'posts': float(stat_er['posts']),
                'avr_likes': float(stat_er['avr_likes']),
                'updated': timenow,
                'pd': float(stat_er['pd']),
                'pm': float(stat_er['pm']),
                'er': float(stat_er['er']),
                'ltcr': float(stat_ltcr['ltcr']),
                'username': str(username),
                'bio': str(stat_er['bio']),
                'full_name': str(stat_er['full_name']),
            }

            log_info("Get stat for username {}, stat {}".format(username, data), username, None)

            if option_mysql_instance > 0:
                export_keys = {'pd': 'pd', 'pm': 'pm', 'er': 'er', 'ltcr': 'ltcr', 'username': 'username',
                               'follow': 'follow', 'followers': 'followers', 'posts': 'posts', 'updated': 'updated'}
                answer = amqp_request_f.answer_store_data_mysql('account', data, export_keys, option_mysql_instance)
                amqp_send_message(ch, method, answer, 'mysql', 50)

            if routing_return:
                amqp_send_message(ch, method,
                                  amqp_request_f.answer_string_statistic_complete(data, project_flow, [username],
                                                                                  token),
                                  routing_return, option_priority)

        ch.basic_ack(delivery_tag=method.delivery_tag)
        return

    # -----------------------------------------------------------------------------------------------------------------
    #  Task not found
    #

    else:
        ch.basic_ack(delivery_tag=method.delivery_tag)
        # Default message "task not found"
        answer = {
            "task": "public-{}-error".format(instance),
            "daterime": timenow,
            "parsed":task,
            "message": "{}" .format(json.dumps(json_body))
        }
        amqp_send_message(ch, method, answer, '', 1, True)

    # if mysql:
    #    mysql.close()

    return


def get_posts_from_s3(username, posts_ig, send_to_update_posts=False):
    filename = str(username) + '.json'

    content = s3.get_object_content(s3_posting_bucket, filename)

    posts = json.loads(content)

    log.debug('Number posts in s3 is {}'.format(len(posts)))

    posts_date = find_date_in_posts(posts, posts_ig)

    log.debug('Posts date status {}'.format(posts_date))

    if posts_date['first_post_date_instagram'] > posts_date['last_post_date_from_s3']:
        log.debug('There is new posts in IG')
        return {'posts': [], 'error': 400}

    if posts_date['first_post_date_instagram'] < posts_date['last_post_date_from_s3'] and posts_date[
        'last_post_date_from_s3'] != posts_date['last_post_date_instagram']:
        log.debug('There is new posts in IG, but we get post older then in s3 so we can use this one')

        # Get new posts older than exists
        posts_new = Instagram.return_posts_not_older_then_datetime(posts_ig, posts_date['last_post_date_from_s3'])

        posts = posts_new + posts

        # Store data to Minio
        path = custom_f.store_results_to_file(path_posts, filename, json.dumps(posts))
        status = s3.upload_file(path, s3_posting_bucket, filename)
        log.debug('{' + '"message":"Storing at S3 file complete","message_tag":""' + '}')
        if status:
            log.debug('{' + '"message":"Storing at s3 file complete","message_tag":""' + '}')
            os.remove(path)

    return {'posts': posts, 'error': None, 'dates': posts_date}


def get_posts_from_minio_storage(username, posts_ig, send_to_update_posts=False):
    # Check if posts file have already saved in S3
    filename = str(username) + '.json'

    posts = []

    content = get_content_from_minio_storage(s3_posting_bucket, filename, True)

    if content['error']:
        log_warning("Error during get content from Storage", filename)
        if content['error'] == 401:
            s3.delete_file(s3_posting_bucket, filename)
            minioS3.delete_file(s3_posting_bucket, filename)

        if content['error'] == 402:
            return {'posts': [], 'error': 400}

        if content['error'] == 403:
            s3.delete_file(s3_posting_bucket, filename)
            minioS3.delete_file(s3_posting_bucket, filename)
            return {'posts': [], 'error': 400}

    else:
        posts = content['content']

    if 'n/a' in posts:
        posts.remove('n/a')

    posts_date = find_date_in_posts(posts, posts_ig)

    if posts_date['first_post_date_instagram'] > posts_date['last_post_date_from_s3']:
        log.debug('There is new posts in IG')
        return {'posts': [], 'error': 400}

    if posts_date['first_post_date_instagram'] < posts_date['last_post_date_from_s3'] and posts_date[
        'last_post_date_from_s3'] != posts_date['last_post_date_instagram']:
        log.debug('There is new posts in IG, but we get post older then in s3 so we can use this one')

        # Get new posts older than exists
        posts_new = Instagram.return_posts_not_older_then_datetime(posts_ig, posts_date['last_post_date_from_s3'])

        posts = posts_new + posts

        # Store data to Minio
        path = custom_f.store_results_to_file(path_posts, filename, json.dumps(posts))
        status = minioS3.upload_file(path, s3_posting_bucket, filename)
        log.debug('{' + '"message":"Storing at Minio file complete","message_tag":""' + '}')
        if status:
            log.debug('{' + '"message":"Storing at Minio file complete","message_tag":""' + '}')
            os.remove(path)

    return {'posts': posts, 'error': None, 'dates': posts_date}


def find_date_in_posts(posts, posts_ig):
    if len(posts) > 0 and posts[0] != 'n/a':
        log.debug('Number of posts is {}'.format(len(posts)))
        try:
            last_date_index = max(range(len(posts)), key=lambda index: posts[index]['created'])
            last_post_date_from_s3 = datetime.utcfromtimestamp(posts[last_date_index]['created']).replace(
                tzinfo=timezone.utc)
        except Exception as err:
            log.exception('{} - {}'.format(posts, err))
            raise
    else:
        last_post_date_from_s3 = datetime.utcfromtimestamp(1).replace(tzinfo=timezone.utc)

    last_date_index = max(range(len(posts_ig)), key=lambda index: posts_ig[index]['created'])
    first_date_index = min(range(len(posts_ig)), key=lambda index: posts_ig[index]['created'])

    last_post_date_instagram = datetime.utcfromtimestamp(posts_ig[last_date_index]['created']).replace(
        tzinfo=timezone.utc)
    first_post_date_instagram = datetime.utcfromtimestamp(posts_ig[first_date_index]['created']).replace(
        tzinfo=timezone.utc)

    log.debug('Last post in IG {}, first post in IG {}, Last post in S3 {}'.format(last_post_date_instagram,
                                                                                   first_post_date_instagram,
                                                                                   last_post_date_from_s3))

    return {'first_post_date_instagram': first_post_date_instagram,
            'last_post_date_instagram': last_post_date_instagram,
            'last_post_date_from_s3': last_post_date_from_s3}


#
# Check if exists content in Minio/S3.
# If we need to remove current file in Minio/S3 - return error code 401
# If we need to update data - return error code 402
# If we need to remove current file in Minio/S3 and update data - return error code 403
#
def get_content_from_minio_storage(bucket_name, filename, check_from_s3=False, option_store_local_json=False):
    content = []

    while not len(content):

        # Check if file exists in Minio s3
        file_with_posts_exists_minio = minioS3.check_file_exist(bucket_name, filename)

        if file_with_posts_exists_minio:
            log.debug('File exists in Minio S3')

            # Get content
            content_json = minioS3.get_object_content(bucket_name, filename)
            try:
                content = json.loads(content_json)
            except Exception as err:
                log.exception(err)
                return {'content': [], 'error': 403}
            return {'content': content, 'error': None}
        # If not exists in Minio - get data from s3
        else:
            log.debug('File do not exists in Minio')

            if check_from_s3:
                # Check if file exists in AWS s3
                if s3.check_file_exist(bucket_name, filename):
                    object_data = s3.get_key_option(bucket_name, filename)

                    log.debug(
                        '{' + '"message":"Filename {} exists in store in s3, his LastModified date is {}",'.format(
                            filename,
                            object_data['LastModified']) + '"message_tag":"input-data"' + '}')

                    # Get content and store to minio
                    content_json = s3.get_object_content(bucket_name, filename)
                    try:
                        content = json.loads(content_json)
                    except Exception as err:
                        log.exception(err)
                        return {'content': [], 'error': 401}

                    path = custom_f.store_message_to_json_file(path_posts, filename, content)
                    status = minioS3.upload_file(path, bucket_name, filename)
                    if status and not option_store_local_json:
                        os.remove(path)
                    if debug:
                        log_info("Storing at S3 file complete", filename)


                else:
                    # todo: resend to get posts data for that user
                    log.debug(
                        'File {} not exists in both storage - send request to get all posts and send back to get ER'.format(
                            filename))
                    return {'content': [], 'error': 402}

    return {'content': content, 'error': None}


def looping_results(data, key, index, export_key, func_name=''):
    result = []
    error404 = []
    recheck = []
    proxy_errors_counter = 0

    # Looping results
    if isinstance(data, list):
        for item in data:
            try:
                if 'error' in item[key]:
                    if item[key]['error'] == 404:
                        #  Element is not created or deleted
                        try:
                            error404.append(item[key][index])
                        except Exception as err:
                            log.exception(err)
                            log.error(item)
                            quit()
                    elif item[key]['error'] == 408:
                        log.error(
                            '{' + '"message":"Timeout exception - need to change proxy to another","message_tag":""' + '}')
                        proxy_errors_counter += 1
                    elif item[key]['error'] == 429:
                        recheck.append(item[key][index])
                        log_warning('429 error ', str(item[key][index]))
                        proxy_errors_counter += 1
                    elif item[key]['error']:
                        log_warning('Error during execution', item[key])
                        proxy_errors_counter = max(
                            item[key]['proxy_error_counter'] if 'proxy_error_counter' in item[
                                key] else 0,
                            proxy_errors_counter)
                        proxy_errors_counter += 1
                        recheck.append(item[key][index])
                    else:
                        # Compeleted getting results
                        item[key]['error'] = 0
                        item[key]['status'] = 2

                        export = {}
                        for i in export_key:
                            export[i] = item[i]
                        # Append to result array
                        result.append(export)
                else:
                    result.append(item)
            except Exception as e:
                log.exception('Error {}'.format(data))
                quit()

    return {'complete': result, 'error404': error404, 'recheck': recheck, 'proxy_errors': proxy_errors_counter}


def get_usernames_list_by_options(input, option_force_check, option_store_redis):
    usernames = []

    # Check if data exist in redis
    if not option_force_check:  # Check for "Force" checking
        log.debug('{' + '"message":"Not forced check","message_tag":"input-data"' + '}')
        if option_store_redis:  # Check if we use Redis
            log.debug('{' + '"message":"Checking in redis","message_tag":"redis-log"' + '}')
            rows = check_ids_for_exist_in_redis(input)  # Get data from redis
            log.debug(
                '{' + '"message":"Rows in redis = {}",'.format(len(rows)) + '"message_tag":"input-data"' + '}')
            for item in input:
                if isinstance(item, dict):  # Check if dict or list
                    if item['u'] not in rows:
                        usernames.append(item['u'])
                else:
                    usernames.append(item)
        else:
            for item in input:
                if isinstance(item, dict):  # Check if dict or list
                    usernames.append(item['u'])
                else:
                    usernames.append(item)
    else:
        for item in input:
            if isinstance(item, dict):  # Check if dict or list
                usernames.append(item['u'])
            else:
                usernames.append(item)
    return usernames


def check_ids_for_exist_in_redis(ids):
    result = []
    not_id = 0
    for item in ids:
        if isinstance(item, dict):
            value = 0
            try:
                if 'id' in item:
                    value = redis.get_value_by_int(int(item['id']), bucket_size)
            except Exception as e:
                log.exception(
                    'Error check_ids_for_exist_in_redis id={}, bucket size={}'.format(item['id'],
                                                                                      bucket_size))

            if value and 'u' in item:
                result.append(item['u'])
        else:
            not_id += 1

    if not_id:
        log.debug('List elements without ids = %s', not_id)

    return result


def get_ua_dict():
    content = custom_f.get_file_content(config.USERAGENTS_PATH)
    data = json.loads(content[0])
    return data


if __name__ == '__main__':

    test_url = 'https://www.instagram.com'

    app = 'public'
    instance = 61
    amqp_instance = 3
    virtual_host = 'prod'
    exchange = 'instagram-exchange'
    bucket_size = config.REDIS_ACCOUNTS_BUCKETS_KEYS
    debug = False

    if 'instance' in os.environ:
        instance = int(os.environ['instance'])

    if 'amqp' in os.environ:
        amqp_instance = int(os.environ['amqp'])

    if 'virtual_host' in os.environ:
        virtual_host = str(os.environ['virtual_host'])

    if 'exchange' in os.environ:
        exchange = str(os.environ['exchange'])

    if 'bucket_size' in os.environ:
        bucket_size = int(os.environ['bucket_size'])

    if 'debug' in os.environ:
        debug = bool(os.environ['debug'])

    if 'docker' in os.environ:
        fluent_host = config.LOG_FLUENT_HOST_DOCKER
    else:
        fluent_host = config.LOG_FLUENT_HOST

    if len(sys.argv) > 1:
        instance = int(sys.argv[1])

    if len(sys.argv) > 2:
        amqp_instance = int(sys.argv[2])

    if len(sys.argv) > 3:
        bucket_size = int(sys.argv[3])

    if len(sys.argv) > 4:
        virtual_host = str(sys.argv[4])

    if len(sys.argv) > 5:
        exchange = str(sys.argv[5])

    if len(sys.argv) > 6:
        debug = bool(int(sys.argv[6]))

    custom_f = CustomFunction(instance, debug)

    # Set logger
    log = custom_f.set_logging(instance, None, 'fluent', app, 'application.assem.{}.{}'.format(app, virtual_host),
                               {'host': fluent_host, 'port': config.LOG_FLUENT_PORT},
                               'INFO' if not debug else 'DEBUG', ['s3transfer', 'pika'],
                               ['botocore', 'urllib3', 'requests.packages.urllib3.connectionpool'], virtual_host)

    amqp_request_f = AMQPRequests(log)

    ua = get_ua_dict()

    log_info("Start public.py", None)

    routing_request_config = config.AMQP_QUEUE_PUBLIC
    # routing_request_config = 'z_test'
    routing_return_config = config.AMQP_QUEUE_APPLICATION
    routing_error_config = config.AMQP_QUEUE_ERROR
    routing_redis_config = config.AMQP_QUEUE_REDIS
    routing_mysql_config = config.AMQP_QUEUE_MYSQL

    # Get AQMP details
    amqp_creds = custom_f.get_amqp_by_key(amqp_instance)

    # Set RabbitMQ
    Amqp = AMQP(
        exchange,
        routing_request_config,
        virtual_host,
        amqp_creds['host'],
        amqp_creds['port'],
        amqp_creds['login'],
        amqp_creds['pass'],
        amqp_callback,
        log,
        debug
    )

    s3_posting_bucket = 'posting-bucket'
    s3_comments_account_bucket = 'comments-account-bucket'

    try:

        # Star Insatgram class
        Instagram = InstagramParsing(20, log, instance, debug)

        # setttings
        Instagram.config_set_ua_local(True, ua)

        # Set keys to export to Mysql
        insert_keys = {
            'username': 'username',
            'follow': 'follow',
            'followers': 'followers',
            'posts_count': 'posts',
            'bio': 'bio',
            'er': 'er',
            'df': 'df',
            'fp': 'fp',
            'error': 'error',
            'status': 'status',
            'current_datetime': 'updated',
            'business': 'business',
            'user_id': 'user_id',
            'category': 'category',
            'private': 'private'}

        # Set proxy to parser
        log.debug('Try to get proxy')
        proxy_errors_counter = 0
        proxy_path = custom_f.get_proxy()
        Instagram.config_set_proxy(True, {'http': proxy_path, 'https': proxy_path})

        # Start Redis
        redis = RedisClass(log, config.REDIS_ACCOUNTS_HOST, config.REDIS_ACCOUNTS_PORT, config.REDIS_PASSWORD)
        redis_location = RedisClass(log, config.REDIS_LOCATION_HOST, config.REDIS_LOCATION_PORT,
                                    config.REDIS_LOCATION_PASSWORD)

        # Store posts on local
        path_posts = config.INSTAGRAM_POSTS_LOCAL_STORE_PATH
        path_comments = config.INSTAGRAM_COMMENTS_LOCAL_STORE_PATH

        custom_f.check_folder_exist(path_posts)
        custom_f.check_folder_exist(path_comments)

        minioS3 = MinioClass(log, config.MINIO_S3_HOST, config.MINIO_S3_ACCESS_KEY_ID,
                             config.MINIO_S3_ACCESS_KEY_SECRET)
        minioS3.connect_client()

    except Exception as e:
        log.exception(e)
        quit()

    # s3 connection
    s3 = S3(log, config.AWS_S3_ACCESS_KEY_ID, config.AWS_S3_ACCESS_KEY_SECRET)
    s3.connect_client()

    try:
        Amqp.run()
    except KeyboardInterrupt:
        Amqp.stop()
    except Exception as e:
        log.exception('Global raise')
        time.sleep(20)
        pass
