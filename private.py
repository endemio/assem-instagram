import config
import logging
import json
import os
import random
from datetime import datetime, timezone
import time
import requests
import sys
import ast

from module.instagram.privateapi import PrivateAPI
from module.rabbitmq.amqp import AMQP
from module.redis.main import RedisClass
from module.aws.s3 import S3
from module.custom.functions import CustomFunction
from module.custom.amqp_requests import AMQPRequests


def log_info(message, username, tag=None):
    log.info({
        'message': message,
        'username': username,
        'link': custom_f.get_link(),
        'token': custom_f.get_token(),
        'virtual_host': virtual_host,
        'message_tag': tag
    })


def log_warning(message, username, tag=None):
    log.warning({
        'message': message,
        'username': username,
        'link': custom_f.get_link(),
        'token': custom_f.get_token(),
        'virtual_host': virtual_host,
        'message_tag': tag
    })

# docker rmi -f $(docker images | grep "<none>" | awk "{print \$3}")

def amqp_send_message(ch, method, answer, routing_key, priority=1, error=False, delivery_mode=2):
    routing = routing_key if not error else routing_error_config

    try:
        json_object = json.loads(answer)
    except:
        answer = json.dumps(answer, default=lambda o: o.__dict__, indent=4)
        pass

    try:
        ch.basic_publish(
            exchange=Amqp.get_exchange(),
            routing_key=routing,
            body=answer,
            properties=Amqp.basic_properties(delivery_mode, priority))
    except Exception as e:
        log.error("Error while sending message to result queue " + str(e))

    log_info("Send message to {} queue, message len =  {}, text = {}".format(routing, len(answer), json.loads(answer)),None)


def amqp_callback(ch, method, properties, message):
    message = message.decode('utf-8')
    start_datetime = datetime.now(timezone.utc)
    log_info("Get Body: {}".format(message),None)

    # Parsing input message
    try:
        json_body = json.loads(message)
    except Exception as err_exception:
        log.exception(' Get exception at Body message JSON parsing ' + str(err_exception))
        answer = '"task":"task-parsing-error","datetime":"{}","status":false,"error":"{}","received":"{}"'.format(
            str(start_datetime), ' Get exception at JSON parsing ' + str(err_exception), message)
        ch.basic_ack(delivery_tag=method.delivery_tag)
        amqp_send_message(ch, method, answer, routing_error_config, 1)
        return

    # Project flow
    project_flow = list(json_body['project_flow']) if 'project_flow' in json_body else {}
    if not project_flow:
        # Default answer
        answer = '"task":"project-flow-error","status":false,"datetime":"{}","error":"No tasks found"'.format(
            start_datetime)
        ch.basic_ack(delivery_tag=method.delivery_tag)
        amqp_send_message(ch, method, answer, routing_error_config, 1)
        return
    else:
        body = project_flow.pop(0)

    # Task
    task = str(body['task']) if 'task' in body else ''
    token = str(body['token']) if 'token' in body else ''
    routing_return = str(body['return_queue']) if 'return_queue' in body else routing_return_config
    results_limit = int(body['limit']) if 'limit' in body else number_check_limit

    # Check option
    option_force_check = bool(body['force_check']) if 'force_check' in body else False
    option_force_check = False

    # Store options
    option_store_local_json = bool(body['store_json']) if 'store_json' in body else False
    option_store_s3 = bool(body['store_s3']) if 'store_s3' in body else False

    # Export options
    option_export_random = bool(body['export_random']) if 'export_random' in body else False
    option_export_full = bool(body['export_full']) if 'export_full' in body else False
    option_export_random_limit = int(body['send_limit']) if 'send_limit' in body else number_random_export_limit
    option_file_append = str(body['file_token']) if 'file_token' in body else ''

    # AMQP option
    option_priority = int(body['priority']) if 'priority' in body else 1

    # Get input list
    username = json_body['completed']

    if not username:
        log.error({
            'message': 'Username not found in message {}'.format(json_body),
            'username': '',
            'token': token,
            'message_tag': '{}.{}'.format('private.get_followers', '')
        })

        ch.basic_ack(delivery_tag=method.delivery_tag)
        answer = '{' + '"task":"{}-error","datetime":"{}","status":false,"token":"{}","error":"{}"'.format(
            task, token, start_datetime, 'Usernames not found') + '}'
        amqp_send_message(ch, method, answer, routing_error_config, 1)
        return

    # if not instagram_account_turn_on:
    # username = PrivateAPI.foo_get_random_username()

    # todo: add redis checker flag that this account is checking, using ttl

    # Get list followers for account
    if task == 'get-followers-list':

        # Get input list
        if not isinstance(json_body['completed'], list) and not isinstance(json_body['completed'], str):
            input_list = list(filter(None, ast.literal_eval(json_body['completed'])))
        elif isinstance(json_body['completed'], str):
            input_list = [json_body['completed']]
        else:
            input_list = json_body['completed']

        if len(input_list) > 1:
            for username in input_list:
                project_flow = [body] + project_flow
                amqp_send_message(ch, method, amqp_request_f.answer_default(project_flow, username, username), 'private',
                                  option_priority)
            return
        else:
            input = input_list[0]

        username = input

        # Result filename for S3/Local store
        filename = str(username) + str(option_file_append) + '.json'
        filename_temp = str(username) + '-temp.json'

        temp_followers = []
        number_follower = 0

        # Check if followers have already saved in S3

        log_info('Start task {}, s3 store {},force_check'.format(task, option_store_s3, option_force_check),username)

        next_max_id = redis_temp.get_value_by_key(str(username) + '-followers-next_max_id')
        checker_login = redis_temp.get_value_by_key(str(username) + '-instagram-account')
        checker_login = '' if not checker_login else checker_login

        log_info('Checker is {}, next_max_id is {}'.format(checker_login, next_max_id),username)

        checker_compare = False

        if checker_login:
            if checker_login == instagram_login:
                checker_compare = True
            else:
                # Return message to queue - its not owner of scraping

                log.debug({
                    'message': 'Not owner of check. Return',
                    'username': username,
                    'token': token,
                    'message_tag': '{}.{}'.format('private.get_followers', '')
                })

                if not instagram_account_turn_on: time.sleep(5)

                ch.basic_ack(delivery_tag=method.delivery_tag)
                amqp_send_message(ch, method, message, routing_request_config,
                                  option_priority - 1 if option_priority > 1 else 0)

                time.sleep(2)
                return

        if next_max_id and checker_compare:
            log_info('Next_max_id and checker_compare True',username)

            if s3.check_file_exist(s3_followers_bucket, filename_temp):

                log_info('File {} exists'.format(filename_temp),username)

                temp_followers_json = s3.get_object_content(s3_followers_bucket, filename_temp)
                temp_followers = json.loads(temp_followers_json)
                number_follower = len(temp_followers)

                log_info('Number followers in S3 is {}'.format(number_follower),username)

                if not instagram_account_turn_on: time.sleep(5)
            else:
                log_info('File {} NOT exists, clear next_max_id and checker_login'.format(filename_temp),username)
                next_max_id = ''

        # Check if we have already file with followers in S3 if next_max_id is empty
        if option_store_s3 and not next_max_id:

            if s3.check_file_exist(s3_followers_bucket, filename):

                log_info("File {} exists in S3".format(filename),username)

                # Get object data to check time modified
                object_data = s3.get_key_option(s3_followers_bucket, filename)

                # If object modified too much time ago - repeat
                if (start_datetime - object_data['LastModified']).seconds > 72 * 3600 and option_force_check:

                    log_info("file with followers is too old, continue work - get full followers list",username)

                else:
                    log_info('Result file {} created less then {} seconds ago, get this data and send'.format(filename, 72 * 3600),username)

                    # Send to result queue (with export random account or not)
                    if option_export_random or option_export_full:

                        log_info('Get object content',username)

                        followers_json = s3.get_object_content(s3_followers_bucket, filename)
                        followers = json.loads(followers_json)

                        # Get export list to send for checking
                        export = get_export_list(followers, option_export_random, option_export_full,
                                                 option_export_random_limit)

                        sending_accounts = []
                        for item in export:
                            if 'c' in item:
                                sending_accounts.append(item)

                        # todo: Change routing return!!!!!!
                        if len(sending_accounts) > 0:
                            # Send list of export to queue "account" to check this usernames for data
                            parts = divide_chunks(sending_accounts, amqp_send_limit)
                            # for part in parts:
                            # amqp_send_message(ch, method,
                            #    answer_string_scraping_complete(part, 'followers', project_flow, username),
                            #    routing_error_config, option_priority)

                        else:
                            answer = '{' + '"task":"{}-error","datetime":"{}","status":false,"error":"{}"'.format(
                                task, start_datetime,
                                'Export error for user ' + str(username) + ' - export list is empty') + '}'

                            amqp_send_message(ch, method, answer, routing_error_config, 1, True)

                    else:
                        log_info("Send message to result queue {} by found file in s3".format(routing_return),username)

                        answer = amqp_request_f.answer_default(project_flow,username,[username],[])

                        amqp_send_message(ch, method, answer, routing_return, option_priority)

                    ch.basic_ack(delivery_tag=method.delivery_tag)
                    return
            else:
                log_info("File not {} exists in S3".format(filename),username)

        log_info("next_max_id is {}, checker is {}".format(next_max_id, checker_login),username)

        # Start get followers from list
        try:
            data = privateAPI.get_total_followers(username, 0, results_limit, next_max_id, number_follower)
        except Exception as err_exception:
            log.exception(err_exception)
            data = {'followers': [], 'error': [str(err_exception)]}
            pass

        log_info("Number of scraped account is {}".format(len(data['followers'])),username)

        if 'error' in data:
            if len(data['error']):
                log.error({
                    'message': 'Error in data is {}'.format(data['error']),
                    'username': username,
                    'token': token,
                    'message_tag': '{}.{}'.format('private.get_followers', '')
                })

        if 'next_max_id' in data:

            redis_temp.insert_data_key_value(username + '-followers-next_max_id', str(data['next_max_id']), 3 * 3600)
            redis_temp.insert_data_key_value(username + '-instagram-account', str(instagram_login), 3 * 3600)

            temp_followers.extend(data['followers'])
            temp_followers = list({v['id']: v for v in temp_followers}.values())
            number_follower = len(temp_followers)

            log_info("Number of scraped account is {}, summary unique accounts is {}".format(len(data['followers']),number_follower),username)

            filename = filename_temp

            # Store to temp file for followers
            if option_store_s3:
                log_info("Storing at S3 file to {}".format(filename),username)
                try:
                    path = custom_f.store_message_to_json_file(path_followers, filename, temp_followers)
                except Exception as err:
                    log.exception(err)
                    time.sleep(120)
                    raise
                status = s3.upload_file(path, s3_followers_bucket, filename)
                if status and not option_store_local_json:
                    os.remove(path)

            # Return message back to queue
            log_info('Return message to queue',username)

            amqp_send_message(ch, method, message, routing_request_config, option_priority + 1)

            ch.basic_ack(delivery_tag=method.delivery_tag)

            return
        else:
            log_info('next_max_id is Null, set 0 to account and next_max_id in temp redis DB',username)

            redis_temp.insert_data_key_value(username + '-followers-next_max_id', 0, 1)
            redis_temp.insert_data_key_value(username + '-instagram-account', 0, 1)

        log_info("Number of received followers {}, error {}".format(len(data['followers']), data['error']),username)

        # Check if there is error after checking
        if data['error']:
            for item in data['error']:
                if item == 'json_error_parsing':
                    log.error({
                        'message': "json_error_in_followers_parsing",
                        'username': username,
                        'token': token,
                        'message_tag': '{}.{}'.format('private.get_followers', '')
                    })

                    if not s3.check_file_exist(s3_followers_bucket, filename):
                        log_info("repeated send to request queue",username)

                        amqp_send_message(ch, method, message, routing_request_config, option_priority)

                        ch.basic_ack(delivery_tag=method.delivery_tag)
                    else:
                        log_info("File already exists in s3",username)

                        answer = '{' + '"task":"{}-error","datetime":"{}","status":false,"error":"{}"'.format(
                            task, start_datetime,
                            'User ' + str(username) + ' has json_error_in_followers_parsing') + '}'
                        amqp_send_message(ch, method, answer, routing_error_config, 1)

                        ch.basic_ack(delivery_tag=method.delivery_tag)
                    return

                if item == 'not_found_user_error_parsing':
                    answer = '"task":"{}-error","datetime":"{}","status":false,"error":"{}"'.format(
                        task, start_datetime, 'User ' + str(username) + ' not found (404 error)')
                    amqp_send_message(ch, method, answer, routing_error_config, 1)

                    ch.basic_ack(delivery_tag=method.delivery_tag)
                    return

                if item == 'user_is_private':
                    answer = '"task":"{}-error","datetime":"{}","status":false,"error":"{}"'.format(
                        task, start_datetime, 'User ' + str(username) + ' is private')

                    amqp_send_message(ch, method, answer, routing_error_config, 1)

                    ch.basic_ack(delivery_tag=method.delivery_tag)
                    return

        data['followers'].extend(temp_followers)
        data['followers'] = list({v['id']: v for v in data['followers']}.values())

        log_info("Number of all scraped followers is {}".format(len(data['followers'])),username)

        if len(data['followers']) > 0:
            log_info("Export random is {}, Export full list is {}, Export random limit is {}".format(
                    option_export_random, option_export_full, option_export_random_limit),username)

            # Get export list to send for checking
            export = get_export_list(data['followers'], option_export_random, option_export_full,
                                     option_export_random_limit)

            # Export to s3 and local store
            log_info("Storing in s3 to filename {} is {}".format(filename, option_store_s3),username)

            if option_store_s3:
                try:
                    path = custom_f.store_message_to_json_file(path_followers, filename, export)
                except Exception as err:
                    log.exception(err)
                    time.sleep(120)
                    raise
                status = s3.upload_file(path, s3_followers_bucket, filename)
                if status and not option_store_local_json:
                    os.remove(path)

            # Send to result queue
            if option_export_random or option_export_full:
                log_info("Send to get instagram info",username)

                sending_accounts = []
                for item in export:
                    if 'c' in item:
                        sending_accounts.append(item)

                # Send list of export to queue "account" to check this usernames for data
                if len(sending_accounts) > 0:
                    parts = divide_chunks(sending_accounts, amqp_send_limit)
                    for part in parts:
                        amqp_send_message(ch, method,
                                          #answer_string_scraping_complete(part, 'followers', project_flow, username),
                                          amqp_request_f.answer_default(project_flow,username,[username],part),
                                          routing_return, 1)
                else:
                    answer = '{' + '"task":"{}-error","datetime":"{}","status":false,"error":"{}"'.format(
                        task, start_datetime,
                        'Export error for user ' + str(username) + ' - export list is empty') + '}'
                    amqp_send_message(ch, method, answer, routing_error_config, 1, True)
                    ch.basic_ack(delivery_tag=method.delivery_tag)
                return
            else:
                #answer = answer_string_scraping_complete([], 'followers', project_flow, username)
                answer = amqp_request_f.answer_default(project_flow,username,[username],[])

                log_info("Send message {} to {} queue".format(answer, routing_return),username)

                amqp_send_message(ch, method, answer, routing_return, option_priority)
                time.sleep(config.PRIVATE_CHECK_SLEEP)
                ch.basic_ack(delivery_tag=method.delivery_tag)
                return

    #
    #
    # Get list followings for account
    #
    #

    elif task == 'get-followings-list':  # Get list followings for account

        # Get input list
        if not isinstance(json_body['completed'], list):
            input_list = list(filter(None, ast.literal_eval(json_body['completed'])))
        else:
            input_list = json_body['completed']

        if len(input_list) > 1:
            for username in input_list:
                project_flow = [body] + project_flow
                amqp_send_message(ch, method, amqp_request_f.answer_default(project_flow, username, username), 'private',
                                  option_priority)
            return
        else:
            input = input_list[0]

        username = input

        filename = str(username)+'.json'

        # Check if username not in Redis temp DB
        value = redis_temp.get_value_by_key(username + '-followings')
        log_info("Value in Redis temp = {}".format(value),username)

        if not value:
            log_info("Insert True value with ttl to Redis temp",username)
            redis_temp.insert_data_key_value(username + '-followings', 1, int(results_limit / 400))
        else:
            # Send ack that message is delivered
            answer = '{' + '"task":"{}-already-parsing-error","datetime":"{}","username":"{}","filename":"{}",' \
                .format(task, start_datetime, username, filename) \
                     + '"status":false,"error":"{}"'.format('This user is already parsed') + '}'
            amqp_send_message(ch, method, answer, routing_error_config, 1)
            ch.basic_ack(delivery_tag=method.delivery_tag)
            return

        # Check if followers have already saved in S3
        if option_store_s3 and not option_force_check:
            if s3.check_file_exist(s3_followers_bucket, filename) and not option_force_check:
                answer = '{' + '"task":"{}-error","datetime":"{}","status":false,"username":"{}","filename":"{}",' \
                    .format(task, start_datetime, username, filename) + '"error":"Username in S3 exists"' + '}'

                amqp_send_message(ch, method, answer, 'doubled', 1)
                amqp_send_message(ch, method,
                                  #answer_string_scraping_complete([], 'followings', project_flow, username),
                                  amqp_request_f.answer_default(project_flow,username,[username],[]),
                                  routing_return, option_priority)
                ch.basic_ack(delivery_tag=method.delivery_tag)

                return

        # Getting list of followings from account
        log_info("Start get followings for account ",username)
        try:
            data = privateAPI.get_total_followings(username, 0, results_limit)
        except Exception as err_exception:
            log.error(err_exception)
            data = {'followers': [], 'error': []}
            pass
        log_info("Number of received followings ",username)

        # Check if there is error after checking
        if data['error']:
            for item in data['error']:
                if item == 'json_error_parsing':
                    log.error('json_error_in_followings_parsing')
                    if not s3.check_file_exist(s3_followings_bucket, filename):
                        log.error('repeated send to request queue')
                        amqp_send_message(ch, method, message, routing_request_config, option_priority - 1)
                    else:
                        log.error('File already exists in s3')
                        answer = '{' + '"task":"{}-error","datetime":"{}","status":false,"error":"{}"'.format(
                            task, start_datetime,
                            'User ' + str(username) + ' has json_error_in_followings_parsing') + '}'
                        amqp_send_message(ch, method, answer, routing_error_config, 1)
                    ch.basic_ack(delivery_tag=method.delivery_tag)
                    return

                if item == 'not_found_user_error_parsing':
                    answer = '"task":"{}-error","datetime":"{}","status":false,"error":"{}"'.format(
                        task, start_datetime, 'User ' + str(username) + ' not found (404 error)')
                    amqp_send_message(ch, method, answer, routing_error_config, 1)
                    ch.basic_ack(delivery_tag=method.delivery_tag)
                    return

                if item == 'user_is_private':
                    answer = '"task":"{}-error","datetime":"{}","status":false,"error":"{}"'.format(
                        task, start_datetime, 'User ' + str(username) + ' is private')
                    amqp_send_message(ch, method, answer, routing_error_config, 1)
                    ch.basic_ack(delivery_tag=method.delivery_tag)
                    return

        if len(data['followings']) > 0:
            log_info("Export random is {}, Export full list is {}".format(option_export_random,option_export_full),username)

            # Get export list
            export = get_export_list(data['followings'], option_export_random, option_export_full,
                                     option_export_random_limit)

            # Send data to result queue with export options
            if option_export_random or option_export_full:
                log_info("Send to get instagram info",username)
                if len(export) > 0:
                    # Send list of export to queue "account" to check this usernames for data
                    parts = divide_chunks(export, amqp_send_limit)
                    for part in parts:
                        amqp_send_message(ch, method,
                                          #answer_string_scraping_complete(part, 'followings', project_flow, username),
                                          amqp_request_f.answer_default(project_flow,username,[username],part),
                                          routing_return, option_priority)
                    ch.basic_ack(delivery_tag=method.delivery_tag)
                else:
                    answer = '"task":"{}-error","datetime":"{}","status":false,"error":"{}"'.format(
                        task, start_datetime, 'Export error for user ' + str(username) + ' - export list is empty')
                    amqp_send_message(ch, method, answer, routing_error_config, 1)
                    ch.basic_ack(delivery_tag=method.delivery_tag)
            else:
                if routing_return:
                    amqp_send_message(ch, method,
                                      #answer_string_scraping_complete([], 'followings', project_flow, username),
                                      amqp_request_f.answer_default(project_flow, username, [username], []),
                                      routing_return, option_priority)
                    ch.basic_ack(delivery_tag=method.delivery_tag)

            # Export to s3 and local store
            log_info("Storing in s3 is {}".format(option_store_s3),username)
            if option_store_s3:
                try:
                    path = custom_f.store_message_to_json_file(path_followings, filename, export)
                except Exception as err:
                    log.exception(err)
                    time.sleep(120)
                    raise
                status = s3.upload_file(path, s3_followings_bucket, filename)
                if status and not option_store_local_json:
                    os.remove(path)
                log_info("Storing at S3 file complete",username)

            time.sleep(config.PRIVATE_CHECK_SLEEP)
            return

    else:
        # Default message "task not found"
        answer = '{' + '"task":"private-error","status":false,"token":"{}","datetime":"{}","error":"{}"'.format(
            token, start_datetime, 'Task not found') + '}'
        amqp_send_message(ch, method, answer, routing_error_config, 1)
        return

    ch.basic_ack(delivery_tag=method.delivery_tag)
    return


def get_export_list(data, option_export_random, option_export_full, number_check):
    export = []

    # If we need to export random accounts
    if option_export_random:
        if len(data) > number_check:
            list_of_random = random.sample(data, number_check)  # Get random
            for item in data:
                if item in list_of_random:
                    item['c'] = 1
                export.append(item)
        else:  # get all follower to check
            for item in data:
                item['c'] = 1
                export.append(item)
        return export

    # If we need to export all accounts
    if option_export_full:
        for item in data:
            item['c'] = 1
            export.append(item)
        return export

    # If all option is false - just return data
    if len(export) == 0:
        export = data

    return export


def divide_chunks(l, n):
    # looping till length l
    for i in range(0, len(l), n):
        yield l[i:i + n]

def get_instagram_credentials_by_key(key):
    r = requests.post(config.CREDENTIAL_INSTAGRAM_ACCOUNTS_URL, headers=get_post_header_proxy(), json={'id': key})
    data = json.loads(r.content.decode('utf-8'))
    return data['credential']


def get_post_header_proxy():
    return {
        'Content-Type': 'application/json; charset=utf-8',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3',
        'accept-language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,cs;q=0.6,da;q=0.5,de;q=0.4,es;q=0.3,it;q=0.2,'
                           'pt;q=0.1,zh-CN;q=0.1,zh;q=0.1',
        'cache-control': 'max-age=0',
        'referer': 'https://www.google.com/',
        'X-AUTH-TOKEN': config.PROXY_ENDEMIC_AUTH_TOKEN_KEY
    }


if __name__ == '__main__':

    debug = False

    instagram_account_turn_on = True

    app = 'private'
    instance = 61
    amqp_instance = 1
    virtual_host = ''
    exchange = ''

    if 'instance' in os.environ:
        instance = int(os.environ['instance'])

    if 'amqp' in os.environ:
        amqp_instance = int(os.environ['amqp'])

    if 'virtual_host' in os.environ:
        virtual_host = str(os.environ['virtual_host'])

    if 'exchange' in os.environ:
        exchange = str(os.environ['exchange'])

    if 'debug' in os.environ:
        debug = bool(os.environ['debug'])

    if 'docker' in os.environ:
        fluent_host = config.LOG_FLUENT_HOST_DOCKER
    else:
        fluent_host = config.LOG_FLUENT_HOST

    if len(sys.argv) > 1:
        instance = int(sys.argv[1])

    if len(sys.argv) > 2:
        amqp_instance = int(sys.argv[2])

    if len(sys.argv) > 3:
        virtual_host = str(sys.argv[3])

    if len(sys.argv) > 4:
        exchange = str(sys.argv[4])

    if len(sys.argv) > 5:
        debug = bool(int(sys.argv[5]))

    if len(sys.argv) > 6:
        instagram_account_turn_on = bool(int(sys.argv[6]))

    custom_f = CustomFunction(instance, debug)

    # Set logger
    log = custom_f.set_logging(instance, None, 'fluent', app, 'application.assem.{}.{}'.format(app, virtual_host),
                               {'host': fluent_host, 'port': config.LOG_FLUENT_PORT},
                               'INFO' if not debug else 'DEBUG', ['s3transfer', 'pika'],
                               ['botocore', 'urllib3', 'requests.packages.urllib3.connectionpool'], virtual_host)

    amqp_request_f = AMQPRequests(log)

    # Set values from config
    routing_request_config = config.AMQP_QUEUE_PRIVATE
    routing_return_config = config.AMQP_QUEUE_APPLICATION
    routing_error_config = config.AMQP_QUEUE_ERROR

    # Get AQMP details
    amqp_creds = custom_f.get_amqp_by_key(amqp_instance)

    # Set RabbitMQ
    Amqp = AMQP(
        exchange,
        routing_request_config,
        virtual_host,
        amqp_creds['host'],
        amqp_creds['port'],
        amqp_creds['login'],
        amqp_creds['pass'],
        amqp_callback,
        log,
        debug
    )

    log_info('Instagram account is {}'.format(instagram_account_turn_on),None)

    # Get account credentials by key
    credentials = get_instagram_credentials_by_key(instance)

    # Star Insatgram class
    if credentials['ig_login']:

        instagram_login = credentials['ig_login']

        privateAPI = PrivateAPI(log, credentials['ig_login'], credentials['ig_pass'], credentials['proxy'], debug)

        if instagram_account_turn_on:
            try:
                privateAPI.connect()
            except Exception as err_exception:

                error_message = ast.literal_eval(str(err_exception))
                error_dict = {}
                error_dict['message'] = "Error when auth, instance {}, message {}".format(instance, error_message)
                error_dict['message_tag'] = error_message['message']
                error_dict['task'] = "credentials-error"
                error_dict['username'] = instagram_login
                error_dict['datetime'] = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
                Amqp.publish_message(json.dumps(error_dict), 'instagram', 1, 'instagram-exchange')

                log.error("Error when auth, instance {}, sleep for 12 hours, {}".format(
                        instance, json.dumps(ast.literal_eval(str(err_exception)))))

                time.sleep(12 * 3600)
                quit()
        else:
            try:
                privateAPI.foo_connect()
            except Exception as err_exception:
                log.error('{' + '"message":"Error when foo auth","details":{},"message_tag":"credentials-error"'.format(
                    json.dumps(ast.literal_eval(str(err_exception)))) + '}')

    else:
        log.error("This creads not founded {}".format(instance))
        time.sleep(3600)
        quit()

    # s3 connection
    s3 = S3('', config.AWS_S3_ACCESS_KEY_ID, config.AWS_S3_ACCESS_KEY_SECRET)
    s3.connect_client()
    s3_followers_bucket = config.AWS_S3_FOLLOWERS_BUCKET
    s3_followings_bucket = config.AWS_S3_FOLLOWINGS_BUCKET

    # Folder to local store
    path_followers = config.PRIVATE_FOLLOWERS_LOCAL_STORE_PATH
    path_followings = config.PRIVATE_FOLLOWINGS_LOCAL_STORE_PATH

    # Number of accounts in list to check
    number_check_limit = int(config.PRIVATE_CHECK_MAX_LIMIT)
    number_random_export_limit = int(config.PRIVATE_CHECK_RANDOM_LIMIT)

    # Sending limit
    amqp_send_limit = int(config.AMQP_SEND_ACCOUNTS_LIMIT)

    # Start Redis
    redis_temp = RedisClass(log, config.REDIS_TEMP_VALUES_HOST, config.REDIS_TEMP_VALUES_PORT,
                            config.REDIS_TEMP_VALUES_PASSWORD, 1)
    buckets_keys = config.REDIS_TEMP_VALUES_BUCKETS_KEYS

    # Start consuming
    try:
        # Amqp2.run_blocking()

        Amqp.run()
    except KeyboardInterrupt:
        Amqp.stop()
