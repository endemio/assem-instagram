import json
import logging
import config
import os
import requests
import time
import http.cookiejar
import statistics

from fluent import handler
from datetime import datetime, timedelta, timezone


class CustomFunction:

    test_url = 'https://www.instagram.com'
    redis_start_date = "01/03/2019"
    address_splitter = '!'

    # threads - number of pool threads
    def __init__(self, instance, debug=False):
        self.log = None
        self.token = None
        self.link = None
        self.task = None
        self.instance = instance
        self.debug = debug

    # Set token for logging
    def set_token(self, token):
        self.token = token

    # Set token for logging
    def get_token(self):
        return self.token

    def set_link(self, link):
        self.link = link

    def get_link(self):
        return self.link

    def set_task(self, task):
        self.task =task

    def get_task(self):
        return self.task



    # Set logging
    def set_logging(self, instance, log_path, logger, filename='log', log_name='custom', fluentd_config=None,
                    level='INFO', min_info=None, min_warning=None,virtual_host = None):

        custom_format = {
            'host': '%(hostname)s',
            'where': '%(module)s.%(funcName)s',
            'type': '%(levelname)s',
            'stack_trace': '%(exc_text)s',
            'created': '%(created)s',
            'stack_info': '%(stack_info)s',
            'virtual_host': virtual_host,
            'instance': str(filename) + '-' + str(instance)
        }

        # Logging settings
        log_file = log_path + '/' + filename + '-' + str(instance) + '.log' if log_path else None

        if level == 'INFO':
            level = logging.INFO

        if level == 'DEBUG':
            level = logging.DEBUG

        logging.basicConfig(level=level,
                            format='%(asctime)s %(name)-8s %(levelname)-8s %(message)s',
                            datefmt='%m-%d %H:%M',
                            filename=log_file,
                            filemode='a')

        # Console settiongs
        console = logging.StreamHandler()
        console.setLevel(level)
        formatter = logging.Formatter('%(name)-8s: %(levelname)-8s %(message)s')

        # tell the handler to use this format
        console.setFormatter(formatter)

        # add the handler to the root logger
        # logging.getLogger('').addHandler(console)

        if min_info:
            for i in min_info:
                logging.getLogger(i).setLevel(logging.INFO)

        if min_warning:
            for i in min_warning:
                logging.getLogger(i).setLevel(logging.WARNING)

        # Fluend settings if setted
        print('Set FLUENT logging. Server settings is {}, log_name is {}'.format(fluentd_config, log_name))
        if fluentd_config:
            h = handler.FluentHandler(log_name, host=fluentd_config['host'], port=fluentd_config['port'])
        else:
            h = handler.FluentHandler(log_name)
        formatter = handler.FluentRecordFormatter(custom_format)
        h.setFormatter(formatter)

        self.log = logging.getLogger(logger)
        self.log.addHandler(h)

        return self.log

    def check_file_exist(self, path):
        if not os.path.exists(path):
            return False
        return True

    def check_folder_exist(self, folder):
        if not os.path.exists(folder):
            os.makedirs(folder)
        return True

    def get_file_content(self, filepath):
        if os.path.isfile(filepath):
            with open(filepath, 'r') as the_file:
                data = the_file.read()
                result = data.split('\n')
                return result
        else:
            self.log.error('File {} is not exists!'.format(filepath))
            return []

    def get_mysql_by_key(self, key):
        list = self.get_file_content(config.SETTINGS_MYSQL_PATH)
        mysql = list[key - 1].split(':')
        return {'host': mysql[0], 'port': int(mysql[1]), 'login': mysql[2], 'pass': mysql[3], 'db': mysql[4]}

    def get_amqp_by_key(self, key):
        list = self.get_file_content(config.SETTINGS_AMQP_PATH)
        data = list[key - 1].split(':')
        return {'host': data[0], 'port': int(data[1]), 'login': data[2], 'pass': data[3]}

    def get_redis_by_key(self, key):
        list = self.get_file_content(config.SETTINGS_REDIS_PATH)
        data = list[key].split(':')
        return {'host': data[0], 'port': int(data[1]), 'pass': data[2]}

    def get_redis_slave_by_key(self, key):
        list = self.get_file_content(config.SETTINGS_REDIS_SLAVE_PATH)
        data = list[key - 1].split(':')
        return {'host': data[0], 'port': int(data[1]), 'pass': data[2], 'bucket_size': data[3]}

    def store_message_to_json_file(self, folder, filename, message):
        try:
            self.check_folder_exist(folder)
            with open(os.path.join(folder, filename), 'w', encoding='utf-8') as f:
                json.dump(message, f)
                return os.path.join(folder, filename)
        except Exception as e:
            self.log.error(e)
            raise

    def get_data_from_json_file(self, folder, filename):
        try:
            self.check_folder_exist(folder)
            with open(os.path.join(folder, filename), encoding='utf-8') as f:
                return json.load(f)
        except Exception as e:
            self.log.error(e)
            raise

    def get_instagram_credentials_by_key(self, key):
        r = requests.post(config.CREDENTIAL_INSTAGRAM_ACCOUNTS_URL, headers=self.get_post_header_proxy(),
                          json={'id': key})
        data = json.loads(r.content.decode('utf-8'))
        return data

    def get_post_header_proxy(selg):
        return {
            'Content-Type': 'application/json; charset=utf-8',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
            'accept-language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,cs;q=0.6,da;q=0.5,de;q=0.4,es;q=0.3,it;q=0.2,pt;q=0.1,zh-CN;q=0.1,zh;q=0.1',
            'cache-control': 'max-age=0',
            'referer': 'https://www.google.com/',
            'X-AUTH-TOKEN': config.PROXY_ENDEMIC_AUTH_TOKEN_KEY
        }

    def get_proxy(self, key=0):
        proxy_status = False

        self.log.debug('Proxy status is {}'.format(proxy_status))
        while not proxy_status:
            # Set proxy
            proxy_path = self.get_proxy_by_key(self.instance).rstrip() if not key else self.get_proxy_by_key(
                key).rstrip()
            self.log.debug('Proxy path is {}'.format(proxy_path))
            proxy_data = self.check_if_proxy_is_alive(proxy_path)
            self.log.debug('Proxy data is {}'.format(proxy_data))
            if not proxy_data['error']:
                self.log.debug('There is not errors during check')
                # proxy_status = True
                # proxy_path = proxy_data['proxy']
                return proxy_data['proxy']
            else:
                # Send data to say that this proxy is bad
                data = {'proxy': proxy_data['proxy'], 'error': proxy_data['error']}
                requests.post('https://proxy.endemic.ru/api/broke-proxy',
                              headers=self.get_post_header_proxy(),
                              json=data)
                return None
            """
            except requests.exceptions.ReadTimeout:
                self.log.debug('Oops. Read timeout occured')
                error+=1
                pass
    #                return {'proxy': proxy, 'error': 'ReadTimeout'}
            except requests.exceptions.ConnectTimeout:
                self.log.debug('Oops. Connection timeout occured!')
                error += 1
                pass
    #               return {'proxy': proxy, 'error': 'ConnectTimeout'}
            except requests.exceptions.RequestException as e:
                self.log.debug('Oops. Retry error timeout occured!')
                error += 1
                pass
            except requests.exceptions. as e:
                self.log.debug('Oops. Retry error timeout occured!')
                error += 1
                pass
    #                return {'proxy': proxy, 'error': 'RequestException'}
            """

    def check_if_proxy_is_alive(self, proxy):
        # print(str("Trying HTTP proxy %21s \t\t" % (proxy)))

        self.log.debug('Start checking is proxy alive or not - {}'.format(proxy))

        cj = http.cookiejar.CookieJar()

        error = 0
        working = False

        while error < 5 and not working:
            self.log.debug('Errors is {}'.format(error))
            try:
                t1 = time.time()
                r = requests.get(self.test_url, proxies={'http': proxy, 'https': proxy}, cookies=cj, timeout=(5, 20))
                t2 = time.time()
                working = True
            except requests.exceptions.ProxyError as e:
                self.log.debug(e)
                error += 1
                pass
            except requests.exceptions.ReadTimeout as e:
                self.log.debug(e)
                error += 1
                pass
            except requests.packages.urllib3.exceptions.MaxRetryError as e:
                self.log.debug(e)
                error += 1
                pass
            except Exception as e:
                self.log.exception(e)
                error += 1
                pass
            self.log.debug('Sleep for 3 seconds')
            time.sleep(3)

        if error > 4:
            return {'proxy': proxy, 'error': True}
        else:
            self.log.debug(" Response time: %d, length=%s" % (int((t2 - t1) * 1000), str(len(r.content))))
            return {'proxy': proxy, 'error': False}

    def store_results_to_file(self, folder, filename, data):
        try:
            with open(os.path.join(folder, filename), 'w', encoding='utf-8') as f:
                f.write(str(data) + "\n")
            return os.path.join(folder, filename)
        except Exception as e:
            self.log.error(e)
            raise

    def remove_local_file(self, folder, filename):
        return False

    def get_proxy_by_key(self, key):
        if key < 1000:
            r = requests.get(config.CREDENTIAL_INSTAGRAM_PROXY_URL)
            data = r.content.decode('utf-8')
            lines = data.split('\n')
            return lines[key - 1]
        else:
            # Get from proxy.endemic.ru
            r = requests.post('https://proxy.endemic.ru/api/new-proxy', headers=self.get_post_header_proxy(),
                              json={'instance': key})
            try:
                content = json.loads(r.content)
                return 'http://' + content['proxy']['proxy']
            except Exception as e:
                self.log.error(e)
                self.log.error(r.content)
                quit()

    def get_redis_bucket_and_index_by_id(self, id, bucket_keys):

        key = 0
        index = 0

        try:
            if bucket_keys > 0:
                key = int(id / bucket_keys)
                index = id - key * bucket_keys
        except Exception as e:
            self.log.exception(e)
            raise

        return [key, index]

    def chunkIt(self, seq, num):
        avg = len(seq) / float(num)
        out = []
        last = 0.0

        while last < len(seq):
            out.append(seq[int(last):int(last + avg)])
            last += avg

        return out

    def count_redis_index(self, user_id, bucket_size):
        user_id = int(user_id)
        bucket_index = int(user_id / int(bucket_size))
        index = user_id - bucket_index * bucket_size
        return [bucket_index, index]

    def count_user_id_from_index_and_key(self, key, index, bucket_size):
        return bucket_size * key + index

    def generate_string_for_redis(self, data):
        diff = datetime.today() - datetime.strptime(self.redis_start_date, "%d/%m/%Y")
        result = str(int(1)) + ':' + \
                 str(diff.days) + ':' + \
                 str(data['posts_count']) + ':' + \
                 str(data['follow']) + ':' + \
                 str(data['followers']) + ':' + \
                 str(int(data['business'])) + ':' + \
                 str(int(data['private'])) + ':' + \
                 str(int(data['get_followers']) if 'get_followers' in data else 0)

        if 'city' in data:
            if isinstance(data['city'], dict):
                result += ':' + str(
                    data['city']['title'] if 'title' in data['city'] else '') + self.address_splitter + str(
                    data['city']['percent'] if 'percent' in data['city'] else '')
            else:
                result += ':' + str(data['city'])
        else:
            result += ':'

        if 'country' in data:
            if isinstance(data['city'], dict):
                result += ':' + str(
                    data['country']['title'] if 'title' in data['country'] else '') + self.address_splitter + str(
                    data['country']['percent'] if 'percent' in data['country'] else '')
            else:
                result += ':' + str(data['country'])
        else:
            result += ':'

        if 'age' in data:
            result += ':' + str(int(data['age']))
        else:
            result += ':'

        if 'gender' in data:
            result += ':' + str(int(data['gender']))
        else:
            result += ':'

        return result

    def generate_string_for_redis_from_mysql(self, data):
        diff = datetime.today() - datetime.strptime(self.redis_start_date, "%d/%m/%Y")
        try:
            result = str(int(1)) + ':' + \
                 str(diff.days) + ':' + \
                 str(0 if data['posts'] is None else int(data['posts'])) + ':' + \
                 str(0 if data['follow'] is None else int(data['follow'])) + ':' + \
                 str(0 if data['followers'] is None else int(data['followers'])) + ':' + \
                 str(0 if data['business'] is None else int(data['business'])) + ':' + \
                 str(0 if data['private'] is None else int(data['private'])) + ':' + \
                 str(0)
        except Exception as err:
            self.log.error(data)
            self.log.exception(err)
            raise
        return result



    @staticmethod
    def generate_string_for_redis_location(data):

        country = str(data['address_dict']['country_code']) if 'country_code' in data['address_dict'] else ''
        city = str(data['address_dict']['city_name']) if 'city_name' in data['address_dict'] else ''
        zip_code = str(data['address_dict']['zip_code']) if 'zip_code' in data['address_dict'] else ''

        try:
            if not country:
                country = str(data['directory']['country']['id'])
        except:
            country = 'ZZ'

        try:
            if not city:
                city = str(data['directory']['city']['name'])
        except:
            city = ''

        try:
            return country + ':' + \
                   str(data['lat']) + ':' + \
                   str(data['lng']) + ':' + \
                   city + ':' + \
                   zip_code
        except Exception as e:
            logging.warning(data)
            raise Exception(e)

    # todo: find data check of stirng
    @staticmethod
    def generate_data_from_redis_location_string(location_string, token):
        elem = str(location_string).split(':')
        return {
            'lat': elem[1],
            'len': elem[2],
            'city': elem[3] if len(elem) > 3 else '',
            'country': elem[0],
            'token': token,
            'status': True
        }


    def update_redis_data_by_string(self,user_id,bucket_size,old,new):
        [key, index] = self.get_redis_bucket_and_index_by_id(user_id, bucket_size)

        old_values = self.generate_data_from_redis_string(old,key,index,bucket_size)
        new_values = self.generate_data_from_redis_string(new,key,index,bucket_size)

        if not new_values['city']['percent']:
            new_values.pop('city',None)

        if not new_values['country']['percent']:
            new_values.pop('country', None)

        result_values = old_values
        for item in new_values:
            result_values[item] = new_values[item] if new_values[item] else old_values[item]

        return result_values


    # todo: find data check of stirng
    def generate_data_from_redis_string(self, string, key=0, index=0, bucket_size=0):
        if string:
            elem = str(string).split(':')
        else:
            self.log.error('Received None')
            elem = [0]

        # Get city
        if len(elem) > 8:
            try:
                temp = str(elem[8]).replace('/0', self.address_splitter+str(0)).replace('/1', self.address_splitter+str(1)).split(self.address_splitter)
                if len(temp) > 1:
                    city = {'title': str(temp[0]), 'percent': float(temp[1] if len(temp) > 1 else 0)}
                elif elem[8] == '':
                    city = {'title': '', 'percent': 0}
                else:
                    city = {'title': '', 'percent': 1}
            except Exception as e:
                self.log.exception('error for {}'.format(elem[8]))
                raise
        else:
            city = {'title': '', 'percent': 0}

        # Get country
        if len(elem) > 9:
            temp = str(elem[9]).replace('/', self.address_splitter).split(self.address_splitter)
            if len(temp) > 1:
                country = {'title': str(temp[0]), 'percent': float(temp[1] if len(temp) > 1 else 0)}
            elif elem[9] == '':
                country = {'title': '', 'percent': 0}
            else:
                country = {'title': '', 'percent': 1}
        else:
            country = {'title': '', 'percent': 0}

        try:
            result = {
                'user_id': self.count_user_id_from_index_and_key(key, index, bucket_size),
                'status': True if int(elem[0]) > 0 else False,
                'check_day': datetime.utcnow(),
                'posts_count': int(elem[2]) if len(elem) > 2 else -1,
                'follow': int(elem[3]) if len(elem) > 3 else -1,
                'followers': int(elem[4]) if len(elem) > 4 else -1,
                'business': bool(int(elem[5])) if len(elem) > 5 else -1,
                'private': bool(int(elem[6])) if len(elem) > 6 else -1,
                'get_followers': bool(int(elem[7])) if len(elem) > 7 else -1,
                'city': city,
                'country': country,
                'age': int(elem[10]) if len(elem) > 10 and len(elem[10]) > 0 else -1,
                'gender': int(elem[11]) if len(elem) > 11 and len(elem[11]) > 0 else -1,
            }
            return result
        except Exception as e:
            self.log.info('{} - {}'.format(string,elem))
            self.log.exception(e)
            quit()

    def statistic_standard_deviation(self, data):
        return statistics.stdev(data)

    def check_account_type_by_values(self, data, user_id=0, bucket_size=0):

        [key, index] = self.get_redis_bucket_and_index_by_id(user_id, bucket_size)

        if isinstance(data, str):
            """
            explode = data.split(':')
            if len(explode) > 6:
                data = {
                    'posts': int(explode[2]),
                    'follow': int(explode[3]),
                    'followers': int(explode[4]),
                    'business': int(explode[5]),
                    'private': int(explode[6]),
                    'get_followers': int(explode[7]) if len(explode) > 7 else -1,
                    'country': json.dumps(explode[8]) if len(explode) > 8 else [],
                    'city': json.dumps(explode[9]) if len(explode) > 9 else [],
                    'age': json.dumps(explode[10]) if len(explode) > 10 else [],
                    'gender': json.dumps(explode[11]) if len(explode) > 11 else [],
                }
            else:
                return {'error': False, 'type': 'data-not-exists'}
            """
            data = self.generate_data_from_redis_string(data, key, index, bucket_size)

        else:
            self.log.error('Not string {}'.format(data))
            return {'error': False, 'type': 'input-not-string'}

        if not data['status']:
            return {
                'error': True,
                'input': {
                    'user_id': data['user_id'],
                    'posts': -1,
                    'follow': -1,
                    'followers': -1,
                    'private': 1,
                    'business': -1,
                    'country': 'ZZ',
                    'city': '',
                    'age': 0,
                    'gender': '',
                },
                'summary': {'real': 0, 'influ': 0, 'fake': 0, 'mass': 0, 'inact': 100},
                'details': {
                    'posts': -1,
                    'follow': -1,
                    'followers': -1,
                    'private': -1,
                    'business': -1}
            }

        """
                        #Followers	    #Follow 	    #Post
        Real people	    20<x<1000	    20<x<1000	    x > 12
        Influencers	    x > 1000        20<x<1000	    x > 24
        Fake	        x > 1000	    x > 1000        N.A.
        Inactive	    x < 20	        x < 1000	    N.A.
        Mass Followers	x < 1000	    x > 1000	    N.A.
        Suspicious	    Rest	        Rest	        Rest
         """

        try:
            posts = {
                'real': data['posts_count'] if data['posts_count'] else 1 / 100,
                'influ': data['posts_count'] / 12 if data['posts_count'] > 1 else 0,
                'fake': 10 / (data['posts_count'] + 1) if data['posts_count'] < 20 and data[
                    'posts_count'] > 0 else 1 / 5,
                'inact': 10 / (data['posts_count'] + 1) if data['posts_count'] < 10 and data[
                    'posts_count'] > 0 else 1 / 5,
                'mass': 1 if data['posts_count'] > 0 else 1
            }
        except Exception as err:
            self.log.exception(data)
            raise

        follow = {
            'real': 100 / (int(data['follow']) + 1) if data['follow'] < 1000 and data['follow'] > 0 else 0,
            'influ': 100 / (int(data['follow']) + 1) if data['follow'] < 1000 and data['follow'] > 0 else 0,
            'fake': data['follow'] / 100 if data['follow'] > 100 else 0,
            'inact': 100 / int(data['follow']) if data['follow'] > 0 else 0,
            'mass': data['follow'] / 100 if data['follow'] > 100 else 0,
        }

        followers = {
            'real': data['followers'] / 6 if data['followers'] > 1 and data['followers'] < 1000 else 0,
            'influ': data['followers'] / 100 if data['followers'] > 1000 else 0,
            'fake': 10 / int(data['followers']) if data['followers'] > 1 else 100,
            'inact': (100 / (data['followers'] + 1)) if data['followers'] > 0 else 100,
            'mass': data['followers'] / 1000 if data['followers'] > 1 else 0,
        }

        private = {
            'real': 1 / 3 if data['private'] else 10,
            'influ': 0 if data['private'] else 10,
            'fake': 5 if data['private'] else 10,
            'inact': 1 if data['private'] else 1,
            'mass': 1 if data['private'] else 1,
        }

        business = {
            'real': 1 / 10 if data['business'] else 1,
            'influ': 20 if data['business'] else 1,
            'fake': 0 if data['business'] else 1,
            'inact': 1 / 100 if data['business'] else 1,
            'mass': 1 if data['business'] else 1,
        }

        summary_by_types = {}
        for typename in posts:
            summary_by_types[typename] = posts[typename] * follow[typename] * followers[typename] * private[typename] * \
                                         business[
                                             typename]

        summary = sum(summary_by_types.values())

        for index in summary_by_types:
            if summary_by_types[index] > 0:
                summary_by_types[index] = round(summary_by_types[index] / summary * 100, 2)

        return {
            'error': False,
            'input': {
                'user_id': data['user_id'],
                'posts': data['posts_count'],
                'follow': data['follow'],
                'followers': data['followers'],
                'private': data['private'],
                'business': data['business'],
                'country': data['country'],
                'city': data['city'],
                'age': data['age'],
                'gender': data['gender'],
            },
            'summary': summary_by_types,
            'details': {
                'posts': posts,
                'follow': follow,
                'followers': followers,
                'private': private,
                'business': business}
        }
