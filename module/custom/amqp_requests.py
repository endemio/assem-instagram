from datetime import datetime


class AMQPRequests:

    def __init__(self, log):
        self.log = log
        self.link = None

    def set_link(self, link):
        self.link = link

    def answer_string_all_posts_scraping_complete(self, data, task_type, project_flow, token=None):
        return {
            'project_flow': project_flow,
            'task_type': task_type,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'token': token,
            'link': self.link,
            'answer': '{}'.format('answer_string_all_posts_scraping_complete'),
            'data': data,
            'completed': [token]
        }

    def answer_hmsets_data_success(self, data, usernames, project_flow, token=None):
        return {
            'project_flow': project_flow,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'token': token,
            'link': self.link,
            'status': True,
            'usernames': usernames,
            'data': data
        }

    def answer_accounts_data_success(self, data, project_flow, full_export, token=None):
        return {
            'project_flow': project_flow,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'token': token,
            'link': self.link,
            'status': True,
            'data': data if full_export else {}
        }

    def answer_account_data_success(self, data, project_flow, full_export, token=None):
        return {
            'project_flow': project_flow,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'token': token,
            'link': self.link,
            'status': True,
            'completed': data['account']['username'],
            'data': data if full_export else {}
        }

    def answer_user404(self, data, project_flow, token=None):
        return {
            'project_flow': project_flow,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'token': token,
            'link': self.link,
            'error404': data
        }

    def answer_account_info_success(self, data, error404, project_flow, full_export, token=None):
        return {
            'project_flow': project_flow,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'token': token,
            'link': self.link,
            'status': True,
            'completed': [data['edge']['username']],
            'error404': error404,
            'data': data if full_export else {}
        }

    def answer_private_list_account_success(self, data, error404, project_flow, full_export, token=None):
        return {
            'project_flow': project_flow,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'token': token,
            'link': self.link,
            'status': True,
            'completed': [item['username'] for item in data if 'username' in item],
            'error404': error404,
            'data': data if full_export else {}
        }

    def answer_location_list_success(self, data, error404, project_flow, full_export, token=None):
        return {
            'project_flow': project_flow,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'token': token,
            'link': self.link,
            'status': True,
            'completed': [item['token'] for item in data if 'token' in item],
            'error404': error404,
            'data': data if full_export else {}
        }

    def answer_string_all_comments_scraping_complete(self, data, error404, project_flow, full_export, token=None):
        return {
            'project_flow': project_flow,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'token': token,
            'link': self.link,
            'status': True,
            'completed': data if full_export else {}
        }

    def answer_string_statistic_complete(self, data, project_flow, usernames, token=None):
        return {
            'project_flow': project_flow,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'token': token,
            'data': data,
            'link': self.link,
            'status': True,
            'completed': usernames
        }

    def answer_string_location_list(self, data, project_flow, usernames, token=None):
        return {
            'project_flow': project_flow,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'token': token,
            'data': data,
            'link': self.link,
            'status': True,
            'completed': usernames,
        }

    def answer_string_check_location_from_info(self, data, project_flow, ids, token=None):
        return {
            'project_flow': project_flow,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'token': token,
            'link': self.link,
            'status': True,
            'completed': ids
        }

    def answer_string_check_location_from_bio(self, data, project_flow, text, token=None):
        return {
            'project_flow': project_flow,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'token': token,
            'link': self.link,
            'status': True,
            'completed': text
        }

    def answer_string_get_all_posts(self, data, project_flow, usernames, token=None):
        return {
            'project_flow': project_flow,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'token': token,
            'link': self.link,
            'completed': usernames
        }

    def answer_store_data_mysql(self, table, data, insert_keys, mysql_instance):
        step = {
            'task': 'add-data-to-db',
            'table': table,
            'link': self.link,
            'insert_keys': insert_keys,
            'mysql_instance': mysql_instance
        }

        return {
            'project_flow': [step],
            'data': data,
            'link': self.link,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        }

    def answer_store_data_mysql_username_exist(self, table, data, insert_keys, mysql_instance):
        step = {
            'task': 'add-data-to-db',
            'table': table,
            'link': self.link,
            'insert_keys': insert_keys,
            'mysql_instance': mysql_instance
        }

        return {
            'project_flow': [step],
            'data': data,
            'link': self.link,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
        }

    def answer_store_data_mysql_comments_stat(self, table, data, insert_keys, mysql_instance):
        step = {
            'task': 'add-data-to-db',
            'table': table,
            'link': self.link,
            'insert_keys': insert_keys,
            'mysql_instance': mysql_instance
        }

        return {
            'project_flow': [step],
            'data': data,
            'link': self.link,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
        }

    def answer_store_data_redis(self, data):
        step = {'task': 'add-data-to-redis'}
        return {
            'project_flow': [step],
            'data': data,
            'link': self.link,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
        }

    def answer_get_followers_from_account(self, username, project_flow):
        return {
            'project_flow': project_flow,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'link': self.link,
            'completed': username
        }

    def answer_default(self, project_flow, token=None, completed=None, data=None, ):
        return {
            'project_flow': project_flow,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'token': token,
            'link': self.link,
            'completed': completed if completed else [],
            'data': data if data else []
        }

    '''
    {'project_flow':[
            {'task': 'full-audit-check-redis-complete', 
             'project_id': 164, 
             'token': 'krimanpbrz', 
             'priority': 10, 
             'datetime': '2019-07-05 06:18:17', 
             'return_queue': ''}],
         'datetime':'2019-07-02 14:08:08',
         'token':'',
         'status':True,
         'completed':'[]',
         'error404':[],
         'data':{}}
    '''

    #
    #
    # ---------------------------  Requests --------------------------------------------------------------------
    #
    #
    '''
    
    def task_get_followers_from_account(username, limit, force_check, priority):
        return {
            'task': 'get-followers-list',
            'store_s3': True,
            'force_check': force_check,
            'owner': username,
            'file_token': '-full',
            'limit': limit,
            'priority': priority,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        }
    '''

    '''
    Request creating info
    1) Request will be used for project flow, so we need to include next fields:
        - return_queue - name of amqp queue where this task will be send after complete
        - datetime - current datetime when we create this task
        - token - unique uuid4 token for linking between different tasks
        - link - unique uuid4 token for linking with main token (project token)
        - username - account username, if needed. None if not needed
        - user_id - account user_id, if needed. None if not needed
    
    
    '''

    def request_get_account_data(self, return_queue, priority, mysql_instance=0, get_posts_data=False, token=None,
                                 username=None, user_id=None):
        return {
            'task': 'get-account-data',
            'use_proxy': True,
            'mysql_instance': mysql_instance,
            'store_json': False,
            'store_redis': True,
            'get_posts_data': get_posts_data,
            'priority': priority,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'token': token,
            'username': username,
            'user_id': user_id,
            'link': self.link,
            'return_queue': return_queue,
        }

    def request_get_all_posts(self, return_queue, priority, limit=0, mysql_instance=0, export_full=False,
                              comments_stat=False, forced_check=False, token=None, username=None, user_id=None):
        return {
            'task': 'get-all-posts-for-username',
            'use_proxy': True,
            'store_json': False,
            'get_posts_data': True,
            'force_check': forced_check,
            'mysql_instance': mysql_instance,
            'priority': priority,
            'store_s3': True,
            'comments_stat': comments_stat,
            'export_full': export_full,
            'limit': limit,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'token': token,
            'username': username,
            'user_id': user_id,
            'link': self.link,
            'return_queue': return_queue,
        }

    def request_get_followers(self, return_queue, priority, limit=100, mysql_instance=4, token=None, username=None,
                              user_id=None):
        return {
            'task': 'get-followers-list',
            'use_proxy': True,
            'mysql_instance': mysql_instance,
            'store_json': False,
            'force_check': True,
            'priority': priority,
            'store_s3': True,
            'limit': limit,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'token': token,
            'username': username,
            'user_id': user_id,
            'link': self.link,
            'return_queue': return_queue,
        }

    def request_check_location_by_bio(self, mysql_instance, priority, return_queue, token=None, username=None,
                                      user_id=None):
        return {
            'task': 'get-location-from-bio',
            'mysql_instance': mysql_instance,
            'priority': priority,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'token': token,
            'username': username,
            'user_id': user_id,
            'link': self.link,
            'return_queue': return_queue,
        }

    def request_check_location_by_location_ids(self, mysql_instance, priority, return_queue, token=None, username=None,
                                               user_id=None):
        return {
            'task': 'get-location-by-location-ids',
            'mysql_instance': mysql_instance,
            'priority': priority,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'token': token,
            'username': username,
            'user_id': user_id,
            'link': self.link,
            'return_queue': return_queue,
        }

    def request_check_account_location_by_info(self, return_queue = '', mysql_instance=0, priority=1, token=None, username=None,
                                       user_id=None,export=False):
        return {
            'task': 'get-account-info',
            'use_proxy': True,
            'mysql_instance': mysql_instance,
            'priority': priority,
            'export': export,
            'export_mysql_table': 'account_location',
            'export_mysql_keys': {'username': 'username', 'location': 'location_info'},
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'token': token,
            'username': username,
            'user_id': user_id,
            'link': self.link,
            'return_queue': return_queue,
        }

    def request_get_all_location_from_posts(self, return_queue, priority, mysql_instance=0, project_id=-1,
                                            username=None, owner='', limit=500, store_redis=False, user_id=0,
                                            token=None):
        return {
            'task': 'get-location-data-from-posts',
            'project_id': project_id,
            'mysql_instance': mysql_instance,
            'priority': priority,
            'store_redis': store_redis,
            'limit': limit,
            'owner': owner,
            'token': token,
            'username': username,
            'user_id': user_id,
            'link': self.link,
            'return_queue': return_queue,
        }

    def request_audit_choose_accounts(self, return_queue, priority, mysql_instance, project_id, username, owner,
                                      limit=100, store_s3=True, store_json=False, token=None, user_id=None):
        return {
            'task': 'full-audit-choose-accounts',
            'project_id': project_id,
            'mysql_instance': mysql_instance,
            'priority': priority,
            'limit': limit,
            'store_s3': store_s3,
            'store_json': store_json,
            'owner': owner,
            'token': token,
            'username': username,
            'user_id': user_id,
            'link': self.link,
            'return_queue': return_queue,
        }

    def request_get_data_for_redis(self, return_queue, priority, use_proxy=True, store_json=False, force_check=False,
                                   store_redis=True, export_full=True, token=None, username=None, user_id=None):
        return {
            'task': 'get-private-list-accounts-info',
            'use_proxy': use_proxy,
            'mysql_instance': False,
            'store_json': store_json,
            'force_check': force_check,
            'store_redis': store_redis,
            'export_full': export_full,
            'priority': priority,
            'token': token,
            'username': username,
            'user_id': user_id,
            'link': self.link,
            'return_queue': return_queue
        }

    def request_comments_owner_audit(self, return_queue, priority, project_id=-1, token=None, export_comments=True,
                                     store_s3=True, store_mysql=True, username=None, user_id=None):
        return {
            'task': 'full-audit-comments',
            'project_id': project_id,
            'priority': priority,
            'store_s3': store_s3,
            'store_mysql': store_mysql,
            'export_comments': export_comments,
            'token': token,
            'username': username,
            'user_id': user_id,
            'link': self.link,
            'return_queue': return_queue,
        }

    def request_full_audit_check_complete(self, return_queue, priority, token=None, project_id=-1, username=None,
                                          user_id=None):
        return {
            'task': 'full-audit-check-redis-complete',
            'project_id': project_id,
            'priority': priority,
            'token': token,
            'username': username,
            'user_id': user_id,
            'link': self.link,
            'return_queue': return_queue
        }

    def request_get_location_info(self, return_queue, priority, token=None, username=None, user_id=None):
        return {
            'task': 'get-location-info',
            'priority': priority,
            'store_redis': True,
            'token': token,
            'username': username,
            'user_id': user_id,
            'link': self.link,
            'return_queue': return_queue,
        }

    def request_store_account_to_project_owner_list(self, return_queue, priority, project_id=-1, owner='',
                                                    username=None, token=None, user_id=None):
        return {
            'task': 'add-account-to-project-owner-result-list',
            'project_id': project_id,
            'priority': priority,
            'owner': owner,
            'account': username,
            'token': token,
            'username': username,
            'user_id': user_id,
            'link': self.link,
            'return_queue': return_queue,
        }

    def request_posts_with_comments(self, return_queue, priority, export_posts=True, token=None, username=None,
                                    user_id=None):
        return {
            'task': 'get-posts-with-comments',
            'export_posts': export_posts,
            'priority': priority,
            'store_s3': False,
            'token': token,
            'username': username,
            'user_id': user_id,
            'link': self.link,
            'return_queue': return_queue,
        }

    def request_get_comments_from_posts(self, return_queue, priority, limit=0, export_comments=True, store_s3=True,
                                        token=None, username=None, user_id=None):
        return {
            'task': 'get-all-comments-by-post',
            'update_limit': limit,
            'priority': priority,
            'store_s3': store_s3,
            'export_comments': export_comments,
            'token': token,
            'username': username,
            'user_id': user_id,
            'link': self.link,
            'return_queue': return_queue,
        }

    def request_get_engagement_rate(self, return_queue, priority, limit=12, store_redis=False, token=None,
                                    username=None, user_id=None):
        return {
            'task': 'get-account-engagement-rate',
            'priority': priority,
            'limit_er': limit,
            'store_redis': store_redis,
            'token': token,
            'username': username,
            'user_id': user_id,
            'link': self.link,
            'return_queue': return_queue
        }

    def request_extract_followers_from_list(self, return_queue, priority, limit=500, union=1, token=None, username=None,
                                            user_id=None):
        return {
            'task': 'extract-accounts-from-followers-list',
            'priority': priority,
            'limit': limit,
            'union_return': union,
            'token': token,
            'username': username,
            'user_id': user_id,
            'link': self.link,
            'return_queue': return_queue
        }

    def request_uncrease_counter_in_redis(self, return_queue, priority, value_token=None, token=None, username=None,
                                          user_id=None):
        return {
            'task': 'append-variable-to-redis-counter',
            'priority': priority,
            'value_token': value_token,
            'token': token,
            'username': username,
            'user_id': user_id,
            'link': self.link,
            'return_queue': return_queue
        }

    def request_check_followers_data_in_redis(self, return_queue, priority, check_non_exists=False, uuid='', token=None,
                                              username=None, user_id=None):
        return {
            'task': 'check-full-data-for-account-exists-in-redis',
            'priority': priority,
            'uuid': uuid,
            'check_non_exist': check_non_exists,
            'token': token,
            'username': username,
            'user_id': user_id,
            'link': self.link,
            'return_queue': return_queue
        }

    def request_face_analysis_for_age_and_gender(self, return_queue, priority, mysql_instance=0, store_redis=False,
                                                 user_id=0, token=None, username=None):
        return {
            'task': 'face-analysis',
            'priority': priority,
            'mysql_instance': mysql_instance,
            'store_redis': store_redis,
            'token': token,
            'username': username,
            'user_id': user_id,
            'link': self.link,
            'return_queue': return_queue
        }

    def request_append_account_data_to_redis(self, return_queue, priority, db='account', user_id=None, token=None,
                                             username=None):
        return {
            'task': 'append-data-to-current',
            'priority': priority,
            'db': db,
            'token': token,
            'username': username,
            'user_id': user_id,
            'link': self.link,
            'return_queue': return_queue
        }

    def request_return_ready_influencer_data(self, return_queue, priority, username=None, token=None,
                                             user_id=None):
        return {
            'task': 'return-ready-influencer-data',
            'priority': priority,
            'token': token,
            'username': username,
            'user_id': user_id,
            'link': self.link,
            'return_queue': return_queue
        }

    def request_return_task_not_found(self, instance_type, instance, token, message, username=None, user_id=None):
        return {
            'task': 'task-not-found',
            'instance': '{}-{}'.format(instance_type, instance),
            'token': token,
            'username': username,
            'user_id': user_id,
            'link': self.link,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'message': message
        }
