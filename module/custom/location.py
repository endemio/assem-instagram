import pycountry
import operator

from fluent import handler
from datetime import datetime, timedelta, timezone


class CustomLocationFunction:

    # threads - number of pool threads
    def __init__(self,log):
        self.log = log
        self.token = None

    def get_country_iso_by_name(self,title):
        if title:
            try:
                country_iso = pycountry.countries.get(name=title).alpha_2
                return country_iso
            except Exception as err:
                return 'ZZ'
        else:
            return 'ZZ'


    def analyze_account_locations_to_result_post(self, result_post):

        if not len(result_post):
            return {}

        self.log.debug('{}'.format(result_post))

        countries = {}
        cities = {}

        for location in result_post:
            if location:
                country_iso = pycountry.countries.get(alpha_2=location['country'])
                if country_iso:
                    full_name = country_iso.name
                    countries[full_name] = countries[full_name]+1 if full_name in countries else 1
                else:
                    countries['Not found'] = countries['Not found'] + 1 if 'Not found' in countries else 1

                if 'city' in location:
                    cities[location['city']] = cities[location['city']]+1 if location['city'] in cities else 1

            else:
                countries['Not found'] = countries['Not found'] + 1 if 'Not found' in countries else 1

        percent_country = {}
        for full_name in countries:
            if sum(countries.values()):
                percent_country[full_name] = "%.2f" % (countries[full_name]/sum(countries.values()))

        percent_city = {}
        for full_name in cities:
            if sum(countries.values()):
                percent_city[full_name] = "%.2f" % (cities[full_name]/sum(cities.values()))

        main_country = max(percent_country.items(), key=operator.itemgetter(1))[0]
        main_city = max(percent_city.items(), key=operator.itemgetter(1))[0]

        return {
            'main_country': main_country,
            'main_country_percent': percent_country[main_country],
            'main_city': main_city,
            'main_city_percent': percent_city[main_city],
            'all_country_string':', '.join(percent_country.keys()),
            'all_country_array': percent_country,
            'all_city_array': percent_city
        }