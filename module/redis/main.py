import redis

class RedisClass:

    def __init__(self, logging, host, port, password=None, debug=False, db=0):
        self.logging = logging
        self.host = host
        self.port = port
        self.db_id = db
        self.debug = debug
        self.password = password
        self.make_connection_redis()

    def make_connection_redis(self):
        try:
            self.connection = redis.StrictRedis(host=self.host, port=self.port,password=self.password,decode_responses=True,db=self.db_id)
        except Exception as e:
            print('Error:', e)
            exit('Failed to connect, terminating.')

    def get_redis_clent(self):
        return self.connection

    def insert_data_from_dict(self, data):
        self.logging.debug('{'+'"message":"Start add to redis, count=","message_tag":""'+str(len(data))+'}')
        p = self.connection.pipeline()
        for name, mapping in data.items():
            p.hmset(name, mapping)
            self.logging.debug('Add new item to key '+str(name)+' mapping '+str(mapping))
        p.execute()
        return True

    def insert_data_key_value(self, key ,value,ttl=0):
        self.logging.debug('Start add to redis, key= %s, value=%s',key,value)
        p = self.connection.pipeline()
        if not ttl:
            p.set(key, value)
        else:
            p.set(key, value,ex=ttl)
        p.execute()
        return True

    def insert_dict_key_value(self, data, ttl=0):
        self.logging.debug('Start add to redis {}'.format(data))
        p = self.connection.pipeline()

        for item in data:
            if not ttl:
                p.set(item, data[item])
            else:
                p.set(item, data[item],ex=ttl)
        p.execute()
        return True

    def get_all_keys(self):
        return self.connection.keys()

    def get_key_value(self, key):
        return self.connection.hgetall(key)

    def get_value_by_key(self, key):
        return self.connection.get(key)

    def get_value_by_int(self, id, bucket_keys):
        try:
            key = int(id/bucket_keys)
            index = id - key*bucket_keys
            self.logging.debug('Id='+str(id)+' key='+str(key)+' index='+str(index))
        except Exception as e:
            self.logging.error(e)
            return None
        try:
            value = self.connection.hget(key, index)
            return value
        except Exception as e:
            self.logging.error('Error at "get_value_by_int" '+str(key)+' '+str(index))
            self.logging.error(e)
            raise

    def get_used_memory(self):
        info = self.connection.info()['used_memory']
        return info

    def get_dbsize(self):
        return self.connection.dbsize()

    def remove_all_data_use_very_carefully(self):
        self.connection.flushall()
