import pika
import time
import json


class AMQP:

    def __init__(self, exchange, request_queue, virtual_host, host, port , user, password, callback, logging, debug,priority = 100):
        self.exchange = exchange
        self.request = request_queue
        self.virtual_host = virtual_host
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.callback = callback
        self.logging = logging
        self.debug = debug
        self._connection = None
        self._channel = None
        self._closing = False
        self._consumer_tag = None
        self.priority = priority
        self._url = 'amqp://'+str(user)+':'+str(password)+'@'+str(host)+':'+str(port)+'/'+str(virtual_host)

    def get_exchange(self):
        return self.exchange

    def get_queue_request(self):
        return self.request

    def basic_properties(self,delivery_mode,priority):
        return pika.BasicProperties(delivery_mode=delivery_mode, priority=priority)

    def run(self):
        """Run the example consumer by connecting to RabbitMQ and then
        starting the IOLoop to block and allow the SelectConnection to operate.

        """
        self._connection = self.connect()
        self._connection.ioloop.start()

    def run_blocking(self):
        """Run the example consumer by connecting to RabbitMQ and then
        starting the IOLoop to block and allow the SelectConnection to operate.

        """
        self._connection = self.connect_blocking()
        self._channel = self._connection.channel()


    def connect(self):
        """This method connects to RabbitMQ, returning the connection handle.
        When the connection is established, the on_connection_open method
        will be invoked by pika.

        :rtype: pika.SelectConnection

        """
        self.logging.debug('{'+'"message":"Connecting to {}",'.format(self._url)+'"message_tag":"amqp-log"'+'}')

        credentials = pika.PlainCredentials(
            username=self.user,
            password=self.password
        )

        parameters = pika.ConnectionParameters(
            host=self.host,
            port=self.port,
            virtual_host=self.virtual_host,
            credentials=credentials
        )

        return pika.SelectConnection(parameters, self.on_connection_open)


    def connect_blocking(self):
        """This method connects to RabbitMQ, returning the connection handle.
        When the connection is established, the on_connection_open method
        will be invoked by pika.

        :rtype: pika.SelectConnection

        """
        self.logging.debug('{'+'"message":"Connecting to {}","message_tag":"amqp-log"'.format(self._url)+'}')

        credentials = pika.PlainCredentials(
            username=self.user,
            password=self.password
        )

        parameters = pika.ConnectionParameters(
            host=self.host,
            port=self.port,
            virtual_host=self.virtual_host,
            credentials=credentials
        )

        return pika.BlockingConnection(parameters)


    def on_connection_open(self, unused_connection):
        """This method is called by pika once the connection to RabbitMQ has
        been established. It passes the handle to the connection object in
        case we need it, but in this case, we'll just mark it unused.

        :type unused_connection: pika.SelectConnection

        """
        self.logging.debug('{'+'"message":"Connection opened","message_tag":"amqp-log"'+'}')
        self.add_on_connection_close_callback()
        self.open_channel()

    def add_on_connection_close_callback(self):
        """This method adds an on close callback that will be invoked by pika
        when RabbitMQ closes the connection to the publisher unexpectedly.

        """
        self.logging.debug('{'+'"message":"Adding connection close callback","message_tag":"amqp-log"'+'}')
        self._connection.add_on_close_callback(self.on_connection_closed)

    def on_connection_closed(self, connection, reason, temp):
        """This method is invoked by pika when the connection to RabbitMQ is
        closed unexpectedly. Since it is unexpected, we will reconnect to
        RabbitMQ if it disconnects.

        :param pika.connection.Connection connection: The closed connection obj
        :param Exception reason: exception representing reason for loss of
            connection.

        """
        self._channel = None
        if self._closing:
            self._connection.ioloop.stop()
        else:
            self.logging.warning('{'+'"message":"Connection closed, reopening in 5 seconds: {}","message_tag":"amqp-log"'.format(reason)+'}')
            self._connection.add_timeout(5, self._connection.ioloop.stop)
            #self.reconnect()

    def reconnect(self):
        """Will be invoked by the IOLoop timer if the connection is
        closed. See the on_connection_closed method.

        """
        # This is the old connection IOLoop instance, stop its ioloop
        self._connection.ioloop.stop()

        if not self._closing:

            # Create a new connection
            self._connection = self.connect()

            # There is now a new connection, needs a new ioloop to run
            self._connection.ioloop.start()

    def open_channel(self):
        """Open a new channel with RabbitMQ by issuing the Channel.Open RPC
        command. When RabbitMQ responds that the channel is open, the
        on_channel_open callback will be invoked by pika.

        """
        self.logging.debug('{'+'"message":"Creating a new channel","message_tag":"amqp-log"'+'}')
        self._connection.channel(on_open_callback=self.on_channel_open)

    def on_channel_open(self, channel):
        """This method is invoked by pika when the channel has been opened.
        The channel object is passed in so we can make use of it.

        Since the channel is now open, we'll declare the exchange to use.

        :param pika.channel.Channel channel: The channel object

        """
        self.logging.debug('{'+'"message":"Channel opened","message_tag":"amqp-log"'+'}')
        self._channel = channel
        self.add_on_channel_close_callback()
        self.setup_exchange(self.exchange)

    def add_on_channel_close_callback(self):
        """This method tells pika to call the on_channel_closed method if
        RabbitMQ unexpectedly closes the channel.

        """
        self.logging.debug('{'+'"message":"Adding channel close callback","message_tag":"amqp-log"'+'}')
        self._channel.add_on_close_callback(self.on_channel_closed)

    def on_channel_closed(self, channel, reason, temp):
        """Invoked by pika when RabbitMQ unexpectedly closes the channel.
        Channels are usually closed if you attempt to do something that
        violates the protocol, such as re-declare an exchange or queue with
        different parameters. In this case, we'll close the connection
        to shutdown the object.

        :param pika.channel.Channel: The closed channel
        :param Exception reason: why the channel was closed

        """
        self.logging.debug('{'+'"message":"Channel {} was closed: {}, {}",'.format(channel, reason, temp)+'"message_tag":"amqp-log"'+'}')
        self._connection.close()


    def setup_exchange(self, exchange_name):
        """Setup the exchange on RabbitMQ by invoking the Exchange.Declare RPC
        command. When it is complete, the on_exchange_declareok method will
        be invoked by pika.

        :param str|unicode exchange_name: The name of the exchange to declare

        """
        self.logging.debug('{'+'"message":"Declaring exchange {}",'.format(exchange_name)+'"message_tag":"amqp-log"'+'}')
        self._channel.exchange_declare(callback=self.on_exchange_declareok,
                                       exchange=exchange_name,
                                       durable=True,
                                       exchange_type='direct')

    def on_exchange_declareok(self, unused_frame):
        """Invoked by pika when RabbitMQ has finished the Exchange.Declare RPC
        command.

        :param pika.Frame.Method unused_frame: Exchange.DeclareOk response frame

        """
        self.logging.debug('{'+'"message":"Exchange declared","message_tag":"amqp-log"'+'}')
        self.setup_queue(self.request)

    def setup_queue(self, queue_name):
        """Setup the queue on RabbitMQ by invoking the Queue.Declare RPC
        command. When it is complete, the on_queue_declareok method will
        be invoked by pika.

        :param str|unicode queue_name: The name of the queue to declare.

        """
        self.logging.debug('{'+'"message":"Declaring queue {}",'.format(queue_name)+'"message_tag":"amqp-log"'+'}')
        try:
            self._channel.queue_declare(self.on_queue_declareok,queue=queue_name,passive=False,durable=True,arguments={'x-max-priority':int(self.priority)})
        except Exception as err:
            self.logging.exception(err)
            time.sleep(5)

        #self.start_consuming()


    def on_queue_declareok(self, method_frame):
        """Method invoked by pika when the Queue.Declare RPC call made in
        setup_queue has completed. In this method we will bind the queue
        and exchange together with the routing key by issuing the Queue.Bind
        RPC command. When this command is complete, the on_bindok method will
        be invoked by pika.

        :param pika.frame.Method method_frame: The Queue.DeclareOk frame

        """
        self.logging.debug('{'+'"message":"Binding {} to {} with {}",'.format(self.exchange, self.request, self.request)+'"message_tag":"amqp-log"'+'}')
        self._channel.queue_bind(self.on_bindok, self.request,
                                 self.exchange, self.request)


    def on_bindok(self, unused_frame):
        """Invoked by pika when the Queue.Bind method has completed. At this
        point we will start consuming messages by calling start_consuming
        which will invoke the needed RPC commands to start the process.

        :param pika.frame.Method unused_frame: The Queue.BindOk response frame

        """
        self.logging.debug('{'+'"message":"Queue bound","message_tag":"amqp-log"'+'}')
        self.start_consuming()

    def start_consuming(self):
        """This method sets up the consumer by first calling
        add_on_cancel_callback so that the object is notified if RabbitMQ
        cancels the consumer. It then issues the Basic.Consume RPC command
        which returns the consumer tag that is used to uniquely identify the
        consumer with RabbitMQ. We keep the value to use it when we want to
        cancel consuming. The on_message method is passed in as a callback pika
        will invoke when a message is fully received.

        """
        self.logging.debug('{'+'"message":"Issuing consumer related RPC commands","message_tag":"amqp-log"'+'}')
        self.add_on_cancel_callback()
        #self._consumer_tag = self._channel.basic_consume(
        #    consumer_callback=self.callback,
        #    no_ack=False,
        #    queue=self.request)

        self._channel.basic_qos(prefetch_count=1)
        #self._channel.confirm_delivery()
        self.logging.debug('{'+'"message":"Set prefetch to 1","message_tag":"amqp-log"'+'}')
        self._consumer_tag = self._channel.basic_consume(queue=self.request,
                                                         consumer_callback=self.callback)

    def add_on_cancel_callback(self):
        """Add a callback that will be invoked if RabbitMQ cancels the consumer
        for some reason. If RabbitMQ does cancel the consumer,
        on_consumer_cancelled will be invoked by pika.

        """
        self.logging.debug('{'+'"message":"Adding consumer cancellation callback","message_tag":"amqp-log"'+'}')
        self._channel.add_on_cancel_callback(self.on_consumer_cancelled)

    def on_consumer_cancelled(self, method_frame):
        """Invoked by pika when RabbitMQ sends a Basic.Cancel for a consumer
        receiving messages.

        :param pika.frame.Method method_frame: The Basic.Cancel frame

        """
        self.logging.debug('{'+'"message":"Consumer was cancelled remotely, shutting down: {}",'.format(method_frame)+'"message_tag":"amqp-log"'+'}')
        if self._channel:
            self._channel.close()


    def publish_message(self, json_message, routing_key, priority=1, exchange = '', hdrs={}):
        """If the class is not stopping, publish a message to RabbitMQ,
        appending a list of deliveries with the message number that was sent.
        This list will be used to check for delivery confirmations in the
        on_delivery_confirmations method.

        Once the message has been sent, schedule another message to be sent.
        The main reason I put scheduling in was just so you can get a good idea
        of how the process is flowing by slowing down and speeding up the
        delivery intervals by changing the PUBLISH_INTERVAL constant in the
        class.

        """
        self.logging.debug('Start')
        while self._channel is None or not self._channel.is_open:
            self.logging.error('{'+'"message":"_channel is {}",'.format(self._channel)+'"message_tag":"amqp-log"'+'}')
            self.run_blocking()

        self.logging.debug('Prop')

        properties = pika.BasicProperties(delivery_mode=2,
                                          priority=priority,
                                          content_type='application/json',
                                          headers=hdrs)

        self.logging.debug('Sending...')

        send = False

        while not send:

            try:
                self._channel.basic_publish(
                    exchange=self.exchange if not exchange else exchange,
                    routing_key=routing_key,
                    body=json_message,
                    properties=properties)

                self.logging.debug('{'+'"message":"Message {} sended to queue with routing_key {}'.format(json_message,routing_key)+'","message_tag":"amqp-log"'+'}')

                return True
            except pika.connection.exceptions.ConnectionClosed as exc:
                self.logging.warning('Error. Connection closed, and the message was never delivered. Sleeping 5 sec before resend. Message = {}'.format(json_message))
                try:
                    self.run_blocking()
                except Exception as err:
                    self.logging.exception('Error during reconnect {}'.format(err))
                    raise
                self.logging.warning('Sleeping for 5 sec')
                time.sleep(5)
                pass




    '''
    def on_message(self, unused_channel, basic_deliver, properties, body):
        """Invoked by pika when a message is delivered from RabbitMQ. The
        channel is passed for your convenience. The basic_deliver object that
        is passed in carries the exchange, routing key, delivery tag and
        a redelivered flag for the message. The properties passed in is an
        instance of BasicProperties with the message properties and the body
        is the message that was sent.

        :param pika.channel.Channel unused_channel: The channel object
        :param pika.Spec.Basic.Deliver: basic_deliver method
        :param pika.Spec.BasicProperties: properties
        :param str|unicode body: The message body

        """
        self.logging.debug('Received message # %s from %s: %s',
                    basic_deliver.delivery_tag, properties.app_id, body)
        time.sleep(1)
        self.acknowledge_message(basic_deliver.delivery_tag)
    '''

    def acknowledge_message(self, delivery_tag):
        """Acknowledge the message delivery from RabbitMQ by sending a
        Basic.Ack RPC method for the delivery tag.

        :param int delivery_tag: The delivery tag from the Basic.Deliver frame

        """
        self.logging.debug('{'+'"message":"Acknowledging message {}",'.format(delivery_tag)+'"message_tag":"amqp-log"'+'}')
        self._channel.basic_ack(delivery_tag)

    def stop_consuming(self):
        """Tell RabbitMQ that you would like to stop consuming by sending the
        Basic.Cancel RPC command.

        """
        if self._channel:
            self.logging.debug('{'+'"message":"Sending a Basic.Cancel RPC command to RabbitMQ","message_tag":"amqp-log"'+'}')
            self._channel.basic_cancel(self.on_cancelok, self._consumer_tag)

    def on_cancelok(self, unused_frame):
        """This method is invoked by pika when RabbitMQ acknowledges the
        cancellation of a consumer. At this point we will close the channel.
        This will invoke the on_channel_closed method once the channel has been
        closed, which will in-turn close the connection.

        :param pika.frame.Method unused_frame: The Basic.CancelOk frame

        """
        self.logging.debug('{'+'"message":"RabbitMQ acknowledged the cancellation of the consumer","message_tag":"amqp-log"'+'}')
        self.close_channel()

    def close_channel(self):
        """Call to close the channel with RabbitMQ cleanly by issuing the
        Channel.Close RPC command.

        """
        self.logging.debug('{'+'"message":"Closing the channel","message_tag":"amqp-log"'+'}')
        self._channel.close()

    def stop(self):
        """Cleanly shutdown the connection to RabbitMQ by stopping the consumer
        with RabbitMQ. When RabbitMQ confirms the cancellation, on_cancelok
        will be invoked by pika, which will then closing the channel and
        connection. The IOLoop is started again because this method is invoked
        when CTRL-C is pressed raising a KeyboardInterrupt exception. This
        exception stops the IOLoop which needs to be running for pika to
        communicate with RabbitMQ. All of the commands issued prior to starting
        the IOLoop will be buffered but not processed.

        """
        #self.logging.debug('Begin Stopping')
        self._closing = True
        self.stop_consuming()
        self._connection.ioloop.start()
        self.logging.debug('{'+'"message":"Stopped","message_tag":"amqp-log"'+'}')

    def close_connection(self):
        """This method closes the connection to RabbitMQ."""
        self.logging.debug('{'+'"message":"Closing connection","message_tag":"amqp-log"'+'}')
        self._connection.close()
