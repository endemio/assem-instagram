import json
import os
import requests
import time
import random
from datetime import datetime, timezone
import hashlib
import re

from fake_useragent import UserAgent
from time import time as timer
from multiprocessing.dummy import Pool as ThreadPool


class InstagramParsing:
    InstagramAccountURL = 'https://www.instagram.com/'
    InstagramPostURL = 'https://www.instagram.com/p/'
    InstagramLocationURL = 'https://www.instagram.com/explore/locations/'
    InstagramAccountInfoURL = 'https://i.instagram.com/api/v1/users/'

    # threads - number of pool threads
    def __init__(self, threads, log, instance, debug=False):
        self.threads = threads
        self.instance = instance
        self.debug = debug
        self.proxy_error = 0
        self.proxy_status = False
        self.ftp_status = False
        self.mysql_status = False
        self.aws_s3_status = False
        self.ua_local_status = False
        self.local_store_status = False
        self.log = log
        self.mysql_data_keys = []
        self.ua_dict = None
        self.token = None
        self.mysql_connection = None
        self.export_account = False
        self.export_posts = False
        self.count_engagements = False
        self.private_cookies = None
        self.private_user_agent = None
        # self.log.debug('InstagramParsing')
        # self.log.info('InstagramParsing')
        # self.log.warning('InstagramParsing')
        # self.log.exception('InstagramParsing')
        # self.log.error('InstagramParsing')

    # ------------------------- Start Config --------------------------------

    def config_set_proxy(self, status, proxy=''):
        self.proxy = proxy
        self.proxy_error = 0
        self.proxy_status = status

    def config_set_export_dict(self, account=False, posts=False, count_engagements=False):
        self.export_account = account
        self.export_posts = posts
        self.count_engagements = count_engagements

    def config_set_ftp(self, status, host='', port='', user='', password='', folder=''):
        self.ftp_host = host
        self.ftp_port = port
        self.ftp_user = user
        self.ftp_password = password
        self.ftp_folder = folder
        self.ftp_export_status = status

    def config_set_aws_s3(self, status, id='', key='', bucket=''):
        self.aws_s3_id = id
        self.aws_s3_key = key
        self.aws_s3_bucket = bucket
        self.aws_s3_export_status = status

    def config_set_local_storing(self, status, folder=''):
        self.local_store_folder = folder
        self.local_store_status = status
        if folder:
            try:
                if not os.path.exists(folder):
                    os.makedirs(folder)
            except Exception as e:
                self.log.error(e)

    def config_set_ua_local(self, status, ua=None):
        if status:
            self.ua_local_status = True
            self.ua_dict = ua

    def config_set_credential(self, status, credential):
        if status:
            self.private_cookies = credential['cookies']
            self.private_user_agent = credential['user_agent']

    # -----------------------------------------  Get User Agent  ------------------------------------------------------

    def get_ua(self):
        if self.ua_local_status:

            browser = 'chrome'
            if len(self.ua_dict['randomize']) > 1:
                id = str(random.randint(1, len(self.ua_dict['randomize'])) - 1)
                try:
                    browser = self.ua_dict['randomize'][id]
                except Exception as e:
                    self.log.error(
                        '{' + '"message":"get_ua error id={} Exception={}","message_tag":""'.format(id, e) + '}')
                    pass

            item = random.randint(0, len(self.ua_dict['browsers'][browser]) - 1)

            self.log.debug('{' + '"message":"User agent is {}",'.format(
                self.ua_dict['browsers'][browser][item]) + '"message_tag":"browser-setting"' + '}')

            return self.ua_dict['browsers'][browser][item]
        else:
            ua = UserAgent(verify_ssl=False)
            return ua.random

    # -----------------------------------------  Main function - get data by usernames  -------------------------------

    def get_instagram_data_for_accounts_by_username(self, usernames):
        self.log.debug(
            '{' + '"message":"get_instagram_data_for_accounts_by_username","message_tag":""' + '}')
        urls = []
        result = []
        start = timer()

        for username in usernames:
            try:
                urls.append((1, self.InstagramAccountURL + str(username) + '/', username, self.get_ua()))
            except Exception as e:
                self.log.error(
                    '{' + '"message":"urls.append error {} in get_instagram_data_for_accounts_by_username for username {}",'.format(
                        e, username) + '"message_tag":""' + '}')
                raise

        self.log.debug(
            '{' + '"message":"Start threading with urls = {}","message_tag":""'.format(urls) + '}')

        # error = True

        # while error:

        pool = None
        while not pool:
            try:
                pool = ThreadPool(self.threads)
            except Exception as e:
                self.log.error('{' + '"message":"Pool not started {}","message_tag":""' + '}')
                time.sleep(1)
                pass

        results = pool.imap_unordered(self.fetch_url, urls)
        for page in results:
            if page['code'] == 200:
                try:
                    json_str = self.fetch_json_from_html(page['body'], page['unique_key'],
                                                         'get_instagram_data_for_accounts_by_username')
                except Exception as e:
                    self.log.error(
                        '{' + '"message":"fetch_json_from_html in get_instagram_data_for_accounts_by_username error {}",'.format(
                            e) + '"message":""' + '}')
                    json_str = ''
                    result.append({'account': {'username': page['unique_key'], 'error': 429}, 'posts': {}})

                if len(json_str) > 0:
                    # Storing JSON in folder if it needed
                    self.store_json(json_str, page['unique_key'])

                    # Export data from JSON to remote storage/DB
                    export_data = self.fetch_export_account_data_from_json(json_str, page['unique_key'])
                    result.append(export_data)
            if page['code'] == 404:
                export_data = {'account': {'username': page['unique_key'], 'error': 404}, 'posts': {}}
                result.append(export_data)
            if page['code'] == 429:
                export_data = {'account': {'username': page['unique_key'], 'error': 429}, 'posts': {}}
                result.append(export_data)
            if page['code'] == 500:
                export_data = {'account': {'username': page['unique_key'], 'error': 500,
                                           'proxy_error_counter': page[
                                               'proxy_error_counter'] if 'proxy_error_counter' in page else 0,
                                           'message': page['message'] if 'message' in page else ''},
                               'posts': {}}
                result.append(export_data)
        self.log.debug("Elapsed Time for get_instagram_data_for_accounts_by_username: {}".format(timer() - start))
        pool.close()
        return result

    # -----------------------------------------  Main function - get data by posts  -------------------------------

    def get_instagram_data_for_posts(self, codes):
        self.log.debug("get_instagram_data_for_posts")
        urls = []
        result = []
        start = timer()

        for code in codes:
            try:
                urls.append((1, self.InstagramPostURL + str(code) + '/', code, self.get_ua()))
            except Exception as e:
                self.log.exception(
                    "urls.append error {} in get_instagram_data_for_accounts_by_username for username {}".format(
                        e, code))
                raise

        self.log.debug("Start threading with urls = {}".format(urls))

        pool = None
        while not pool:
            try:
                pool = ThreadPool(self.threads)
            except Exception as e:
                self.log.error('{' + '"message":"Pool not started {}","message_tag":""' + '}')
                time.sleep(1)
                pass

        results = pool.imap_unordered(self.fetch_url, urls)
        for page in results:
            if page['code'] == 200:
                try:
                    json_str = self.fetch_json_from_html(page['body'], page['unique_key'],
                                                         'get_instagram_data_for_accounts_by_username')
                except Exception as e:
                    self.log.exception("fetch_json_from_html in get_instagram_data_for_posts error {}".format(e))
                    json_str = ''
                    result.append({'post': {'code': page['unique_key'], 'error': 429}})

                if len(json_str) > 0:
                    # Storing JSON in folder if it needed
                    self.store_json(json_str, page['unique_key'])

                    # Export data from JSON to remote storage/DB
                    export_data = self.fetch_export_post_data_from_post_page_json(json_str, page['unique_key'])

                    result.append(export_data)
            if page['code'] == 404:
                export_data = {'post': {'code': page['unique_key'], 'error': 404}}
                result.append(export_data)
            if page['code'] == 429:
                export_data = {'post': {'code': page['unique_key'], 'error': 429}}
                result.append(export_data)
            if page['code'] == 500:
                export_data = {'post': {'code': page['unique_key'], 'error': 500,
                                        'proxy_error_counter': page[
                                            'proxy_error_counter'] if 'proxy_error_counter' in page else 0,
                                        'message': page['message'] if 'message' in page else ''}
                               }
                result.append(export_data)
        self.log.debug(
            '{' + '"message":"Elapsed Time: {}",'.format(timer() - start) + '"message_tag":""' + '}')
        pool.close()
        return result

    # -----------------------------------------  Main function - get info by usernames  -------------------------------

    def get_instagram_info_for_accounts_by_ids(self, ids):
        self.log.debug("get_instagram_info_for_accounts_by_username")
        urls = []
        result = []
        start = timer()

        user_agent = self.get_ua()

        # Get page with username
        response = self.fetch_url(
            [1, self.InstagramAccountURL, None, user_agent, '', None])

        csrf_token = self.get_csrf_token(response['body'])

        cookie = None

        # Get session data for current instance
        user_agent = self.private_user_agent if self.private_user_agent else self.get_ua()
        #sequence = ['rur', 'mid', 'csrftoken', 'sessionid', 'urlgen']
        #cookies = self.cookie(self.private_cookies, sequence)

        #self.log.debug("Cookies is {}".format(cookies))

        for id in ids:
            # type_header, uri, unique_key, user_agent, headers, cookie, gis, csrf_token
            urls.append((3, self.InstagramAccountInfoURL + str(id) + '/info/', id, user_agent, '', cookie,csrf_token))
            #urls.append((1, self.InstagramAccountInfoURL + str(id) + '/info/', id, user_agent,'',cookie))

        pool = None
        while not pool:
            try:
                pool = ThreadPool(self.threads)
            except Exception as e:
                self.log.error(
                    '{' + '"message":"Pool not started, error {}",'.format(e) + '"message_tag":""' + '}')
                time.sleep(1)
                pass

        results = pool.imap_unordered(self.fetch_url, urls)
        for page in results:
            if page['code'] == 200:
                try:
                    # json_str = self.fetch_json_from_html(page['body'])
                    json_str = page['body']
                except Exception as e:
                    self.log.error(
                        '{' + '"message":"fetch_json_from_html, error {}",'.format(e) + '"message_tag":""' + '}')
                    json_str = ''
                    result.append({'account': {'username': page['unique_key'], 'error': 429}})

                if len(json_str) > 0:
                    # Storing JSON in folder if it needed
                    self.store_json(json_str, page['unique_key'])

                    # Export data from JSON to remote storage/DB
                    export_data = self.fetch_export_account_info_from_json(json_str, page['unique_key'])
                    result.append(export_data)
            if page['code'] == 404:
                export_data = {'edge': {'username': page['unique_key'], 'error': 404}}
                result.append(export_data)
            if page['code'] == 429:
                export_data = {'edge': {'username': page['unique_key'], 'error': 429}}
                result.append(export_data)
            if page['code'] == 500:
                export_data = {'edge': {'username': page['unique_key'], 'error': 500,
                                           'proxy_error_counter': page[
                                               'proxy_error_counter'] if 'proxy_error_counter' in page else 0,
                                           'message': page['message'] if 'message' in page else ''}}
                result.append(export_data)

        self.log.debug(
            '{' + '"message":"Elapsed Time: {}",'.format(timer() - start) + '"message_tag":""' + '}')
        pool.close()

        #quit()
        return result

    # -----------------------------------------  Main function - get location data by tokens  -------------------------

    def get_location_data_for_token(self, tokens):

        self.log.debug(
            '{' + '"message":"get_location_data_for_token","message_tag":""' + '}')
        urls = []
        result = []
        start = timer()

        # Get session data for current instance
        user_agent = self.private_user_agent if self.private_user_agent else self.get_ua()
        sequence = ['rur', 'mid', 'csrftoken', 'sessionid', 'urlgen']
        cookies = self.cookie(self.private_cookies, sequence)

        self.log.debug('{' + '"message":"Cookies is {}",'.format(cookies) + '"message_tag":""' + '}')

        for id in tokens:
            urls.append((6, self.InstagramLocationURL + str(id) + '/?__a=1', id, user_agent, '', cookies))

        pool = None
        while not pool:
            try:
                pool = ThreadPool(self.threads)
            except Exception as e:
                self.log.error(
                    '{' + '"message":"Pool not started, error {}",'.format(e) + '"message_tag":""' + '}')
                time.sleep(1)
                pass

        """
        self.log.debug('{' + '"message":"get_location_data_for_token","message_tag":""' + '}')
        urls = []
        result = []
        start = timer()

        for token in tokens:
            #urls.append((1, self.InstagramLocationURL + str(token) + '/?__a=1', token, self.get_ua()))
            urls.append((1, self.InstagramLocationURL + str(token), token, self.get_ua()))

        self.log.debug(
            '{' + '"message":"Start threading with urls = {}",'.format(urls) + '"message_tag":""' + '}')

        pool = None
        while not pool:
            try:
                pool = ThreadPool(self.threads)
            except Exception as e:
                self.log.error(
                    '{' + '"message":"Pool not started, error {}",'.format(e) + '"message_tag":""' + '}')
                time.sleep(1)
                pass
        """

        results = pool.imap_unordered(self.fetch_url, urls)
        for page in results:
            if page['code'] == 200:

                try:
                    # json_str = self.fetch_json_from_html(page['body'])
                    json_str = page['body'].decode('utf-8')
                except Exception as e:
                    self.log.error(
                        '{' + '"message":"fetch_json_from_html, error {}",'.format(e) + '"message_tag":""' + '}')
                    json_str = ''
                    result.append({'location': {'unique_key': page['unique_key'], 'error': 429}, 'posts': {}})
                """
                try:
                    json_str = self.fetch_json_from_html(page['body'], page['unique_key'],
                                                         'get_instagram_data_for_accounts_by_username')
                except Exception as e:
                    self.log.error(
                        '{' + '"message":"fetch_json_from_html in get_location_data_for_token error {}",'.format(
                            e) + '"message":""' + '}')
                    json_str = ''
                    export_data = {'location': {'token': page['unique_key'], 'error': 429, 'data': {}}}
                    result.append(export_data)
                """

                try:
                    json.loads(json_str)
                except Exception as err:
                    self.log.exception('Json load error')
                    json_str = ''
                    export_data = {'location': {'token': page['unique_key'], 'error': 429, 'data': {}}}
                    result.append(export_data)
                    pass

                if len(json_str) > 0:
                    # Storing JSON in folder if it needed
                    self.store_json(json_str, page['unique_key'])

                    # Export data from JSON to remote storage/DB
                    export_data = self.fetch_export_location_data_from_json(json_str, page['unique_key'])
                    result.append(export_data)
            if page['code'] == 404:
                export_data = {'location': {'token': page['unique_key'], 'error': 404, 'data': {}}}
                result.append(export_data)
            if page['code'] == 429:
                export_data = {'location': {'token': page['unique_key'], 'error': 429, 'data': {}}}
                result.append(export_data)
            if page['code'] == 500:
                export_data = {'location': {'token': page['unique_key'], 'error': 500,
                                            'proxy_error_counter': page[
                                                'proxy_error_counter'] if 'proxy_error_counter' in page else 0,
                                            'message': page['message'] if 'message' in page else '',
                                            'data': {}},
                               'posts': {}}
                result.append(export_data)

        self.log.debug(
            '{' + '"message":"Elapsed Time: {}",'.format(timer() - start) + '"message_tag":""' + '}')
        pool.close()
        return result

    """
    def get_location_data_for_token_deprecated(self, tokens):
        self.log.debug('{' + '"message":"get_location_data_for_token","message_tag":""' + '}')
        urls = []
        result = []
        start = timer()

        for token in tokens:
            #urls.append((1, self.InstagramLocationURL + str(token) + '/?__a=1', token, self.get_ua()))
            urls.append((1, self.InstagramLocationURL + str(token), token, self.get_ua()))

        self.log.debug(
            '{' + '"message":"Start threading with urls = {}",'.format(urls) + '"message_tag":""' + '}')

        pool = None
        while not pool:
            try:
                pool = ThreadPool(self.threads)
            except Exception as e:
                self.log.error(
                    '{' + '"message":"Pool not started, error {}",'.format(e) + '"message_tag":""' + '}')
                time.sleep(1)
                pass

        results = pool.imap_unordered(self.fetch_url, urls)
        for page in results:
            if page['code'] == 200:

                try:
                    # json_str = self.fetch_json_from_html(page['body'])
                    json_str = page['body'].decode('utf-8')
                except Exception as e:
                    self.log.error(
                        '{' + '"message":"fetch_json_from_html, error {}",'.format(e) + '"message_tag":""' + '}')
                    json_str = ''
                    result.append({'location': {'unique_key': page['unique_key'], 'error': 429}, 'posts': {}})

                try:
                    json_str = self.fetch_json_from_html(page['body'], page['unique_key'],
                                                         'get_instagram_data_for_accounts_by_username')
                except Exception as e:
                    self.log.error(
                        '{' + '"message":"fetch_json_from_html in get_location_data_for_token error {}",'.format(
                            e) + '"message":""' + '}')
                    json_str = ''
                    export_data = {'location': {'token': page['unique_key'], 'error': 429, 'data': {}}}
                    result.append(export_data)


                try:
                    json.loads(json_str)
                except Exception as err:
                    self.log.exception('Json load error')
                    json_str = ''
                    export_data = {'location': {'token': page['unique_key'], 'error': 429, 'data': {}}}
                    result.append(export_data)
                    pass

                if len(json_str) > 0:
                    # Storing JSON in folder if it needed
                    self.store_json(json_str, page['unique_key'])

                    # Export data from JSON to remote storage/DB
                    export_data = self.fetch_export_location_data_from_json(json_str, page['unique_key'])
                    result.append(export_data)
            if page['code'] == 404:
                export_data = {'location': {'token': page['unique_key'], 'error': 404, 'data': {}}}
                result.append(export_data)
            if page['code'] == 429:
                export_data = {'location': {'token': page['unique_key'], 'error': 429, 'data': {}}}
                result.append(export_data)
            if page['code'] == 500:
                export_data = {'location': {'token': page['unique_key'], 'error': 500,
                                            'proxy_error_counter': page[
                                                'proxy_error_counter'] if 'proxy_error_counter' in page else 0,
                                            'message': page['message'] if 'message' in page else '',
                                            'data': {}},
                               'posts': {}}
                result.append(export_data)

        self.log.debug(
            '{' + '"message":"Elapsed Time: {}",'.format(timer() - start) + '"message_tag":""' + '}')
        pool.close()
        return result
    """

    def get_csrf_token(self, html):
        json_str = self.fetch_json_from_html(html, '', 'get_csrf_token')

        try:
            json_dict = json.loads(json_str)
        except Exception as err:
            self.log.exception(err)
            raise

        result = json_dict['config']['csrf_token']

        return result

    # -----------------------------------------  Main function - get posts by username  -------------------------------

    def get_all_posts_by_username(self, username, limit, last_post_exist=None, cookie=''):

        start = timer()

        default = [{'posts': [], 'error': []}]

        # Get Query Hash
        user_agent = self.get_ua()

        response_error = True
        self.export_posts = True

        query_hash = ''
        json_str = ''
        csrf_token = ''

        self.log.debug('!!----------Start download account page at get_all_posts_by_username to get data')

        session = requests.Session()

        round = 0

        while response_error:

            self.log.debug('!!---------- Round {}'.format(round))

            round = round + 1

            # Get page with username
            response = self.fetch_url(
                [1, self.InstagramAccountURL + str(username) + '/', username, user_agent, '', cookie])
            cookie = session.cookies.get_dict()

            self.log.debug(
                '{' + '"message":"Get response {} on first request","message_tag":""'.format(response['code']) + '}')

            if response['code'] == 404:
                return default

            # Get query hash
            query_hash = self.get_query_hash(response['body'], 'ProfilePageContainer', user_agent)

            # Get x-csrftoken
            csrf_token = self.get_csrf_token(response['body'])

            self.log.debug("Get query_hash {} on first request, csrf_token {}".format(query_hash, csrf_token))

            # todo: solve query_hashempty
            if query_hash:
                try:
                    json_str = self.fetch_json_from_html(response['body'], str(username), 'get_all_posts_by_username')
                    response_error = False
                except Exception as e:
                    self.log.error(
                        '{' + '"message":"fetch_json_from_html, error {}",'.format(e) + '"message_tag":""' + '}')
                    time.sleep(5)
                    pass

        self.log.debug('!!----------Parsing json form first request')

        if len(json_str) > 0:
            # Export data from JSON

            self.log.debug(
                '{' + '"message":"Response have json, unique_key is {} ","message_tag":""'.format(
                    response['unique_key']) + '}')

            account_data = self.fetch_export_account_data_from_json(json_str, response['unique_key'])
            posts = account_data.pop('posts')
            after = self.extract_after_from_account_page_json(json_str)
            # rhx_gis = self.extract_rhx_gis_from_account_page_json(json_str)
            rhx_gis = None

            self.log.debug(
                '{' + '"message":"Account data={}, after={}, numbers of posts={}",'.format(account_data, after,
                                                                                           len(
                                                                                               posts)) + '"message_tag":"posts-scraping"' + '}')
        else:
            return default

        self.log.debug('Last post exists in storage is {}'.format(last_post_exist))
        if last_post_exist:
            if self.check_if_posts_older_then_exists(posts, last_post_exist):
                self.log.debug('We get post older the limit - stop')
                posts = self.return_posts_not_older_then_datetime(posts, last_post_exist)
                return {'posts': posts, 'error': []}
            else:
                self.log.debug('We need get more posts - continue')

        self.log.debug('!!---------- Start looping posts by Ajax')

        num_posts = 48

        if after['has_next_page']:
            while (after['has_next_page']):
                time.sleep(1)

                response_error = True

                round = 0

                while response_error:

                    variable = self.get_query_posts_variable(account_data['account']['user_id'], num_posts,
                                                             after['end_cursor'])
                    request = 'https://www.instagram.com/graphql/query/?query_hash=' + query_hash + '&variables=' + variable
                    gis = hashlib.md5(variable.encode('utf-8')).hexdigest()

                    self.log.debug("start looping posts, round = {}, request = {}, gis = {}, csrf={} ".format(
                        round, request, gis, csrf_token))

                    response = self.fetch_url([2, request, '', user_agent, gis, cookie, csrf_token])

                    if response['code'] >= 500:
                        num_posts = int(num_posts / 2) if num_posts > 12 else 12
                        if self.debug: self.log.error(
                            '{' + '"message":"Get 500 error on request {}","message_tag":""'.format(request) + '}')
                    elif response['code'] == 200:

                        if response['body']:
                            try:
                                result = self.extract_posts_and_after_from_posts_json(response['body'])
                                response_error = False
                            except Exception as e:
                                if self.debug: self.log.error(
                                    '{' + '"message":"extract_posts_and_after_from_posts_json, error {}, resend request",'.format(
                                        e) + '"message_tag":""' + '}')
                                time.sleep(5)
                                pass

                    round = round + 1

                after = result['after']
                posts += result['posts']
                self.log.debug('{' + '"message":"Posts summary = {}, limit = {}",'.format(len(posts),
                                                                                          limit) + '"message_tag":"posts-scraping"' + '}')
                if last_post_exist:
                    if self.check_if_posts_older_then_exists(posts, last_post_exist):
                        self.log.debug('We get post older the limit - stop')
                        posts = self.return_posts_not_older_then_datetime(posts, last_post_exist)
                        after['has_next_page'] = False
                    else:
                        self.log.debug('Do not found posts older then exists'.format(last_post_exist))

                if not result['posts']:
                    after['has_next_page'] = False

                if len(posts) > limit and limit:
                    after['has_next_page'] = False
        else:
            self.log.debug('{' + '"message":"No new posts. Current values of posts is {}",'.format(
                len(posts)) + '"message_tag":"posts-scraping"' + '}')

        self.log.debug(
            '{' + '"message":"Elapsed Time: {}",'.format(timer() - start) + '"message_tag":"posts-scraping"' + '}')

        return {'posts': posts, 'error': []}

    # -----------------------------------------  Main function - get posts by username  -------------------------------

    def get_engagement_rate(self, account, posts):
        stat = self.transform_instagram_account_json_to_engagement_data_dict(account, posts)
        return stat

    def get_likes_to_comments_rate(self, account, posts):
        stat = self.transform_instagram_account_json_to_engagement_data_dict(account, posts)
        return stat

    def check_if_posts_older_then_exists(self, posts, last_datetime):

        oldest_past_date_index = min(range(len(posts)), key=lambda index: posts[index]['created'])
        oldest_post_datetime = datetime.utcfromtimestamp(posts[oldest_past_date_index]['created']).replace(
            tzinfo=timezone.utc)

        self.log.debug(
            '{' + '"message":"Last post timestamp is: {}",'.format(
                oldest_post_datetime) + '"message_tag":"posts-scraping"' + '}')

        return True if last_datetime > oldest_post_datetime else False

    def return_posts_not_older_then_datetime(self, posts, datetime):
        result = []
        for post in posts:
            post_datetime = datetime.utcfromtimestamp(post['created']).replace(tzinfo=timezone.utc)

            if datetime >= post_datetime:
                return result
            else:
                result.append(post)

        return result

    # -----------------------------------------  Main function - get comments by post code  ---------------------------

    def get_all_comments_by_post(self, shortcode, limit, cookie=''):

        self.log.debug('get_all_comments_by_post')

        post_data = {'comments': [], 'error': [], sum: 0, 'after': {'has_next_page': False}}

        error = True

        user_agent = self.get_ua()

        # Set flag to export first 12 posts from page
        self.export_posts = True

        error_counter = 0

        while error:

            # Get Query Hash
            user_agent = self.get_ua()

            # Get page with username
            response = self.fetch_url([1, self.InstagramPostURL + str(shortcode) + '/', shortcode, user_agent])

            self.log.debug('Response code in get_all_comments_by_post is {}'.format(response['code']))

            if 'code' in response and response['code'] == 404:
                return {'comments': [{'id': {'error': 404, 'code': str(shortcode)}}], 'sum': 0}

            if 'Restricted Video' in response['body'].decode('utf-8'):
                self.log.error({
                    'message': 'Restricted page with video {}'.format(shortcode),
                    'token': '',
                    'message_tag': ''
                })
                return {'comments': [{'id': {'error': 404, 'code': str(shortcode)}}], 'sum': 0}

            try:
                json_str = self.fetch_json_from_html(response['body'], shortcode, 'get_all_comments_by_post')
                post_data = self.fetch_export_post_data_from_json(json_str, shortcode)
                error = False if 'error' not in post_data else True
            except Exception as e:
                error_counter = error_counter + 1
                # If looping too much time - broke
                if error_counter > 5:
                    self.log.error('{' + '"message":"str 375 {}","message_tag":"json-parsing-error"'.format(e) + '}')
                    error = False
                    error_counter = 1000
                time.sleep(5)
                pass

        if error_counter > 999:
            return {'comments': [], 'error': [], sum: 0, 'after': {'has_next_page': False}}

        self.log.debug('Comments summary in post data is {}'.format(post_data['count']))

        if not post_data['after']['has_next_page']:
            self.log.debug('There is not comment in post. Return to main app')
            return {'comments': post_data['comments'], 'error': [], 'complete_by': 'has_next_page',
                    'sum': post_data['count']}

        query_hash = self.get_query_hash(response['body'], 'PostPageContainer', user_agent)

        # Get x-csrftoken
        csrf_token = self.get_csrf_token(response['body'])

        # todo: solve query_hash empty
        if not query_hash:
            return post_data

        time.sleep(1)

        self.log.debug('Query hash %s', query_hash)

        comments = post_data['comments']
        after = post_data['after']
        # rhx_gis = self.extract_rhx_gis_from_account_page_json(json_str)
        self.log.debug('After data = %s', after)
        # self.log.debug('rhx_gis = %s', rhx_gis)
        self.log.debug('Comments number is %s', len(comments))

        complete_by = 'has_next_page'
        if 'has_next_page' in after and after['has_next_page']:
            self.log.debug('has_next_page is True')
            while (after['has_next_page']):
                self.log.debug('Start get comments')
                time.sleep(1)
                variable = self.get_query_comments_variable(shortcode, 24, after['end_cursor'])
                request = 'https://www.instagram.com/graphql/query/?query_hash=' + query_hash + '&variables=' + variable
                # gis = hashlib.md5((rhx_gis + ':' + variable).encode('utf-8')).hexdigest()
                gis = hashlib.md5((variable).encode('utf-8')).hexdigest()

                self.log.debug('gis %s', gis)
                self.log.debug('Request %s', request)

                response = self.fetch_url([2, request, '', user_agent, gis, '', csrf_token])

                if response['body']:
                    result = self.extract_comments_and_after_from_comments_json(response['body'], shortcode)
                    after = result['after']
                    comments += result['comments']
                    self.log.debug('Comments summary = %s, limit = %s, aftter= %s', len(comments), limit,
                                   after)
                    time.sleep(2)
                else:
                    after['has_next_page'] = False
                    complete_by = 'has_next_page'

                if len(comments) > limit:
                    after['has_next_page'] = False
                    complete_by = 'limit'
        else:
            if self.debug:  self.log.error('Not next comments')
            self.log.debug('Current values of comments is %s', len(comments))

        return {'comments': comments, 'error': [], 'complete_by': complete_by, 'sum': post_data['count']}

    # -----------------------------------------  Fetch account data from JSON  ----------------------------------------

    def fetch_export_account_data_from_json(self, json_string, username):

        try:
            json_dict = json.loads(json_string)
        except Exception as e:
            self.log.error(
                '{' + '"message":"Json parsing problem => username = {}'.format(username) + ' str was {}'.format(
                    json_string) + '"message_tag":""' + '}')
            return {'account': {'username': username, 'error': 404, 'message': 'Json not parsed'}, 'posts': {}}

        instagram_dict = []
        posts_dict = []

        if 'entry_data' not in json_dict:
            return {'account': {'username': username, 'error': 404, 'message': 'entry_data not found'}, 'posts': {}}
        else:
            # If we ask to export account data
            if self.export_account:
                instagram_dict = self.transform_instagram_account_json_to_account_data_dict(json_dict)
                # if error during tranform - return as 404
                if not instagram_dict:
                    return {'account': {'username': username, 'error': 404}, 'posts': {}}

            # If we ask to export posts data
            if self.export_posts:
                self.log.debug('{' + '"message":"Extract posts from account page":"message_tag":""' + '}')
                posts_dict = self.transform_instagram_account_json_to_post_data_dict(json_dict)
            else:
                self.log.debug('{' + '"message":"DO NOT Extract posts from account page":"message_tag":""' + '}')

            # If we ask to count accounts staistic
            if self.count_engagements:
                # If we didn't ask to get account data earlie - do it now
                if self.export_account == False:
                    instagram_dict = self.transform_instagram_account_json_to_post_data_dict(json_dict)
                    if not instagram_dict:
                        return {'account': {'username': username, 'error': 404}, 'posts': {}}

                # If we didn't ask to get account posts earlie - do it now
                if self.export_posts == False:
                    posts_dict = self.transform_instagram_account_json_to_post_data_dict(json_dict)

                # Count stat
                stat_dict = self.transform_instagram_account_json_to_engagement_data_dict(instagram_dict, posts_dict)

                # Merge account data and stat in one dict
                instagram_dict = {**instagram_dict, **stat_dict}

            # Return account data
            return {'json-parsing-error': False, 'account': instagram_dict, 'posts': posts_dict}

    # -----------------------------------------  Fetch account INFO from JSON  -------------------------

    def fetch_export_account_info_from_json(self, json_string, username):

        try:
            json_dict = json.loads(json_string)
        except Exception as e:
            self.log.error(
                '{' + '"message":"Json parsing problem => username = {}'.format(username) + ' str was {}'.format(
                    json_string) + '"message_tag":""' + '}')
            return {'account': {'username': username, 'error': 404, 'message': 'Json not parsed'}, 'posts': {}}

        info_dict = []

        if 'user' not in json_dict:
            return {'info': {'username': username, 'error': 404, 'message': '"user" element not found'}}
        else:
            # If we ask to export account data
            if self.export_account:
                info_dict = self.transform_instagram_account_json_to_account_info_dict(json_dict)
                # if error during tranform - return as 404
                if not info_dict:
                    return {'info': {'username': username, 'error': 404}}

            # Return account data
            return {'json-parsing-error': False, 'edge': info_dict}

    # -----------------------------------------  Fetch account data from JSON  ----------------------------------------

    def fetch_export_post_data_from_post_page_json(self, json_string, code):

        try:
            json_dict = json.loads(json_string)
        except Exception as e:
            self.log.exception("Json parsing problem => code = {}".format(code))
            return {'post': {'code': code, 'error': 404, 'message': 'Json not parsed'}, 'posts': {}}

        post_dict = {}

        if 'entry_data' not in json_dict:
            return {'post': {'code': code, 'error': 404, 'message': 'entry_data not found'}, 'posts': {}}
        else:
            # If we ask to export account data
            if self.export_account:
                post_dict = self.transform_instagram_post_json_from_post_page_to_dict(
                    json_dict['entry_data']['PostPage'][0]['graphql']['shortcode_media'], code)

                # if error during tranform - return as 404
                if not post_dict:
                    return {'post': {'code': code, 'error': 404}}

            # Return account data
            return {'post': {'code': code, 'data': post_dict,'error':False}}

    # -------------------------------------------------------------------------------------------------

    def transform_instagram_post_json_from_post_page_to_dict(self, json_dict, code):

        medias = []
        try:
            if 'edge_sidecar_to_children' in json_dict:
                self.log.debug('There are multiple medias {}'.format(code))
                medias = [{'id': d['node']['id'], 'image': d['node']['display_url'],
                           'accessibility_caption': d['node']['accessibility_caption'] if 'accessibility_caption' in d['node'] else '',
                           'is_video': d['node']['is_video'] if 'is_video' in d['node'] else False} for d in json_dict['edge_sidecar_to_children']['edges']]
            elif json_dict['is_video']:
                medias = [{'image': json_dict['display_url'], 'accessibility_caption': None, 'is_video': True}]
            else:
                medias = [
                    {'image': json_dict['display_url'], 'accessibility_caption': json_dict['accessibility_caption'],
                     'is_video': False}]
        except:
            self.log.debug(json_dict)
            raise

        result = {
            'code': json_dict['shortcode'],
            'username': json_dict['owner']['username'],
            'user_id': json_dict['owner']['id'],
            'fullname': json_dict['owner']['full_name'],
            'private': json_dict['owner']['is_private'],
            'verified': json_dict['owner']['is_verified'],
            'media': medias,
            'is_video': json_dict['is_video'],
            'created': json_dict['taken_at_timestamp'],
        }

        return result

    # ----------------------------------------- Count account stat info ----------------------------------------

    # -----------------------------------------  Fetch location data from JSON  -------------------------

    def fetch_export_location_data_from_json(self, json_string, token):

        try:
            json_dict = json.loads(json_string)
        except Exception as e:
            self.log.exception('Json parsing problem fetch_export_location_data_from_json => token = {}'.format(
                token) + ' str was {}'.format(
                json_string))
            return {'account': {'unique_key': token, 'error': 404, 'message': 'Json not parsed'}, 'posts': {}}

        try:
            location = json_dict['graphql']['location']
        except Exception as e:
            self.log.exception('json_dict[graphql][location] problem token = {}'.format(token) + ' str was {}'.format(
                json_string))
            location = {}
            pass

        try:
            if location['address_json']:
                location['address_dict'] = json.loads(location['address_json'])
            else:
                location['address_dict'] = {}
        except Exception as e:
            location['address_dict'] = {}
            pass

        try:
            location['edge_location_to_media'] = ''
            location['edge_location_to_top_posts'] = ''
            location['address_json'] = ''
        except Exception as e:
            self.log.exception('Json parsing problem => token = {}'.format(token) + ' str was {},{}'.format(
                json_string, location['address_json']))
            return {'location': {'token': token, 'error': 404, 'message': 'Json not parsed'}, 'posts': {}}

        return {'location': {'token': token, 'error': False, 'data': location}}

    # ------------------------------------------ Fetch post data from JSON --------------------------------------------

    def fetch_export_post_data_from_json(self, json_string, shortcode):
        try:
            json_dict = json.loads(json_string)
        except Exception as e:
            self.log.error(
                'Json parsing problem => shortcode = ' + shortcode + ' str was ' + str(json_string))
            return {'error': True, 'message': 'Json not parsed'}

        try:
            if 'edge_media_to_comment' in json_dict['entry_data']['PostPage'][0]['graphql']['shortcode_media']:
                comments = json_dict['entry_data']['PostPage'][0]['graphql']['shortcode_media']['edge_media_to_comment']
                return {'count': comments['count'], 'after': comments['page_info'],
                        'comments': self.transform_comments_dict(comments['edges'], shortcode)}

            if 'edge_media_to_parent_comment' in json_dict['entry_data']['PostPage'][0]['graphql']['shortcode_media']:
                comments = json_dict['entry_data']['PostPage'][0]['graphql']['shortcode_media'][
                    'edge_media_to_parent_comment']
                return {'count': comments['count'], 'after': comments['page_info'],
                        'comments': self.transform_comments_dict(comments['edges'], shortcode)}

            self.log.error(
                ' edge_media_to_comment not found ' + str(shortcode) + ' json = {}'.format(
                    json_dict['entry_data']['PostPage'][0]['graphql']))
            return {'count': 0, 'after': False, 'comments': {}}
        except Exception as e:
            self.log.error('json_dict is empty, %s', e)
            return {'count': 0, 'after': False, 'comments': {}}

    """
    def fetch_export_post_data_from_json(self, json_string, shortcode):
        try:
            json_dict = json.loads(json_string)
        except Exception as e:
            self.log.error(
                '{' + '"message":"Json parsing problem => shortcode = {}'.format(shortcode) + ' str was {}'.format(
                    json_string) + '"message_tag":""' + '}')
            return {'error': True, 'message': 'Json not parsed'}

        try:
            comments = json_dict['entry_data']['PostPage'][0]['graphql']['shortcode_media']['edge_media_to_comment']
        except Exception as e:
            self.log.error(
                '{' + '"message":"edge_media_to_comment not found = {}'.format(shortcode) + ' str was {}'.format(
                    json_string) + '"message_tag":""' + '}')
            return {'count': 0, 'after': False, 'comments': {}}

        return {'count': comments['count'], 'after': comments['page_info'],
                'comments': self.transform_comments_dict(comments['edges'])}
    """

    # -----------------------------------------  Extract main account data from account dict  -------------------------

    def transform_instagram_account_json_to_account_data_dict(self, json_dict):
        account = []
        try:
            if not json_dict['entry_data']['ProfilePage'][0]['graphql']['user']:
                self.log.error(
                    '{' + '"message":"transform_instagram_account_json_to_account_data_dict cant find user in dict","message":""' + '}')
                return account
        except Exception as e:
            self.log.error(
                '{' + '"message":"transform error {}","message":""'.format(e) + '}')
            return account

        try:
            json_object = json_dict['entry_data']['ProfilePage'][0]['graphql']['user']
        except Exception as e:
            self.log.error(
                '{' + '"message":"json_object {}","message":""'.format(e) + '}')
            return []

        account = {
            'username': json_object['username'],
            'business': True if json_object['is_business_account'] else False,
            'category': json_object['business_category_name'] if 'business_category_name' in json_dict else '',
            'private': True if json_object['is_private'] else False,
            'fullname': json_object['full_name'] if json_object['full_name'] else '',
            'profile_pic': json_object['profile_pic_url'] if json_object['profile_pic_url'] else '',
            'followers': json_object['edge_followed_by']['count'],
            'follow': json_object['edge_follow']['count'],
            'posts_count': json_object['edge_owner_to_timeline_media']['count'],
            'bio': json_object['biography'] if json_object['biography'] else '',
            'user_id': json_object['id'],
            'current_unixtime': time.time(),
            'current_datetime': str(datetime.now().strftime("%Y-%m-%d %H:%M:%S")),
            'error': False
        }

        return account

    # -----------------------------------------  Extract main account data from account dict  -------------------------

    def transform_instagram_account_json_to_account_info_dict(self, json_dict):
        info = []
        try:
            if not json_dict['user']['pk']:
                self.log.error(
                    'transform_instagram_account_json_to_account_info_dict cant find user in dict')
                return info
        except Exception as e:
            self.log.error(str(e))
            return info

        try:
            json_object = json_dict['user']
        except Exception as e:
            self.log.error(str(e))
            return []

        location = dict()
        if 'latitude' in json_dict['user'] and 'longitude' in json_dict['user']:
            location['point'] = [{'latitude': json_dict['user']['latitude'], 'len': json_dict['user']['longitude']}]

        if 'instagram_location_id' in json_dict['user']:
            location['id'] = json_dict['user']['instagram_location_id']

        if 'address_street' in json_dict['user']:
            location['street'] = json_dict['user']['address_street']

        if 'city_name' in json_dict['user']:
            location['city'] = json_dict['user']['city_name']

        info = {
            'username': json_object['username'],
            'user_id': json_object['pk'],
            'fullname': json_object['full_name'] if 'full_name' in json_object else '',
            'private': json_object['is_private'],
            'verified': json_object['is_verified'],

            'media': json_object['media_count'],
            'follow': json_object['following_count'],
            'followers': json_object['follower_count'],
            'follow_tags': json_object['following_tag_count'],
            'posts_count': json_object['media_count'],

            'bio': json_object['biography'] if json_object['biography'] else '',
            'external_url': json_object['external_url'],

            'business': json_object['is_business'] if 'is_business' in json_object else False,

            'category': json_object['category'] if 'category' in json_object else '',
            'public_email': str(json_object['public_email'])[:99] if 'public_email' in json_object else '',

            'profile_pic': json_object['profile_pic_url'] if json_object['profile_pic_url'] else '',
            'profile_pic_hd':json_object['hd_profile_pic_url_info'] if json_object['hd_profile_pic_url_info'] else '',
            'location': location,

            'current_unixtime': time.time(),
            'current_datetime': str(datetime.now().strftime("%Y-%m-%d %H:%M:%S")),

            'status': 3,  # Status for DB storing - showing account have already get info
            'error': False
        }

        return {'username': json_object['username'], 'error': False, 'data': info, 'json': json.dumps(json_dict)}

        # ----------------------------------------- Count account stat info ----------------------------------------

    def transform_instagram_account_json_to_engagement_data_dict(self, account_dict, posts_dict):

        first = 2_000_000_000
        last = 1
        engagement = 0
        summary = {'likes': 0, 'comments': 0}

        for post in posts_dict:
            engagement += post['likes'] + post['comments']
            summary['likes'] += post['likes']
            summary['comments'] += post['comments']
            first = post['created'] if first > post['created'] else first
            last = post['created'] if last < post['created'] else last

        diff = (last - first) if (last - first) > 0 else 1_000_000_000
        if len(posts_dict) > 0:
            er = engagement / len(posts_dict) / account_dict['followers'] if account_dict['followers'] > 0 else 0
            avr_likes = int(summary['likes'] / len(posts_dict))
        else:
            er = 0
            avr_likes = 0
        pd = 0 if account_dict['private'] else 12 / diff * (24 * 60 * 60)
        pm = pd * 30

        stat = {
            'followers': account_dict['followers'],
            'follow': account_dict['follow'],
            'posts': account_dict['posts_count'],
            'er': "{:0.2f}".format(er * 100),
            'ltcr': "{:0.2f}".format(summary['comments'] / summary['likes'] * 100 if summary['likes'] > 0 else 0),
            'pd': "{:0.3f}".format(pd),
            'pm': "{:0.3f}".format(pm),
            'avr_likes': avr_likes,
            'bio': account_dict['bio'],
            'profile_pic': account_dict['profile_pic'],
            'impressions': None,
            'full_name': None,
            'reach': None
        }
        return stat

        # -----------------------------------------  Extract posts data from account dict-----------------------------------

    def transform_instagram_account_json_to_post_data_dict(self, json_dict):
        posts = []
        if not json_dict['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges']:
            return posts
        else:
            posts_object = json_dict['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media'][
                'edges']
            posts = self.transform_posts_dict(posts_object)

        return posts

    def transform_posts_dict(self, posts_object):
        posts = []

        self.log.debug('{' + '"message":"Posts in obgect is {}",'.format(
            len(posts_object)) + '"message_tag":"transform-json"' + '}')

        if len(posts_object) > 0:

            for edge in posts_object:

                try:
                    like = edge['node']['edge_liked_by']['count'] if edge['node']['edge_liked_by']['count'] else 0
                except:
                    like = 0
                    pass

                like = edge['node']['edge_media_preview_like']['count'] if edge['node']['edge_media_preview_like'][
                    'count'] else like

                tagged_users = []
                tagged = edge['node']['edge_media_to_tagged_user'] if 'edge_media_to_tagged_user' in edge[
                    'node'] else {}

                if 'edges' in tagged:
                    tagged_users = self.transform_tagged_dict(tagged['edges'])

                caption = edge['node']['edge_media_to_caption']['edges'][0]['node']['text'] if \
                    edge['node']['edge_media_to_caption']['edges'] else ''

                post = {
                    'id': edge['node']['id'],
                    'owner': edge['node']['owner']['username'],
                    'code': edge['node']['shortcode'],
                    'created': edge['node']['taken_at_timestamp'],
                    'caption': caption,
                    'image': edge['node']['thumbnail_src'],
                    'likes': like,
                    'location': edge['node']['location'],
                    'tagged_users': tagged_users,
                    'caption_users': self.get_caption_users(caption),
                    'comments': edge['node']['edge_media_to_comment']['count'],
                }
                posts.append(post)

        return posts

    def transform_tagged_dict(self, data):
        result = []
        for item in data:
            result.append({'id': item['node']['user']['id'], 'username': str(item['node']['user']['username']).lower()})

        return result

    def transform_comments_dict(self, comments_object, shortcode=''):
        comments = []

        self.log.debug(
            '{' + '"message":"Comments in {} object is {}",'.format(shortcode,
                                                                    len(comments_object)) + '"message_tag":""' + '}')

        for edge in comments_object:
            node = edge['node']
            owner = node.pop('owner')
            likes = node.pop('edge_liked_by')
            comment = node
            comment['user_id'] = owner['id']
            comment['username'] = owner['username']
            comment['likes'] = likes['count']
            comments.append(comment)

        return comments

    def extract_rhx_gis_from_account_page_json(self, json_str):

        try:
            json_dict = json.loads(json_str)
        except Exception as e:
            self.log.error(
                '{' + '"message":"Json parsing problem =>  extract_after_from_account_page_json {}","message_tag":""'.format(
                    e) + '}')
            return ''

        return json_dict['rhx_gis']

    def store_json(self, json_obj, username):
        if self.local_store_status:
            filename = "{}.json".format(username)
            path = os.path.join(self.local_store_folder, filename)
            self.export_data_to_local_store(path, json_obj)

    def export_instagram_data(self, data):
        if self.ftp_export_status:
            self.log.debug('{' + '"message":"Export data to FTP","message_tag":""' + '}')
        if self.aws_s3_export_status:
            self.log.debug('{' + '"message":"Export data to AWS S3","message_tag":""' + '}')

    def export_data_to_local_store(self, path, data):
        try:
            with open(path, 'w', encoding='utf-8') as f:
                f.write(str(data))
        except Exception as e:
            self.log.error(e)

    def extract_after_from_account_page_json(self, json_str):
        try:
            json_dict = json.loads(json_str)
        except Exception as e:
            self.log.error(
                '{' + '"message":"Json parsing problem =>  extract_after_from_account_page_json {}",'.format(
                    e) + '"message_tag":""' + '}')
            return {'after': '', 'has_next_page': False}

        if not json_dict['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media'][
            'page_info']:
            return {'after': '', 'has_next_page': False}
        else:
            page_info_object = \
                json_dict['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media'][
                    'page_info']
            return {'end_cursor': page_info_object['end_cursor'], 'has_next_page': page_info_object['has_next_page']}

    def extract_posts_and_after_from_posts_json(self, json_str):
        try:
            json_dict = json.loads(json_str)
        except Exception as e:
            # self.log.error(
            #    '{' + '"message":"Json parsing problem =>  extract_posts_and_after_from_posts_json {}","message_id":""'.format(json_str) + '}')
            # return {'posts': [], 'after': {'end_cursor': '', 'has_next_page': False}}
            raise Exception('Json parsing problem =>  extract_posts_and_after_from_posts_json {}'.format(e))

        if not json_dict['data']['user']['edge_owner_to_timeline_media']:
            return {'posts': [], 'after': {'end_cursor': '', 'has_next_page': False}}
        else:
            posts_object = json_dict['data']['user']['edge_owner_to_timeline_media']

            posts = self.transform_posts_dict(posts_object['edges'])

            return {'posts': posts, 'after': {'end_cursor': posts_object['page_info']['end_cursor'],
                                              'has_next_page': posts_object['page_info'][
                                                  'has_next_page']}}

    def extract_comments_and_after_from_comments_json(self, json_str, shortcode):
        try:
            json_dict = json.loads(json_str)
        except Exception as e:
            self.log.error(
                '{' + '"message":"Json parsing problem =>  extract_comments_and_after_from_comments_json {}","message_tag":""'.format(
                    e) + '}')
            return {'posts': [], 'after': {'end_cursor': '', 'has_next_page': False}}

        if not json_dict['data']['shortcode_media']['edge_media_to_comment']:
            return {'posts': [], 'after': {'end_cursor': '', 'has_next_page': False}}
        else:
            comments_object = json_dict['data']['shortcode_media']['edge_media_to_comment']

            comments = self.transform_comments_dict(comments_object['edges'], shortcode)

            return {'comments': comments, 'after': {'end_cursor': comments_object['page_info']['end_cursor'],
                                                    'has_next_page': comments_object['page_info'][
                                                        'has_next_page']}}

    # -----------------------------------------   Fetching data from url ----------------------------------------

    def fetch_url(self, element):

        type_header, uri, unique_key, user_agent, headers, cookie, gis, csrf_token = [1, '', '', '', '', '', '', '']

        if len(element) == 4:
            type_header, uri, unique_key, user_agent = element
        elif len(element) == 6:
            type_header, uri, unique_key, user_agent, gis, cookie = element
            csrf_token = ''
        elif len(element) == 7:
            type_header, uri, unique_key, user_agent, gis, cookie, csrf_token = element
        else:
            self.log.error(
                '{' + '"message":"Elements number in fetch_url is wrong","message_tag":"parsing-fetch-url"' + '}')
            raise Exception(' Elements number in fetch_url is wrong = {}'.format(len(element)) + ' {}'.format(element))

        if type_header == 1:
            headers = self.get_short_header(user_agent)

        if type_header == 2:
            headers = self.get_full_header(user_agent, gis, cookie, csrf_token)

        if type_header == 3:
            headers = self.get_info_header(user_agent, cookie, csrf_token)

        self.log.debug("type_header = {}, Current header is {}, URI is {} ".format(type_header, headers, uri))

        try:
            if self.proxy_status:
                self.log.debug(
                    '{' + '"message":"Send request {} with proxy {}",'.format(uri,
                                                                              self.proxy) + '"message_tag":"parsing-fetch-url"' + '}')
                r = requests.get(uri, headers=headers, proxies=self.proxy)

                self.log.debug('{' + '"message":"Request status is {}","message_tag":""'.format(r.status_code) + '}')

                #if r.status_code != 200:
                #    self.log.debug(r.content)

            else:
                self.log.debug(
                    '{' + '"message":"Send request without proxy","message_tag":"parsing-fetch-url"' + '}')
                r = requests.get(uri, stream=True, headers=headers)

        except requests.exceptions.ReadTimeout:
            self.log.error(
                '{' + '"message":"Oops. Read timeout occurred!","message_tag":"parsing-fetch-url-error"' + '}')
            self.proxy_error += 1
            return {'body': '', 'code': 500, 'not-found': False, 'error': True, 'unique_key': unique_key,
                    'proxy_error_counter': self.proxy_error, 'message': 'Read timeout occurred'}
        except requests.exceptions.ConnectTimeout:
            self.log.error('{' + '"message":"Oops. Connection timeout occurred!')
            self.proxy_error += 1
            return {'body': '', 'code': 500, 'not-found': False, 'error': True, 'unique_key': unique_key,
                    'proxy_error_counter': self.proxy_error, 'message': 'Connection timeout occurred'}
        except requests.exceptions.RequestException as e:
            self.log.error(
                '{' + '"message":"Oops. Retry error timeout occurred! {}",'.format(
                    e) + '"message_tag":"parsing-fetch-url-error"' + '}')
            self.proxy_error += 1
            return {'body': '', 'code': 500, 'not-found': False, 'error': True, 'unique_key': unique_key,
                    'proxy_error_counter': self.proxy_error, 'message': e}
        except Exception as e:
            self.log.error(
                '{' + '"message":"fetch_url error {}",'.format(e) + '"message_tag":"parsing-fetch-url-error"' + '}')
            return {'body': '', 'code': 500, 'not-found': False, 'error': True, 'unique_key': unique_key,
                    'proxy_error_counter': self.proxy_error, 'message': 'All Exception'}

        if r.status_code == 429:
            return {'body': '', 'code': r.status_code, 'not-found': False, 'error': True, 'unique_key': unique_key}
        elif r.status_code == 404:
            return {'body': '', 'code': r.status_code, 'not-found': True, 'error': True, 'unique_key': unique_key}
        elif r.status_code != 200:
            return {'body': '', 'code': r.status_code, 'unique_key': unique_key}
        else:
            try:
                return {'body': r.content, 'code': r.status_code, 'unique_key': unique_key}
            except Exception as e:
                self.log.error(e)
                pass

    # -----------------------------------------   Extract JSON data from html ----------------------------------------

    def fetch_json_from_html(self, html, unique_key='', func_name=''):
        target = None
        try:
            target = \
                html.decode('utf-8').split('<script type="text/javascript">window._sharedData = ')[1].split(
                    ';</script>')[
                    0]
        except Exception as e:
            self.log.exception(
                'fetch_json_from_html error for <scrpit></script> by func {} and unuque key {}, content {}'.format(
                    func_name, unique_key, html))
            quit()

        # Check if getting json
        try:
            if target:
                json.loads(target)
            else:
                self.log.error({
                    "message": "fetch_json_from_html error - it's not json",
                    "token": self.token,
                    "message_tag": '{}.{}'.format('parsing', 'fetch_json_from_html')
                })
                quit()
        except Exception as e:
            self.log.exception("fetch_json_from_html error - it's not json")
            quit()

        return target

    # -----------------------------------------   Get short header (without cookie) -----------------------------------

    def get_short_header(self, user_agent):
        header = {
            'Content-Type': 'html/text; charset=utf-8',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,' + \
                      'application/signed-exchange;v=b3',
            'accept-language': 'en-US;q=0.8,en;q=0.7,cs;q=0.6,da;q=0.5,de;q=0.4,es;q=0.3,it;q=0.2,pt;' + \
                               'q=0.1,zh-CN;q=0.1,zh;q=0.1',
            'referer': 'https://www.instagram.com/',
            'user-agent': user_agent,
        }

        # self.log.debug('{' + '"message":"Header {}","message_tag":"browser-settings"'.format(header) + '}')

        return header

    def get_full_header(self, user_agent, gis, cookie='', csrf_token=''):
        return {
            'X-Instagram-GIS': gis,
            'X-Requested-With': 'XMLHttpRequest',
            'x-csrftoken': csrf_token,
            'user-agent': user_agent
        }

    def get_info_header(self, user_agent, cookie='', csrf_token=''):
        header = {
            'accept-language': 'en-US;q=0.8,en;q=0.7,cs;q=0.6,da;q=0.5,de;q=0.4,es;q=0.3,it;q=0.2,pt;q=0.1,zh-CN;q=0.1,zh;q=0.1',
            'accept-charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.7',
            'cookie': cookie,
            'referer': 'https://i.instagram.com',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
            'user-agent': user_agent,
            'x-csrftoken': csrf_token
        }

        self.log.debug('{' + '"message":"Header {}","message_tag":"browser-settings"'.format(header) + '}')

        return header

    def get_profile_data_from_json(self, json, fields):
        result = []
        return result

    def get_video_details(self, json):
        result = []
        return result

    def get_comments(self):
        result = []
        return result

    def get_query_posts_variable(self, account_id, limit, cursor):
        return '{' + '"id":"{}","first":"{}","after":"{}"'.format(account_id, limit, cursor) + '}'

    def get_query_comments_variable(self, shortcode, limit, cursor):
        return '{' + '"shortcode":"{}","first":"{}","after":"{}"'.format(shortcode, limit, cursor) + '}'

    def get_query_hash(self, html, container, user_agent):

        # pattern = '/\/static\/bundles\/metro\/' + str(container) + '\.js\/(.*?)\.js/'
        pattern = '/bundles/metro/' + str(container) + '\.js\/(.*?)\.js'

        if not isinstance(html, str):
            html = html.decode('utf-8')

        matches = re.findall(pattern, html)

        if not matches:
            return ''
            # raise Exception('Matches not found')

        url = 'https://www.instagram.com/static/bundles/metro/' + container + '.js/' + str(matches[0]) + '.js'
        response = self.fetch_url([1, url, '', user_agent])

        if container == 'PostPageContainer':
            pattern = '\(n\)\.pagination\}\,queryId\:\"(.*?)\"\,queryParams'
            # pattern = 'o\.pagination\}\,queryId\:\"(.*?)\"\,queryParams'

        if container == 'ProfilePageContainer':
            pattern = 's\.pagination\}\,queryId\:\"(.*?)\"\,queryParams'

        if isinstance(response['body'], (bytes, bytearray)):
            matches = re.findall(pattern, response['body'].decode('utf-8'))
        else:
            self.log.error('Html answer not bytes')
            quit()

        if not matches:
            raise Exception('Matches not found')

        return matches[0]

    def get_caption_users(self, text):
        pattern = '\@[a-zA-Z0-9_]{0,30}'
        matches = re.findall(pattern, text)

        result = []
        if matches:
            for item in matches:
                result.append(str(item).replace('@', '').lower())

        return result

    def cookie(self, data, sequence):
        cookie = {
            'rur': data['rur'] if 'rur' in data else '',
            'mid': data['mid'] if 'mid' in data else '',  # not changes for different requests
            'csrftoken': data['csrftoken'] if 'csrftoken' in data else '',
            'ds_user_id': data['ds_user_id'],
            'sessionid': data['sessionid'],
            'urlgen': data['urlgen'] if 'urlgen' in data else '',
        }

        self.log.debug('{' + '"message":"Cookie {}","message_tag":""'.format(cookie) + '}')
        cookie = ''.join([k + '=' + cookie[k] + '; ' for k in sequence])[: -2]

        return cookie
