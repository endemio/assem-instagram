import os
import time
import json
import random

from random import choice
from string import ascii_lowercase, ascii_letters

from InstagramFakeAPI import *
import imageio

imageio.plugins.ffmpeg.download()


class PrivateAPI:

    def __init__(self, logging, username, password, proxy, debug=False):
        self.logging = logging
        self.username = username
        self.password = password
        self.api = None
        self.foo = False
        self.token = None
        self.debug = debug
        self.local_store_folder = None
        self.local_store_status = None

        if not proxy:
            self.proxy_status = False
            self.proxy = ''
        else:
            self.proxy_status = True
            self.proxy = proxy
        self.logging.debug('{' + '"message":"Set login={}, password={},proxy={}","message_tag":""'.format(
            self.username, self.password, self.proxy) + '}')

    def connect(self):
        self.api = InstagramFakeAPI(self.username, self.password)
        time.sleep(3.5)
        if self.proxy_status:
            self.api.setProxy(proxy=self.proxy)
        status = self.api.login()
        if status:
            self.api.getSelfUserFeed()  # get self user feed
            self.logging.debug("Login success!")
        else:
            raise Exception(self.api.LastJson)

    def foo_connect(self):
        self.api = None
        self.foo = True

    def config_set_local_storing(self, status, folder=''):
        self.local_store_folder = folder
        self.local_store_status = status
        try:
            if not os.path.exists(folder):
                os.makedirs(folder)
        except Exception as e:
            self.logging.exception(e)

    #
    # -------------------------- get folloWERS ----------------------------------------
    #

    def get_total_followers(self, username, start, limit, next_max_id='', number_followers=0):
        followers = []
        errors = []

        if self.foo:
            self.logging.debug({
                'message': 'Start foo version of followers scraping',
                'username': username,
                'token': self.token,
                'message_tag': '{}.{}'.format('privateAPI.get_followers', '')
            })

            [followers, next_max_id] = self.foo_get_total_followers(limit)

            self.logging.debug({
                'message': 'Summary get {} followers, next_max_id = {}'.format(len(followers), next_max_id),
                'username': username,
                'token': self.token,
                'message_tag': '{}.{}'.format('privateAPI.get_followers', '')
            })
            time.sleep(5)
            if next_max_id:
                return {'followers': followers, 'error': [], 'next_max_id': next_max_id}
            else:
                return {'followers': followers, 'error': []}

        self.api.searchUsername(username)

        try:
            jz = self.api.LastJson
        except Exception as e:
            self.logging.error('Json parsing error in get_total_followers ' + str(e))
            errors.append('json_error_in_followings_parsing')
            return {'followers': [], 'error': errors}

        if jz['user']['is_private']:
            errors.append('user_is_private')
            return {'followers': [], 'error': errors}

        try:
            user_id = jz['user']['pk']
        except Exception as e:
            self.logging.error('User not found in get_total_followers ' + str(e))
            errors.append('not_found_user_error_in_followings_parsing')
            return {'followers': [], 'error': errors}

        follower_count = jz['user']['follower_count']
        if not follower_count:
            self.logging.error({
                'message': "Number of followers is 0",
                'username': username,
                'token': self.token,
                'message_tag': '{}.{}'.format('privateAPI.get_followers', 'error')
            })

            errors.append('followers number is 0')
            return {'followers': [], 'error': errors}

        self.logging.debug({
            'message': "Number of followers = {}".format(follower_count),
            'username': username,
            'token': self.token,
            'message_tag': '{}.{}'.format('privateAPI.get_followers', '')
        })

        time.sleep(1)
        cnt = limit if limit > 0 else follower_count
        cnt = cnt if follower_count > cnt else follower_count
        # Remove number of already exists followers
        cnt -= number_followers

        # next_max_id = next_max_id if next_max_id else ''
        next_round = True

        itteration = 0

        while next_round:

            itteration += 1

            self.logging.debug({
                'message': 'Iterration is {}'.format(itteration),
                'username': username,
                'token': self.token,
                'message_tag': '{}.{}'.format('privateAPI.get_followers', '')
            })

            request_error = True
            request_error_counter = 0
            js = {}

            while request_error:

                try:
                    self.logging.debug({
                        'message': 'Start getting data from IG for user_id {} with next_max_id {}'.format(user_id,
                                                                                                          next_max_id),
                        'username': username,
                        'token': self.token,
                        'message_tag': '{}.{}'.format('privateAPI.get_followers', '')
                    })

                    _ = self.api.getUserFollowers(user_id, maxid=next_max_id)
                    js = self.api.LastJson
                    next_max_id = self.api.LastJson.get('next_max_id', '')
                    request_error = False

                    self.logging.debug({
                        'message': 'Next_max_id {}'.format(next_max_id),
                        'username': username,
                        'token': self.token,
                        'message_tag': '{}.{}'.format('privateAPI.get_followers', '')
                    })

                except Exception as e:
                    self.logging.exception('{' + '"message":"Getting data {} error ","message_tag":""'.format(e) + '}')
                    request_error_counter = request_error_counter + 1
                    if request_error_counter > 5:
                        return {'followers': followers, 'error': errors, 'next_max_id': next_max_id}

            if 'users' in js:
                self.logging.debug({
                    'message': 'Numbers of followers in round {}'.format(len(js['users'])),
                    'username': username,
                    'token': self.token,
                    'message_tag': '{}.{}'.format('privateAPI.get_followers', '')
                })

                for i in range(len(js['users'])):
                    try:
                        followers.append({'u': js['users'][i]['username'], 'id': js['users'][i]['pk']})
                        if cnt <= len(followers):
                            self.logging.debug({
                                "message": "Stop scraping during reaching limit in {}".format(limit),
                                'username': username,
                                'token': self.token,
                                'message_tag': '{}.{}'.format('privateAPI.get_followers', '')
                            })

                            return {'followers': followers, 'error': []}
                    except Exception as e:
                        self.logging.exception('Parsing error %s', e)
                        errors.append('Getting data error = ' + str(e))
                        return {'followers': followers, 'error': errors, 'next_max_id': next_max_id}
                time.sleep(1.75)
                next_round = True if next_max_id else False

                self.logging.debug({
                    'message': "Get {} followers from account, len next_max_id {}, itteration = {}, next_round {}".format(
                        len(followers),
                        next_max_id, itteration, next_round),
                    'username': username,
                    'token': self.token,
                    'message_tag': '{}.{}'.format('privateAPI.get_followers', '')
                })

                if itteration > 300:
                    self.logging.debug({
                        'message': 'Reach limit per itteration. Stop scraping',
                        'username': username,
                        'token': self.token,
                        'message_tag': '{}.{}'.format('privateAPI.get_followers', '')
                    })
                    return {'followers': followers, 'error': [], 'next_max_id': next_max_id}
            else:
                self.logging.debug({
                    'message': '"users" not found in js :(. {}'.format(js),
                    'username': username,
                    'token': self.token,
                    'message_tag': '{}.{}'.format('privateAPI.get_followers', '')
                })

        self.logging.debug({
            'message': "Scraping followers is complete {} followers".format(len(followers)),
            'username': username,
            'token': self.token,
            'message_tag': '{}.{}'.format('privateAPI.get_followers', '')
        })

        return {'followers': followers, 'error': []}

    #
    # -------------------------- get followINGS ----------------------------------------
    #

    def get_total_followings(self, target, start, limit):
        followings = []
        errors = []
        self.api.searchUsername(target)

        try:
            jz = self.api.LastJson
        except Exception as e:
            self.logging.error('Json parsing error in get_total_followings ' + str(e))
            errors.append('json_error_in_followings_parsing')
            return {'followings': [], 'error': errors}

        if jz['user']['is_private']:
            errors.append('user_is_private')
            return {'followings': [], 'error': errors}

        try:
            user_id = jz['user']['pk']
        except Exception as e:
            self.logging.error('User not found in get_total_followings ' + str(e))
            errors.append('not_found_user_error_in_followings_parsing')
            return {'followings': [], 'error': errors}

        followings_count = jz['user']['following_count']

        self.logging.debug('For user %s followings number = %s', target, followings_count)

        if not followings_count:
            self.logging.error('Followings number is 0')
            errors.append('followings number is 0')
            return {'followings': followings, 'error': None}

        time.sleep(1)
        cnt = limit if limit > 0 else followings_count
        cnt = cnt if followings_count > cnt else followings_count

        next_max_id = True
        while next_max_id:
            if next_max_id is True:
                next_max_id = ''
            _ = self.api.getUserFollowings(user_id, maxid=next_max_id)
            js = self.api.LastJson
            for i in range(len(js['users'])):
                try:
                    followings.append({'u': js['users'][i]['username'], 'id': js['users'][i]['pk']})
                    if cnt <= len(followings):
                        return {'followings': followings, 'error': None}
                except:
                    pass
            time.sleep(1.75)
            self.logging.debug('Current number of followings = %s', len(followings))
            next_max_id = self.api.LastJson.get('next_max_id', '')
        return {'followings': followings, 'error': None}

    @staticmethod
    def foo_get_random_username(min_length=8, max_length=20):
        return "".join(choice(ascii_lowercase) for i in range(random.randint(min_length, max_length)))

    def foo_get_total_followers(self, limit):

        error = True

        stringLength = 70

        followers = []

        next_max_id = ''

        while error:

            n_generate = random.randint(180, 200)

            for i in range(n_generate):
                random_username = self.foo_get_random_username()
                random_user_id = random.randint(1_000_000, 16_000_000_000)
                followers.append({'u': random_username, 'id': random_user_id})

            next_max_id = ''.join(random.choice(ascii_letters) for i in range(stringLength))

            error = random.randint(0, 5)
            self.logging.debug(
                'Number of followers is {}, next_max_id {} next round {}'.format(len(followers), next_max_id,
                                                                                 bool(error)))

            time.sleep(1.75)

        next_max_id = next_max_id if random.randint(0, 1) else ''
        return [followers, next_max_id]
