import boto3
from botocore.exceptions import ClientError

class S3:

    def __init__(self,logging, id, secret):
        self.logging = logging
        self.id = id
        self.secret = secret
        self.s3_client = None
        self.s3_resource = None

    def connect_client(self):
        self.s3_client = boto3.client(
            's3',
            aws_access_key_id=self.id,
            aws_secret_access_key=self.secret
        )

    def connect_resource(self):
        self.s3_resource = boto3.resource(
            's3',
            aws_access_key_id=self.id,
            aws_secret_access_key=self.secret
        )

    def upload_file(self,path,bucket,filename_is_s3,content_type = "application/json"):
        try:
            self.s3_client.upload_file(path, bucket, filename_is_s3, ExtraArgs={'ContentType': content_type},)
            return True
        except Exception as e:
            self.logging.error(e)
            raise

    def delete_file(self,bucket,filename_is_s3):
        try:
            self.s3_resource.Object(bucket,filename_is_s3).delete()
            return True
        except Exception as e:
            self.logging.error(e)
            raise


    def check_file_exist(self, bucket, filename_is_s3):
        try:
            self.s3_client.head_object(Bucket=bucket, Key=filename_is_s3)
        except ClientError as e:
            return int(e.response['Error']['Code']) != 404
        return True

    def get_object_content(self,bucket,filename_in_s3):
        try:
            result = self.s3_client.get_object(Bucket=bucket, Key=filename_in_s3)
            return result['Body'].read().decode('utf-8')
        except Exception as e:
            self.logging.error('s3 get_object_content error {}'.format(e))
            raise

    def get_key_option(self,bucket,filename_in_s3):
        file_props = self.s3_client.head_object(Key=filename_in_s3, Bucket=bucket)
        #modified_stamp = time.mktime(file_props.get("LastModified").timetuple())
        #os.utime(dest_path, (modified_stamp, modified_stamp))

        return file_props