from minio import Minio
from minio.error import ResponseError
from minio.error import NoSuchKey

class MinioClass:

    def __init__(self,logging, host, id, secret):
        self.logging = logging
        self.host = host
        self.id = id
        self.secret = secret
        self.minio_client = None
        self.s3_resource = None

    def connect_client(self):
        self.minio_client = Minio(
            self.host,
            access_key=self.id,
            secret_key=self.secret,
            secure=False
        )

    def upload_file(self,path,bucket,filename_is_s3,content_type = "application/json"):
        try:
            self.minio_client.fput_object(bucket, filename_is_s3, path,content_type=content_type)
            return True
        except ResponseError as err:
            self.logging.exception(err)
            raise

    def delete_file(self,bucket,filename_is_s3):
        try:
            self.minio_client.remove_object(bucket,filename_is_s3).delete()
            return True
        except ResponseError as err:
            self.logging.exception(err)
            raise


    def check_file_exist(self, bucket, filename_is_s3):
        try:
            result = self.minio_client.stat_object(bucket, filename_is_s3)
            return True
        except NoSuchKey:
            return False
        except ResponseError as err:
            self.logging.exception(err)
        return False

    def get_object_content(self,bucket,filename_in_s3):
        self.logging.debug('Get {} from bucket {}'.format(filename_in_s3,bucket))
        try:
            result = self.minio_client.get_object(bucket, filename_in_s3)
            return result.read().decode('utf-8')
        except ResponseError as err:
            self.logging.exception(err)
        except Exception as e:
            self.logging.error('s3 get_object_content error {}'.format(e))
            raise