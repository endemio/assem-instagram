import pymysql.cursors
import json
from pymysql.err import IntegrityError


class MysqlORM:

    def __init__(self, logging, debug = False):
        self.logging = logging
        self.debug = debug
        self.mysql_connection = None

    def close(self):
        self.mysql_connection.close()

    def config_set_mysql(self, status, host='', port='', user='', password='', db_name=''):
        if status:
            self.mysql_connection = pymysql.connect(
                host=host,
                port=port,
                user=user,
                password=password,
                db=db_name,
                charset='utf8mb4',
                cursorclass=pymysql.cursors.DictCursor)

    def insert_into_table_on_duplicate_ignore(self, table, data, insert_keys):
        export_keys = list(insert_keys.keys())
        export_fields = list(insert_keys.values())

        temp = []
        values = []
        for elem in export_keys:
            if isinstance(data[elem], int):
                temp.append('%s')
                values.append(int(data[elem]))
            else:
                temp.append('%s')
                values.append(str(data[elem]))

        query = 'INSERT INTO '+str(table)+' ('+self.create_list_of_keys(export_fields)+') VALUES ('+self.create_list_of_keys(temp)+') ON '+\
            ' DUPLICATE KEY UPDATE id=VALUES(id)'

        self.logging.debug('Query {}, values {}'.format(query,values))

        with self.mysql_connection.cursor() as cursor:
            try:
                cursor.execute(query, values)
                self.logging.debug('Query execute')
            except IntegrityError as err:
                errnum, errmsg = err.args
                if errnum == 1062:
                    print(err)
                else:
                    raise IntegrityError(errnum, errmsg)
        self.mysql_connection.commit()

        return True

    def insert_into_table_increase_field(self, table, data, field, direction='+'):
        export_keys = list(data.keys())

        temp = []
        values = []
        for elem in export_keys:
            if isinstance(data[elem], int):
                temp.append('%s')
                values.append(int(data[elem]))
            else:
                temp.append('%s')
                values.append(str(data[elem]))

        query = 'INSERT INTO ' + str(table) + ' (' + self.create_list_of_keys(export_keys) + ') VALUES (' \
                + self.create_list_of_values(
            temp) + ') ON DUPLICATE KEY UPDATE ' + field + ' = ' + field + direction + '1'
        self.logging.debug(query)
        self.logging.debug(values)

        with self.mysql_connection.cursor() as cursor:
            try:
                cursor.execute(query, values)
            except IntegrityError as err:
                errnum, errmsg = err.args
                if errnum == 1062:
                    print(err)
                else:
                    raise IntegrityError(errnum, errmsg)
        self.mysql_connection.commit()

        return True

    # SQL query example: SELECT * FROM {table} WHERE {field} IN ({list(data)} Order By {order} {direction} Limit {limit})
    # table - table name
    # data - dict
    # order - field name to order (string)
    # direction - ASC/DESC
    # limit - limit in format string 'start,number'
    #
    def get_row_form_table_by_field_array(self, table, data, field,order=None,direction='ASC',limit=None):
        result = []
        format_strings = ','.join(['%s'] * len(data))
        query = 'SELECT * FROM '+str(table)+' WHERE '+str(field)+' in (%s) ' % format_strings

        if order:
            query += ' ORDER BY {} {}'.format(order,direction)

        if limit:
            query += ' LIMIT {}'.format(limit)

        with self.mysql_connection.cursor() as cursor:
            try:
                cursor.execute(query, tuple(data))
                result = cursor.fetchall()
            except IntegrityError as err:
                self.logging.exception(err)
        self.mysql_connection.commit()
        return result

    # SQL query example: SELECT * FROM {table} WHERE {field} > (value} Order By {order} {direction} Limit {limit})
    # table - table name
    # data - dict
    # order - field name to order (string)
    # direction - ASC/DESC
    # limit - limit in format string 'start,number'
    #
    def get_row_form_table_greater_then_field_array(self, table, value, field,order=None,direction='ASC',limit=None):
        result = []
        query = 'SELECT * FROM '+str(table)+' WHERE '+str(field)+' > (%s) '

        if order:
            query += ' ORDER BY {} {}'.format(order,direction)

        if limit:
            query += ' LIMIT {}'.format(limit)

        with self.mysql_connection.cursor() as cursor:
            try:
                cursor.execute(query, value)
                result = cursor.fetchall()
            except IntegrityError as err:
                self.logging(err)
        self.mysql_connection.commit()
        return result


    def update_table_all_values_by_key(self, table, data, insert_keys):
        export_keys = list(insert_keys.keys())

        temp = []
        values = []
        keys = []

        for elem in export_keys:
            if elem in data:
                if isinstance(data[elem], bool):
                    temp.append('%s')
                    values.append(bool(data[elem]))
                    keys.append(str(insert_keys[elem]))
                elif isinstance(data[elem], dict):
                    temp.append('%s')
                    values.append(str(json.dumps(data[elem])))
                    keys.append(str(insert_keys[elem]))
                else:
                    temp.append('%s')
                    values.append(str(data[elem]))
                    keys.append(str(insert_keys[elem]))

        query = 'INSERT INTO '+str(table)+' ('+self.create_list_of_keys(keys)+') VALUES ('\
                +self.create_list_of_values(temp)+') ON DUPLICATE KEY UPDATE '+ \
                ', '.join(['{1}=values({1})'.format(value, value) for (value) in keys])

        if self.debug: self.logging.debug(query)
        if self.debug: self.logging.debug(values)

        with self.mysql_connection.cursor() as cursor:
            try:
                cursor.execute(query, values)
                if self.debug:
                    self.logging.debug('Added (update table)')
            except IntegrityError as err:
                errnum, errmsg = err.args
                if errnum == 1062:
                    print(err)
                raise IntegrityError(errnum, errmsg)
        self.mysql_connection.commit()


    def create_list_of_keys(self, list):
        string_value = ','.join(str(x) for x in list)
        return string_value

    def create_list_of_values(self, list):
        string_value = ','.join(str(x) for x in list)
        return string_value

    def delete_from_table(self, table, field, value):
        query = 'DELETE FROM '+str(table)+' WHERE '+str(field)+' = %s'
        with self.mysql_connection.cursor() as cursor:
            try:
                cursor.execute(query, value)
            except IntegrityError as err:
                self.logging(err)
        self.mysql_connection.commit()
        return True
