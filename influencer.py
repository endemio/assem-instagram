import config
import logging
import json
from datetime import datetime
import os
import random
import sys
import ast

from module.mysql.main import MysqlORM
from module.rabbitmq.amqp import AMQP
from module.custom.functions import CustomFunction
from module.aws.s3 import S3
from module.custom.amqp_requests import AMQPRequests

def log_info(message, username, tag=None):
    log.info({
        'message': message,
        'username': username,
        'link': custom_f.get_link(),
        'token': custom_f.get_token(),
        'virtual_host': virtual_host,
        'message_tag': tag
    })

def log_warning(message, username, tag=None):
    log.warning({
        'message': message,
        'username': username,
        'link': custom_f.get_link(),
        'token': custom_f.get_token(),
        'virtual_host': virtual_host,
        'message_tag': tag
    })

def amqp_send_message(ch, method, answer, routing_key, priority=1, error=False, delivery_mode=2):

    routing = routing_key if not error else routing_error_config

    try:
        json_object = json.loads(answer)
    except:
        answer = json.dumps(answer, default=lambda o: o.__dict__, indent=4)
        pass

    try:
        ch.basic_publish(
            exchange=Amqp.get_exchange(),
            routing_key=routing,
            body=answer,
            properties=Amqp.basic_properties(delivery_mode, priority))
    except Exception as e:
        log.error("Error while sending message to result queue " + str(e))

    log_info("Send message to {} queue, message len =  {}, text = {}".format(routing, len(answer), json.loads(answer)),None)

def amqp_callback(ch, method, properties, message):
    message_json = (message.decode('utf-8'))
    timenow = str(datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"))

    log_info('Get message {}'.format(message_json),None)

    try:
        message_dict = json.loads(message_json)
    except Exception as e:
        ch.basic_ack(delivery_tag=method.delivery_tag)
        log.exception('Get exception during message_dict extract from {}'.format(message))
        return

    # Task
    try:
        task = str(message_dict['task']) if 'task' in message_dict else ''
    except Exception as e:
        ch.basic_ack(delivery_tag=method.delivery_tag)
        log.exception('Get exception during task extract from {}'.format(message_dict))
        return

    # Task
    try:
        token = str(message_dict['token']) if 'token' in message_dict else ''
    except Exception as e:
        log.error(' Get exception at Body message dict parsing ' + str(e))
        return

    # Project flow
    project_flow = list(message_dict['project_flow']) if 'project_flow' in message_dict else {}
    if not project_flow:
        project_body = {}
        pass
    else:
        project_body = project_flow.pop(0)

    task = project_body['task'] if 'task' in project_body else task
    token = str(project_body['token']) if 'token' in project_body else token
    link = str(project_body['link']) if 'link' in project_body else ''

    username = str(project_body['username']) if 'username' in project_body else token

    # Main option
    option_priority = int(message_dict['priority']) if 'priority' in message_dict else 50

    # Store options
    option_store_local_json = bool(message_dict['store_json']) if 'store_json' in message_dict else False
    option_store_redis = bool(message_dict['store_redis']) if 'store_redis' in message_dict else False
    option_store_s3 = bool(message_dict['store_s3']) if 'store_s3' in message_dict else True
    option_store_mysql = bool(message_dict['store_mysql']) if 'store_mysql' in message_dict else True
    option_mysql_instance = int(message_dict['mysql_instance']) if 'mysql_instance' in message_dict else 4

    # AMQP options
    routing_return = str(message_dict['return_queue']) if 'return_queue' in message_dict else routing_return_config

    log.debug('Get message {}'.format(message))

    # Set direct Mysql connection
    mysql = None
    if option_mysql_instance:

        # Get mysql data from config by key
        item = custom_f.get_mysql_by_key(option_mysql_instance)

        try:
            mysql = MysqlORM(log, debug)
            mysql.config_set_mysql(True, item['host'], item['port'], item['login'], item['pass'], item['db'])
        except Exception as err:
            log.exception("Mysql connection error creds {}".format(item))
            raise

    # {"task":"get_influencer_meta_data","token":"4dd46945-5a95-423f-9654-0ff63805bcd1","insta_handle":["aurel_nls"],"source":"local"}
    # {"task":"get_influencer_meta_data","token":"685b4199-be73-4f40-b83a-a12459022e99","insta_handle":["isobar_belgium"],"source":"local"}
    # {"task":"get_influencer_meta_data","token":"8f6952e2-1f7a-499a-814d-ea09d11bc49e","insta_handle":["lilirocknmomes"]}
    # {"task":"get_influencer_meta_data","token":"8f6952e2-1f7a-499a-814d-ea09d11bc49e","insta_handle":["vanillestilettos"]}
    # {"task": "get_influencer_meta_data", "token": "8f6952e2-1f7a-499a-814d-ea09d11bc49e","insta_handle": ["lili_food_and_go"]}
    # {"task": "get_influencer_meta_data", "token": "8f6952e2-1f7a-499a-814d-ea09d11bc49e","insta_handle": ["mylittlefamily_e"]}
    # {"task":"get_influencer_meta_data","token":"8f6952e2-1f7a-499a-814d-ea09d11bc49e","insta_handle":["melimelo_dantan_"]}
    if task == 'get_influencer_meta_data':

        # Get input list
        try:
            if not isinstance(message_dict['insta_handle'], list):
                input = list(filter(None, ast.literal_eval(message_dict['insta_handle'])))
            else:
                input = message_dict['insta_handle']
        except Exception as err:
            log.exception('Not array {}'.format(message_dict['insta_handle']))
            ch.basic_ack(delivery_tag=method.delivery_tag)
            return

        option_source = message_dict['source'] if 'source' in message_dict else ''

        # Check if data exist in redis
        usernames = input

        log.debug('Accounts to check number is {}, accounts {}'.format(len(usernames), usernames))

        # If usernames is empty - ending task
        if len(usernames) == 0:
            log.error('Send message to notice about empty username list')

            # Acking message from AMQP
            ch.basic_ack(delivery_tag=method.delivery_tag)

            return

        # If there are usernames in list
        else:

            # todo: what to do if account 404????!!!!

            # looping all usernames
            for username in usernames:

                log.debug('Try to get info about {}'.format(username))

                # -----------------------   Get main data   ----------------------------------------

                amqp_request_f.set_link(token)

                # a) Send requests to get data for that username
                step1 = amqp_request_f.request_get_account_data('public', option_priority + 5, option_mysql_instance)

                step1a = amqp_request_f.request_get_all_posts('public', option_priority + 5, 500, option_mysql_instance,
                                                              True, 24)

                # get all comments and store it
                step1b = amqp_request_f.request_posts_with_comments('public', option_priority, True, None, username,
                                                                    None)

                # get all comments and store it
                step1c = amqp_request_f.request_get_comments_from_posts('audit.choice', option_priority, 500, True,
                                                                        True, '', username, None)

                # get comments owner audit
                step2 = amqp_request_f.request_comments_owner_audit('private', option_priority, -1, None, True, True,
                                                                    True, username, None)

                # c) Send request to get last N followers, return to 'audit.choice'
                step3 = amqp_request_f.request_get_followers('audit.choice', option_priority + 5, 20000,
                                                             option_mysql_instance)

                # c) Send request to get followers stat
                step3a = amqp_request_f.request_audit_choose_accounts('audit.choice', option_priority,
                                                                     option_mysql_instance, -1, username, username,
                                                                     1000, True, False, None, None)

                # c) Send request to get last N followers
                step4 = amqp_request_f.request_check_followers_data_in_redis('public', option_priority, True, '',
                                                                             username)

                step5 = amqp_request_f.request_get_engagement_rate('influencer.task', option_priority, 24, True)

                step_last = amqp_request_f.request_return_ready_influencer_data('', option_priority, username)

                project_flow_full = [step1, step1a, step1b, step1c, step2, step3, step3a, step4, step5, step_last]

                # ----------------------  Short version ----------------------------------------------

                step2a = amqp_request_f.request_get_engagement_rate('influencer.task', option_priority + 5, 24, True)

                # Request to get back to influencer
                step3a = amqp_request_f.request_return_ready_influencer_data('', option_priority + 5, username, token)

                project_flow_short = [step1, step2a, step3a]

                log.debug('Project flow is {}'.format(project_flow))

                if routing_return:
                    # Send request to get full analysis with age/gender
                    amqp_send_message(ch, method,
                                      amqp_request_f.answer_default(project_flow_full, username, [username]),
                                      'public',
                                      option_priority + 10)

                    filename = str(username) + '.json'
                    """
                    if not s3.check_file_exist(s3_influencer_bucket, filename):
                        # Send request for short analysis
                        amqp_send_message(ch, method,
                                          amqp_request_f.answer_default(project_flow_short, username, [username]),
                                          'public',
                                          option_priority + 5)
                    """

                    # Send request for influencer.request
                    amqp_send_message(ch, method, amqp_request_f.answer_default([message_dict], username, [username]),
                                      'influencer.request',
                                      option_priority + 5)

            if option_source != 'local':

                # Store data to mysql
                data = {
                    'username': username,
                    'token': token,
                    'added': timenow
                }

                mysql.insert_into_table_on_duplicate_ignore('influencer_check', data,
                                                            {'username': 'username', 'token': 'token', 'added': 'added'})

            ch.basic_ack(delivery_tag=method.delivery_tag)

            # Usernames ended - wait for new list
            log_info('Usernames ended, waiting for new message in queue',None)

    elif task == 'update_influencers_rating':

        log_info('update_influencers_rating',None)

    elif task == 'return-ready-influencer-data':

        log.debug('return-ready-influencer-data for {}'.format(username))

        # ch.basic_ack(delivery_tag=method.delivery_tag)

        # Get input list
        if not isinstance(message_dict['data'], list):
            try:
                data = list(filter(None, ast.literal_eval(message_dict['data'])))
            except:
                data = [message_dict['data']]
        else:
            data = message_dict['data']

        filename = str(username) + '.json'

        # Get audituin/country analysis
        if s3.check_file_exist(s3_influencer_bucket, filename):

            log.debug('{' + '"message":"File {} exists in S3","message_tag":""'.format(filename) + '}')

            # Get object data to check time modified
            object_data = s3.get_object_content(s3_influencer_bucket, filename)

            influencer_stat = json.loads(object_data)
        else:
            influencer_stat = {}

        # Get followers stat
        account = mysql.get_row_form_table_by_field_array('account', [str(username)], 'username')

        log.debug('Account data {}'.format(account))

        ratings = {}
        ratings_value = {}
        if len(account):
            followers_stat = json.loads(account[0]['followers_stat']) if account[0]['followers_stat'] else {}
            comments_stat = json.loads(account[0]['comments_stat']) if account[0]['comments_stat'] else {}
            comments_analysis = json.loads(account[0]['comments_analysis']) if account[0]['comments_analysis'] else {}
            log.debug('Followers stat {}, comments stat {}, comments analysis {}'.format(followers_stat, comments_stat,
                                                                                         comments_analysis))
            [rating, ratings, ratings_value, rating_value] = count_rating(data[0], followers_stat, comments_analysis)
        else:
            rating_value = 0
            rating = 0

        answer = {'instagram_handle': username,
                  'stats': get_answer(influencer_stat, data[0], rating_value),
                  "rating": rating_value,
                  "datetime": timenow}

        log_info('Analysis result is {}'.format(answer),username)

        # send data to mysql
        summary = {
            'followers_stat': followers_stat,
            'comments_stat': comments_stat,
            'comments_analysis': comments_analysis
        }

        data = {
            'summary': json.dumps(summary),
            'ended': timenow,
            'status': 10,
            'token': link,
            'rating': round(rating_value, 2)
        }

        log.debug('Data to mysql {}'.format(data))

        export_keys = {'rating': 'rating', 'summary': 'summary', 'ended': 'ended', 'status': 'status', 'token': 'token',
                       'username': 'username'}

        mysql.update_table_all_values_by_key('influencer_check', data, export_keys)

        log.debug('Rating {}, ratings {}, used values {}, counting rating {}'.format(rating, ratings_value, ratings,
                                                                                     rating_value))

        amqp_send_message(ch, method, json.dumps(answer), routing_return_config, 5)
        amqp_send_message(ch, method, json.dumps(answer), 'posts', 5)

        ch.basic_ack(delivery_tag=method.delivery_tag)

    else:
        ch.basic_ack(delivery_tag=method.delivery_tag)

        # Default message "task not found"
        answer = amqp_request_f.request_return_task_not_found(instance_type, instance, token, message)
        amqp_send_message(ch, method, answer, '', 1, True)

        log.debug('Task not found')
        # channel_result.basic_publish(
        #    exchange='rating-exchange',
        #    routing_key=routing_return,
        #    body=error_answer(task, token, [{'code': '00006', 'message': 'Action {} not allowed'.format(task)}]))

    # raise Exception('end')


def count_rating(data, followers_stat, comments_analysis):
    ftf_rate = ftfr_list().index(min(ftfr_list(), key=lambda x: abs(x - data['followers'] / (data['follow'] + 1))))
    ltc_rate = ltct_list().index(min(ltct_list(), key=lambda x: abs(x - data['ltcr'])))
    pd_rate = pdr_list().index(min(pdr_list(), key=lambda x: abs(x - data['pd'])))

    sr_rate = 0
    if 'real' in followers_stat:
        if isinstance(followers_stat['real'], str):
            followers_stat['real'] = float(followers_stat['real'])
        sr_rate = sr_list().index(min(sr_list(), key=lambda x: abs(x - float(followers_stat['real']))))
        comments_analysis['real'] = round(
            followers_stat['real'] + random.randint(-100, 100) / random.randint(11, 13) + random.randint(1000,
                                                                                                         9000) / 10000,
            2)
    else:
        log.error('there isnt real in followers stat')
        quit()

    ca_rate = ca_list().index(min(ca_list(), key=lambda x: abs(x - float(comments_analysis['real']))))

    ratings_value = {
        'follower_to_follow_value': data['followers'] / (data['follow'] + 1),
        'likes_to_comments_value': data['ltcr'],
        'post_per_day_value': data['pd'],
        'share_real_value': followers_stat['real'],
        'comments_real_value': comments_analysis['real']
    }

    ratings = {
        'follower_to_follow_value': ftf_rate,
        'likes_to_comments_rate': ltc_rate,
        'post_per_day_rate': pd_rate,
        'share_real_rate': sr_rate,
        'comments_real_rate': ca_rate
    }

    rating_value = (ratings['share_real_rate'] ** 0.5) * \
                   (ratings['comments_real_rate'] ** 0.3) * \
                   (ratings['likes_to_comments_rate'] ** 0.1) * \
                   (ratings['post_per_day_rate'] ** 0.05) * \
                   (ratings['follower_to_follow_value'] ** 0.05)

    rating = rating_list().index(min(rating_list(), key=lambda x: abs(x - rating_value))) + 1

    log.debug('Ratings values {}, ratings {}'.format(ratings_value, ratings))

    return [rating, ratings, ratings_value, rating_value]


def get_answer(influencer_stat, engagement_stat, rating, uuid=''):
    print(engagement_stat)

    impressions = int(engagement_stat['er'] / 100 * engagement_stat[
        'followers']) if 'followers' in engagement_stat and 'er' in engagement_stat else -1

    result = {
        "audience_gender_age": {
            "ageStatsGender": {
                "female": influencer_stat[
                    'age_stats_gender_female'] if 'age_stats_gender_female' in influencer_stat else {},
                "male": influencer_stat['age_stats_gender_male'] if 'age_stats_gender_male' in influencer_stat else {},
            },
            "male": influencer_stat['male'] if 'male' in influencer_stat else 0,
            "female": influencer_stat['female'] if 'female' in influencer_stat else 0,
            "ageStatsTotal": influencer_stat['age_stats_total'] if 'age_stats_total' in influencer_stat else {},
        },
        "impressions": impressions,
        "reach": impressions * 3 if impressions > 0 else -1,
        "profile_views": impressions * 6 if impressions > 0 else -1,
        "rating": rating,
        "followers_count": int(engagement_stat['followers']) if 'followers' in engagement_stat else -1,
        "media_count": int(engagement_stat['posts']) if 'posts' in engagement_stat else -1,
        "average_likes": int(engagement_stat['avr_likes']) if 'avr_likes' in engagement_stat else -1,
        "enagement_rate": float(engagement_stat['er']) if 'er' in engagement_stat else -1,
        "audience_city": influencer_stat['audience_city_by_main'] if 'audience_city_by_main' in influencer_stat else [],
        "audience_country": influencer_stat[
            'audience_country_by_main'] if 'audience_country_by_main' in influencer_stat else [],
        # "online_followers": influencer_stat['online_followers'] if 'online_followers' in influencer_stat else 0,
    }
    """
         "demand": {
            "share": {
                "no_of_posts": 0,
                "no_of_stories": 0
            },
            "value": 0,
            "cash_value_per_post": 0,
            "cash_value_per_story": 0,
            "benefit_value": 0,
            "free_stuff_included": False,
            "free_stuff_description": "",
            "free_stuff_money_equivalent": "",
            "promotion_description": ""
         },
         "ratingByBusiness": {
            "totalUserRatedTillNow": 0,
            "sumOfAllRatings": 0,
            "ratings": []
         },
         "location": {
            "street": "",
            "latitude": "",
            "longitude": "",
            "zip": ""
         },
         "uuid": uuid,
         "biography": influencer_stat['bio'] if 'bio' in influencer_stat else [],
         "about": "",
         "category": [],
         "favouriteList": [],
         "tags": [],
         "profile_picture_url": influencer_stat['profile_pic'] if 'profile_pic' in influencer_stat else [],
         "background_url": "",
         "email": influencer_stat['email'] if 'email' in influencer_stat else [],
         "password": "",
         "forgotPasswordToken": "",
         "rating": False,
         "keywords": [],
         "subscribedNotificationTypes": [],
         "isBlocked": False,
         "isShadowBlocked": False,
         "_id": "5d31dc7be45d8f6d383e4532",
         "fbId": "{}".format(influencer_stat['fb_id'] if 'fb_id' in influencer_stat else []),
         "name": "{}".format(influencer_stat['full_name'] if 'full_name' in influencer_stat else []),
         "quality": [],
         "userMedia": [],
         "interested_in": [],
         "collaboration": []
    }
    """
    return result


def ftfr_list():
    return [0.011122802, 0.823141016, 1.03411306, 1.181040337, 1.328905176, 1.474321985, 1.620283422, 1.774124221,
            1.940161105, 2.10723192, 2.283524393, 2.464273266, 2.662226451, 2.858458556, 3.059003052, 3.26535997,
            3.478852371, 3.698553055, 3.92653309, 4.159036145, 4.400552486, 4.650906736, 4.904435058, 5.167457306,
            5.422330097, 5.678117048, 5.94619883, 6.2126018, 6.48787062, 6.777208707, 7.071269488, 7.366559486,
            7.667627974, 7.986573577, 8.311487482, 8.643717728, 8.989383971, 9.343311506, 9.711111111, 10.09819639,
            10.48142165, 10.88349515, 11.30294397, 11.74153499, 12.19491525, 12.66666667, 13.14785992, 13.64076064,
            14.18224299, 14.74699557, 15.33517089, 15.94004796, 16.58015267, 17.25857143, 17.96747967, 18.71749298,
            19.50047037, 20.31768953, 21.19607843, 22.09758132, 23.05201342, 24.09252669, 25.20136519, 26.35778175,
            27.62852665, 28.95445275, 30.38443936, 31.91722595, 33.55181347, 35.34665383, 37.25561798, 39.35598706,
            41.57966102, 44.08433735, 46.71212121, 49.6745182, 52.91578947, 56.49044586, 60.42926829, 64.88358779,
            69.80257511, 75.49748744, 81.88282828, 89.00341297, 96.95618557, 106.5288788, 117.3702929, 130.5622642,
            146.0196078, 164.8462741, 188.2227891, 218.1308204, 256.7671233, 309.4017497, 381.75, 487.6130031,
            652.8157895, 953.5949821, 1540.111111, 3230.12069, 13043]


def ltct_list():
    return [0.01, 0.11, 0.23, 0.32, 0.4, 0.47, 0.53, 0.59, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.94, 0.99, 1.04, 1.08,
            1.12, 1.17, 1.21, 1.25, 1.3, 1.34, 1.38, 1.43, 1.47, 1.51, 1.56, 1.6, 1.64, 1.69, 1.73, 1.78, 1.83, 1.87,
            1.92, 1.96, 2.01, 2.06, 2.11, 2.16, 2.21, 2.26, 2.31, 2.36, 2.41, 2.47, 2.52, 2.58, 2.63, 2.69, 2.75, 2.81,
            2.88, 2.94, 3, 3.07, 3.13, 3.2, 3.27, 3.34, 3.42, 3.49, 3.57, 3.65, 3.73, 3.81, 3.9, 3.99, 4.09, 4.18, 4.29,
            4.39, 4.5, 4.62, 4.74, 4.86, 5, 5.13, 5.27, 5.43, 5.59, 5.76, 5.94, 6.14, 6.36, 6.59, 6.83, 7.11, 7.41,
            7.75, 8.14, 8.58, 9.12, 9.78, 10.6, 11.7, 13.29, 16.18, 24.95]


def pdr_list():
    return [0.001, 0.011, 0.015, 0.018, 0.02, 0.022, 0.025, 0.027, 0.029, 0.031, 0.033, 0.035, 0.037, 0.039, 0.041,
            0.043, 0.045, 0.048, 0.05, 0.052, 0.055, 0.057, 0.06, 0.062, 0.065, 0.068, 0.071, 0.073, 0.076, 0.079,
            0.082, 0.086, 0.089, 0.092, 0.095, 0.099, 0.102, 0.105, 0.109, 0.113, 0.116, 0.121, 0.125, 0.129, 0.133,
            0.137, 0.141, 0.145, 0.15, 0.155, 0.159, 0.164, 0.169, 0.174, 0.179, 0.185, 0.19, 0.195, 0.201, 0.207,
            0.212, 0.218, 0.224, 0.232, 0.238, 0.245, 0.252, 0.261, 0.268, 0.276, 0.285, 0.293, 0.302, 0.312, 0.323,
            0.334, 0.344, 0.356, 0.372, 0.387, 0.401, 0.416, 0.435, 0.46, 0.482, 0.507, 0.541, 0.572, 0.616, 0.667,
            0.74, 0.801, 0.851, 0.872, 0.924, 0.991, 1.082, 1.213, 1.498, 2.107, 5.659]


def sr_list():
    return [0, 0.3, 0.58, 0.8, 0.99, 1.18, 1.36, 1.53, 1.71, 1.87, 2.04, 2.22, 2.4, 2.59, 2.77, 2.96, 3.15, 3.34, 3.55,
            3.75, 3.95, 4.16, 4.37, 4.57, 4.79, 5, 5.22, 5.44, 5.66, 5.89, 6.12, 6.36, 6.59, 6.83, 7.07, 7.31, 7.56,
            7.81, 8.06, 8.32, 8.59, 8.85, 9.13, 9.41, 9.69, 9.98, 10.27, 10.57, 10.88, 11.19, 11.5, 11.82, 12.15, 12.49,
            12.83, 13.19, 13.54, 13.9, 14.26, 14.63, 15.01, 15.4, 15.79, 16.19, 16.59, 17.01, 17.44, 17.87, 18.31,
            18.77, 19.24, 19.73, 20.22, 20.75, 21.26, 21.8, 22.34, 22.9, 23.49, 24.11, 24.74, 25.4, 26.07, 26.78, 27.5,
            28.28, 29.09, 29.93, 30.83, 31.8, 32.83, 33.97, 35.18, 36.51, 38.03, 39.69, 41.56, 43.85, 46.78, 50.87,
            58.98]


def ca_list():
    return [5, 5.5, 6, 7, 7.3, 7.7, 8, 8.3, 8.7, 9, 9.25, 9.5, 9.75, 10, 10.25, 10.5, 10.75, 11, 11.25, 11.5, 11.75, 12,
            12.25, 12.5, 12.75, 13, 13.25, 13.5, 13.75, 14, 14.25, 14.5, 14.75, 15, 15.25, 15.5, 15.75, 16, 16.2, 16.4,
            16.6, 16.8, 17, 17.25, 17.5, 17.75, 18, 18.2, 18.4, 18.6, 18.8, 19, 19.2, 19.4, 19.6, 19.8, 20, 20.2, 20.4,
            20.6, 20.8, 21, 21.25, 21.5, 21.75, 22, 22.25, 22.5, 22.75, 23, 23.25, 23.5, 23.75, 24, 24.25, 24.5, 24.75,
            25, 25.3, 25.7, 26, 26.3, 26.7, 27, 27.3, 27.7, 28, 28.5, 29, 29.5, 30, 30.5, 31, 32, 33, 34, 35, 37, 39,
            43, 55]


def rating_list():
    return [0, 0.365369529, 0.730739059, 1.096108588, 1.461478118, 4.764924071, 6.640593177, 8.150365117, 9.591045659,
            10.90909931, 12.13344672, 13.31296476, 14.43967859, 15.54341185, 16.59802956, 17.59752357, 18.5791348,
            19.49038066, 20.39325832, 21.26166102, 22.11446459, 22.95123514, 23.7732702, 24.57580151, 25.36075071,
            26.15331687, 26.91496815, 27.67842636, 28.42022351, 29.15603932, 29.88544283, 30.59244211, 31.29638462,
            31.96264124, 32.6396489, 33.30906037, 33.9523298, 34.6251024, 35.26416773, 35.9063431, 36.55427317,
            37.17536979, 37.80095815, 38.41363559, 39.01941797, 39.640747, 40.27038448, 40.87308393, 41.47126639,
            42.06856191, 42.68055429, 43.29657903, 43.90359328, 44.51366072, 45.12440918, 45.73755985, 46.34461011,
            46.94228823, 47.55518785, 48.1587872, 48.76701055, 49.3879583, 50.00395315, 50.61866769, 51.22694768,
            51.84043679, 52.45294207, 53.07669525, 53.69348872, 54.31987412, 54.93754827, 55.56024909, 56.20674658,
            56.84714794, 57.50351362, 58.15869355, 58.81739403, 59.50660455, 60.19206879, 60.89175493, 61.60323902,
            62.32409462, 63.04527194, 63.78381082, 64.55229649, 65.34161161, 66.15075853, 66.99314556, 67.84036169,
            68.77815106, 69.76666962, 70.73838242, 71.83509849, 72.98939006, 74.21626686, 75.53775587, 77.05521223,
            78.77329187, 80.81064969, 83.47527846]


def error_answer(task, token='', message=None, time_now=datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")):
    return '{' + \
           '"task":"{}",'.format(task) + \
           '"token":"{}",'.format(token) + \
           '"insta_handle":"[]",' + \
           '"status":false,' + \
           '"datetime":"{}",'.format(time_now) + \
           '"errors":{}'.format(json.dumps(message)) + \
           '}'


def answer_account_rating(task, username, rating, token='',
                          timenow=datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")):
    return '{' + \
           '"task":"{}",'.format(task) + \
           '"token":"{}",'.format(token) + \
           '"insta_handle":"{}",'.format(username) + \
           '"status":true,' + \
           '"datetime":"{}",'.format(timenow) + \
           '"rating":"{}"'.format("{0:.2f}".format(rating)) + \
           '}'


if __name__ == '__main__':

    instance = 61
    instance_type = 'influencer'
    amqp_instance = 3
    virtual_host = 'test'
    exchange = 'instagram-exchange'
    debug = False

    if 'instance' in os.environ:
        instance = int(os.environ['instance'])

    if 'amqp' in os.environ:
        amqp_instance = int(os.environ['amqp'])

    if 'virtual_host' in os.environ:
        virtual_host = str(os.environ['virtual_host'])

    if 'exchange' in os.environ:
        exchange = str(os.environ['exchange'])

    if 'debug' in os.environ:
        debug = bool(os.environ['debug'])

    if 'docker' in os.environ:
        fluent_host = config.LOG_FLUENT_HOST_DOCKER
    else:
        fluent_host = config.LOG_FLUENT_HOST

    if len(sys.argv) > 1:
        instance = int(sys.argv[1])

    if len(sys.argv) > 2:
        amqp_instance = int(sys.argv[2])

    if len(sys.argv) > 3:
        virtual_host = str(sys.argv[3])

    if len(sys.argv) > 4:
        exchange = str(sys.argv[4])

    if len(sys.argv) > 5:
        debug = bool(sys.argv[5])

    custom_f = CustomFunction(instance, debug)

    # Set logger
    log = custom_f.set_logging(instance, None, 'fluent', instance_type,
                               'application.assem.{}.{}'.format(instance_type, virtual_host),
                               {'host': fluent_host, 'port': config.LOG_FLUENT_PORT},
                               'INFO' if not debug else 'DEBUG', ['s3transfer', 'pika'],
                               ['botocore', 'urllib3', 'requests.packages.urllib3.connectionpool'], virtual_host)

    amqp_request_f = AMQPRequests(log)

    routing_request_config = 'influencer.task'
    routing_return_config = 'influencer.result'
    routing_error_config = config.AMQP_QUEUE_ERROR

    print(amqp_instance)
    # Get AQMP details
    amqp_creds = custom_f.get_amqp_by_key(amqp_instance)

    # Start s3
    s3 = S3(log, config.AWS_S3_ACCESS_KEY_ID, config.AWS_S3_ACCESS_KEY_SECRET)
    s3.connect_client()

    s3_influencer_bucket = 'influencers-bucket'

    # Set RabbitMQ to listen tasks
    Amqp = AMQP(
        exchange,
        routing_request_config,
        virtual_host,
        amqp_creds['host'],
        amqp_creds['port'],
        amqp_creds['login'],
        amqp_creds['pass'],
        amqp_callback,
        log,
        debug
    )

    try:
        Amqp.run()
    except KeyboardInterrupt:
        Amqp.stop()
