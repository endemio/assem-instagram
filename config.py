import os
import sys

# AMQP queue default
AMQP_QUEUE_INSTAGRAM = "instagram"
AMQP_QUEUE_AUDIT_2 = "audit"
AMQP_QUEUE_AUDIT_CHOICE = "audit.choice"
AMQP_QUEUE_AUDIT_REQ = "audit.requirements"
AMQP_QUEUE_AUDIT_FINAL = "audit.final"
AMQP_QUEUE_AUDIT_INFINITY = 'audit.infinity'
AMQP_QUEUE_INFO = "info"
AMQP_QUEUE_PUBLIC = "public"
AMQP_QUEUE_PRIVATE = "private"
AMQP_QUEUE_APPLICATION = "application"
AMQP_QUEUE_LOCATION = "location"
AMQP_QUEUE_MYSQL = "mysql"
AMQP_QUEUE_REDIS = "redis"
AMQP_QUEUE_ERROR = "error"
AMQP_QUEUE_RATING_TASK = "ratingtask"
AMQP_QUEUE_RATING_RESULT = "ratingresult"

AMQP_QUEUE_INFLUENCER_TASK = 'influencer.task'
AMQP_QUEUE_INFLUENCER_TASK_Z = 'influencer.z_task'
AMQP_QUEUE_INFLUENCER_RESULT = 'influencer.result'

AMQP_SEND_ACCOUNTS_LIMIT = 50

# LOG settings
# LOG_PATH = os.path.join(str(os.getcwd()), "/home/endemic/storage/logs")
LOG_PATH = os.path.dirname(os.path.realpath(sys.argv[0])) + "/../storage/logs"
LOG_FLUENT_HOST_DOCKER = '172.17.0.1'
LOG_FLUENT_HOST = '127.0.0.1'
LOG_FLUENT_PORT = 24000

# Credentials/proxy
CREDENTIAL_INSTAGRAM_PROXY_URL = 'https://www.snikteam.com/config0vugGLEZOYViWm7j/proxy.txt'
CREDENTIAL_INSTAGRAM_ACCOUNTS_URL = 'https://www.snikteam.com/get-instagram-credential'

# Options settings
SETTINGS_MYSQL_PATH = os.path.dirname(os.path.realpath(sys.argv[0])) + "/config/mysql.txt"
SETTINGS_AMQP_PATH = os.path.dirname(os.path.realpath(sys.argv[0])) + "/config/amqp.txt"
SETTINGS_REDIS_PATH = os.path.dirname(os.path.realpath(sys.argv[0])) + "/config/redis.txt"
SETTINGS_REDIS_SLAVE_PATH = os.path.dirname(os.path.realpath(sys.argv[0])) + "/config/redis-slaves.txt"
SETTINGS_PROXY_AUTO_PATH = os.path.dirname(os.path.realpath(sys.argv[0])) + "/extension/Proxy-Auto-Auth_v2.0.crx"

# User-agent list
USERAGENTS_PATH = os.path.dirname(os.path.realpath(sys.argv[0])) + "/config/ua.json"

# Redis settings
# Instagram account data
REDIS_ACCOUNTS_HOST = '95.217.58.181'
REDIS_ACCOUNTS_PORT = 6379
REDIS_SLAVE_INSTANCE_NUMBER = 9
REDIS_PASSWORD = 'Z6WD7pGGNgTqpCbu'
REDIS_ACCOUNTS_BUCKETS_KEYS = 20000

# Instagram account data
REDIS_TEMP_VALUES_HOST = '88.99.81.198'
REDIS_TEMP_VALUES_PORT = 2083
REDIS_TEMP_VALUES_BUCKETS_KEYS = 1000000
REDIS_TEMP_VALUES_PASSWORD = 'GLmJ1eOv4oxakKMG9Oh1kjdsAmm6Es1GM'

# Instagram account data
REDIS_LOCATION_HOST = '95.217.58.181'
REDIS_LOCATION_PORT = 6389
REDIS_LOCATION_BUCKETS_KEYS = 1
REDIS_LOCATION_PASSWORD = 'GLmJ1eOv4oxakKMG9Oh1k'

# REDIS HUGE SCARAPING SERRINGS
REDIS_HUGE_BIN_KEYS = 500_000_000

# S3 settings
AWS_S3_REGION = 'eu-central-1'
AWS_S3_ACCESS_KEY_ID = 'AKIAJEVP5QEHFSNBRMZQ'
AWS_S3_ACCESS_KEY_SECRET = '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'
AWS_S3_FOLLOWERS_BUCKET = 'followers-bucket'
AWS_S3_FOLLOWINGS_BUCKET = 'followings-bucket'

# S3 settings
MINIO_S3_HOST = '88.99.81.198:9000'
MINIO_S3_REGION = 'eu-central-1'
MINIO_S3_ACCESS_KEY_ID = 'syR48tzPDKbpBL9J'
MINIO_S3_ACCESS_KEY_SECRET = 'TJ9zKBzLmUnwBtdACJVL3EQuE8THCKTjAeX2xVCT'
MINIO_S3_FOLLOWERS_BUCKET = 'followers-bucket'
MINIO_S3_FOLLOWINGS_BUCKET = 'followings-bucket'

# Proxy
PROXY_ENDEMIC_AUTH_TOKEN_KEY = 'ewldoiewhjfj892ur0923ucrj02389r0bu8923ybrh8923'
PROXY_ERROR_LIMIT = 29

# Instagram.py options
INSTAGRAM_CHECK_USERNAMES_PARSING_DELAY = 25
INSTAGRAM_CHECK_USERNAMES_IN_REQUEST = 20
INSTAGRAM_CHECK_INFO_IN_REQUEST = 1
INSTAGRAM_CHECK_INFO_PARSING_DELAY = 10
INSTAGRAM_LOCAL_STORE_PATH = os.path.dirname(os.path.realpath(sys.argv[0])) + "/../storage/instagram"
INSTAGRAM_POSTS_LOCAL_STORE_PATH = os.path.dirname(os.path.realpath(sys.argv[0])) + "/../storage/posts"
INSTAGRAM_COMMENTS_LOCAL_STORE_PATH = os.path.dirname(os.path.realpath(sys.argv[0])) + "/../storage/comments"

# Private.py options
PRIVATE_FOLLOWERS_LOCAL_STORE_PATH = os.path.dirname(os.path.realpath(sys.argv[0])) + "/../storage/followers"
PRIVATE_FOLLOWINGS_LOCAL_STORE_PATH = os.path.dirname(os.path.realpath(sys.argv[0])) + "/../storage/followings"
PRIVATE_CHECK_MAX_LIMIT = 400
PRIVATE_CHECK_RANDOM_LIMIT = 100
PRIVATE_CHECK_SLEEP = 15

# {"task":"get-followers-list","datetime":"2019-04-03 19:09:08","status":true,"username":"tom.boy","export_random":true,"send_limit":"100","store_s3":true,"store_json":false,"return_queue":"instagram"}
